import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/routes/router.gr.dart' as R;
import 'package:farhaty_app/ui/home/pages/account/account_model.dart';
import 'package:farhaty_app/ui/home/pages/favorite/favorite_model.dart';
import 'package:farhaty_app/ui/login/login_model.dart';
import 'package:farhaty_app/ui/reservations/new_reservation/new_reservation_model.dart';
import 'package:farhaty_app/ui/signup/signup_model.dart';
import 'package:farhaty_app/ui/stores/store_model.dart';
import 'package:farhaty_app/utils/fallback-cupertino-localization-delegete.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

import 'di/dependencies_config.dart';
import 'generated/i18n.dart';

Future<void> main() async {
  // init dependencies injector
  WidgetsFlutterBinding.ensureInitialized();
  await configureDependencies();

  // run the app
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AccountModel>(
          create: (_) => getIt<AccountModel>(),
        ),
        Provider<LoginModel>(
          create: (_) => getIt<LoginModel>(),
        ),
        Provider<SignupModel>(
          create: (_) => getIt<SignupModel>(),
        ),
        ChangeNotifierProvider<StoreModel>(
          create: (_) => getIt<StoreModel>(),
        ),
        ChangeNotifierProvider<FavoritesModel>(
          create: (_) => getIt<FavoritesModel>(),
        ),
        ChangeNotifierProvider<NewReservationModel>(
          create: (_) => getIt<NewReservationModel>(),
        ),
      ],
      child: MaterialApp(
        theme: _theme,
        locale: Locale('ar', ''),
        debugShowCheckedModeBanner: false,
        localizationsDelegates: [S.delegate, GlobalMaterialLocalizations.delegate, GlobalWidgetsLocalizations.delegate, FallbackCupertinoLocalisationsDelegate()],
        supportedLocales: S.delegate.supportedLocales,
        localeListResolutionCallback: S.delegate.listResolution(fallback: const Locale('ar', '')),
        onGenerateRoute: R.Router.onGenerateRoute,
        navigatorKey: R.Router.navigator.key,
        initialRoute: R.Router.homeScreen,
      ),
    );
  }

  ThemeData get _theme => ThemeData(
        primaryColor: AppColors.primary,
        accentColor: AppColors.accent,
        fontFamily: 'sky',
        primaryTextTheme: TextTheme(
          headline6: TextStyle(color: Colors.white),
        ),
        primaryIconTheme: const IconThemeData.fallback().copyWith(
          color: Colors.white,
        ),
      );
}
