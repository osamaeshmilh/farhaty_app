import 'package:dio/dio.dart';
import 'package:farhaty_app/core/provider/view_model.dart';
import 'package:farhaty_app/data/remote/services/store_images_service.dart';
import 'package:farhaty_app/data/remote/services/store_service.dart';
import 'package:farhaty_app/ui/stores/store_details/store_details_state.dart';
import 'package:injectable/injectable.dart';

@injectable
class StoreDetailsModel extends ChangeNotiferModel<StoreDetailsState> {
  final StoreService storeService;
  final StoreImagesService storeImagesService;

  StoreDetailsModel(this.storeService, this.storeImagesService);

  @override
  StoreDetailsState get initialState => StoreDetailsInitial();

  query({Map<String, dynamic> queryData}) async {
    try {
      //await PreferenceManager.getToken().then((v) => {print(v)});

      state = StoreDetailsLoading();
      final store = await storeService.getOne(queryData['storeId.equals'].toString());
      store.items.sort((a, b) {
        return b.id.compareTo(a.id);
      });

      final storeImages = await storeImagesService.queryAll(queryData: queryData);

      state = StoreDetailsLoaded(store, storeImages);
    } on DioError {}
  }
}
