import 'package:cached_network_image/cached_network_image.dart';
import 'package:farhaty_app/models/store_image.dart';
import 'package:farhaty_app/ui/widgets/DetailImage.dart';
import 'package:flutter/material.dart';

class StoreImagesWidget extends StatefulWidget {
  final StoreImage storeImage;
  final Function(num id) onTap;

  const StoreImagesWidget(this.storeImage, {this.onTap});

  @override
  _StoreImagesWidgetState createState() => _StoreImagesWidgetState();
}

class _StoreImagesWidgetState extends State<StoreImagesWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 0),
      child: InkWell(
        child: Stack(
          children: <Widget>[
            CachedNetworkImage(
              imageUrl: widget.storeImage.imageUrl,
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: 60,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      Color(0xfffafafa),
                      Color(0xfffafafa),
                      Color(0xfffafafa),
                    ]),
                  ),
                ))
          ],
        ),
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (_) {
            return DetailImage(heroTag: widget.storeImage.id.toString(), image: widget.storeImage.imageUrl);
          }));
        },
      ),
    );
  }
}
