import 'package:farhaty_app/core/provider/change_notifier_provider_widget.dart';
import 'package:farhaty_app/core/provider/state_consumer.dart';
import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/ui/stores/store_details/store_details_model.dart';
import 'package:farhaty_app/ui/stores/store_details/store_details_state.dart';
import 'package:farhaty_app/ui/stores/store_details/store_details_widget.dart';
import 'package:farhaty_app/ui/widgets/error_widget.dart';
import 'package:farhaty_app/ui/widgets/loading_widget.dart';
import 'package:flutter/material.dart';

class StoreDetailsScreen extends ChangeNotifierProviderWidget<StoreDetailsModel> {
  final String storeId;
  final queryData = Map<String, dynamic>();

  StoreDetailsScreen(this.storeId);

  @override
  onModelReady(StoreDetailsModel model) {
    queryData..['storeId.equals'] = storeId;
    model.query(queryData: queryData);
  }

  @override
  Widget build(BuildContext context, StoreDetailsModel storeModel) {
    return Scaffold(
      backgroundColor: AppColors.background,
      body: StateConsumer<StoreDetailsModel>(builder: (model, state) {
        if (state is StoreDetailsLoading) return LoadingWidget();
        if (state is StoreDetailsLoaded) return StoreDetailsWidget(state.store, state.storeImages);
        return ErrorRetryWidget(onRetry: model.query(queryData: queryData));
      }),
    );
  }
}
