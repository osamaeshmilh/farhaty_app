import 'package:cached_network_image/cached_network_image.dart';
import 'package:farhaty_app/models/favorite.dart';
import 'package:farhaty_app/models/item.dart';
import 'package:farhaty_app/models/store.dart';
import 'package:farhaty_app/models/store_image.dart';
import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/res/styles.dart';
import 'package:farhaty_app/ui/stores/store_details/store_images_widget.dart';
import 'package:farhaty_app/ui/stores/store_model.dart';
import 'package:farhaty_app/ui/widgets/DetailImage.dart';
import 'package:farhaty_app/ui/widgets/empty_items_widget.dart';
import 'package:farhaty_app/utils/farhaty_app_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:like_button/like_button.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

class StoreDetailsWidget extends StatefulWidget {
  final Store store;
  final List<StoreImage> storeImages;

  const StoreDetailsWidget(this.store, this.storeImages);

  @override
  _StoreDetailsWidgetState createState() => _StoreDetailsWidgetState();
}

class _StoreDetailsWidgetState extends State<StoreDetailsWidget> with SingleTickerProviderStateMixin {
  final _pageController = PageController(initialPage: 0);

  int _tabIndex = 0;
  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 2, initialIndex: _tabIndex, vsync: this);
    _tabController.addListener(_handleTabSelection);
    super.initState();
  }

  _handleTabSelection() {
    if (_tabController.indexIsChanging) {
      setState(() {
        _tabIndex = _tabController.index;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                leading: new IconButton(
                  icon: new Icon(Icons.arrow_back_ios, color: AppColors.primary),
                  onPressed: () => Navigator.of(context).pop(),
                ),
                title: Text(''),
                floating: true,
                elevation: 0,
                primary: true,
                expandedHeight: 250.0,
                backgroundColor: Colors.white,
                flexibleSpace: FlexibleSpaceBar(
                  background: Container(
                    height: 200,
                    child: Swiper(
                      viewportFraction: 1,
                      layout: SwiperLayout.DEFAULT,
                      itemCount: widget.storeImages.length,
                      // transformer: PageTransformer(),
                      itemHeight: 200.0,
                      autoplay: true,
                      itemBuilder: (_, int index) => StoreImagesWidget(widget.storeImages[index]),
                    ),
                  ),
                ),
                bottom: TabBar(
                    controller: _tabController,
                    indicatorSize: TabBarIndicatorSize.label,
                    labelPadding: EdgeInsets.symmetric(horizontal: 10),
                    unselectedLabelColor: Theme.of(context).accentColor,
                    labelColor: Colors.white,
                    indicator: BoxDecoration(borderRadius: BorderRadius.circular(50), color: Theme.of(context).accentColor),
                    tabs: [
                      Tab(
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(50), border: Border.all(color: Theme.of(context).accentColor.withOpacity(0.2), width: 1)),
                          child: Align(
                            alignment: Alignment.center,
                            child: Text('التفاصيل'),
                          ),
                        ),
                      ),
                      Tab(
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(50), border: Border.all(color: Theme.of(context).accentColor.withOpacity(0.2), width: 1)),
                          child: Align(
                            alignment: Alignment.center,
                            child: Text('المنتجات والخدمات'),
                          ),
                        ),
                      ),
                    ]),
              ),
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Offstage(
                      offstage: 0 != _tabIndex,
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            vS8,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(widget.store.name, overflow: TextOverflow.fade, softWrap: false, maxLines: 1, style: defaultStyle(16, true)),
                                widget.store.vip
                                    ? Container(
                                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                                        decoration: BoxDecoration(color: Colors.orange, borderRadius: BorderRadius.circular(24)),
                                        child: Row(
                                          children: <Widget>[
                                            Text(
                                              'مميز',
                                              style: TextStyle(color: Colors.white),
                                            ),
                                            hS8,
                                            Icon(
                                              FontAwesomeIcons.solidStar,
                                              color: Colors.white,
                                              size: 16,
                                            )
                                          ],
                                        ))
                                    : SizedBox(),
                              ],
                            ),
                            Divider(indent: 8),
                            vS16,
                            Text(widget.store.description),
                            ListTile(
                              leading: Icon(FontAwesome.map_marker, color: AppColors.lightPurple),
                              title: Text(widget.store.address),
                              trailing: (widget.store.lat != null && widget.store.lng != null)
                                  ? SizedBox(
                                      width: 42,
                                      height: 42,
                                      child: IconButton(
                                        padding: EdgeInsets.all(0),
                                        iconSize: 24,
                                        icon: new Image.asset('assets/images/google-maps.png'),
                                        onPressed: () {
                                          launch('https://www.google.com/maps/dir/?api=1&destination=' +
                                              widget.store.lat.toString() +
                                              ',' +
                                              widget.store.lng.toString() +
                                              '&travelmode=driving&dir_action=navigate');
                                        },
                                      ),
                                    )
                                  : SizedBox(),
                            ),
                            vS16,
                            Text(
                              'تواصل معنا عبر',
                              style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                            ),
                            Divider(indent: 8),
                            ListTile(
                              onTap: () {
                                launch("tel:" + widget.store.phone);
                              },
                              leading: Icon(FontAwesome.phone, size: 24, color: AppColors.lightPurple),
                              title: Text(widget.store.phone),
                            ),
                            ListTile(
                              onTap: () {
                                launch("mailto:" + widget.store.email);
                              },
                              leading: Icon(Icons.email, size: 24, color: AppColors.lightPurple),
                              title: Text(widget.store.email),
                            ),
                            if (widget.store.facebook != null)
                              ListTile(
                                onTap: () {
                                  var str = "https://www.facebook.com/" + Uri.encodeComponent(widget.store.facebook);
                                  try {
                                    print(str);
                                    launch(str);
                                  } catch (e) {
                                    print(e.toString());
                                  }
                                },
                                leading: Icon(FontAwesome.facebook_square, size: 24, color: Colors.blueAccent),
                                title: Text(widget.store.facebook),
                              ),
                            if (widget.store.instagram != null)
                              ListTile(
                                onTap: () {
                                  launch("https://instagram.com/" + widget.store.instagram);
                                },
                                leading: Icon(FontAwesome.instagram, size: 24, color: Colors.purple.shade400),
                                title: Text(widget.store.instagram),
                              ),
                            if (widget.store.snapchat != null)
                              ListTile(
                                onTap: () {
                                  launch("https://snapchat.com/" + widget.store.snapchat);
                                },
                                leading: Icon(FontAwesome.snapchat, size: 24, color: Colors.yellow.shade400),
                                title: Text(widget.store.snapchat),
                              ),
                            if (widget.store.whatsup != null)
                              ListTile(
                                onTap: () {
                                  launch("whatsapp://send?phone=+218" + widget.store.whatsup);
                                },
                                leading: Icon(FontAwesome.whatsapp, size: 24, color: Colors.green.shade400),
                                title: Text(widget.store.whatsup),
                              ),
                            if (widget.store.twitter != null)
                              ListTile(
                                onTap: () {
                                  launch("https://twitter.com/" + widget.store.twitter);
                                },
                                leading: Icon(FontAwesome.twitter, size: 24, color: Colors.blue.shade400),
                                title: Text(widget.store.twitter),
                              ),
                          ],
                        ),
                      ),
                    ),
                    Offstage(
                      offstage: 1 != _tabIndex,
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 6,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('الخدمات والمنتجات', style: defaultStyle(16, true)),
                              ],
                            ),
                            Divider(indent: 8),
                            widget.store.items.isNotEmpty
                                ? Column(
                                    children: widget.store.items.map<Widget>((Item item) => buildItemWidget(item)).toList(),
                                  )
                                : Padding(
                                    padding: const EdgeInsets.only(top: 20),
                                    child: EmptyItemsWidget(),
                                  ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        buildFooter()
      ],
    );
  }

  Widget buildFooter() {
    final storeModel = Provider.of<StoreModel>(context, listen: false);
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.9),
        borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20)),
        boxShadow: [
          BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.15), blurRadius: 5, offset: Offset(0, -2)),
        ],
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            child: RaisedButton(
              color: AppColors.primary,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
              padding: EdgeInsets.symmetric(vertical: 8),
              onPressed: () {
                launch("tel:" + widget.store.phone);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    FarhatyApp.phone,
                    color: Colors.white,
                    size: 21,
                  ),
                  hS8,
                  Text("اتصل بنا", style: TextStyle(color: Colors.white)),
                ],
              ),
            ),
          ),
          hS32,
          hS8,
          FutureBuilder<bool>(
              future: storeModel.isInFavorites(widget.store.id),
              builder: (context, snapshot) {
                return LikeButton(
                  size: 28,
                  isLiked: snapshot?.data == true,
                  onTap: (isLiked) async {
                    print(isLiked);
                    storeModel.toggleFavoriteStatus(Favorite(store: widget.store, isFavorite: isLiked));
                    return !isLiked;
                  },
                );
              }),
          IconButton(
            icon: Icon(
              FarhatyApp.share_alt,
              color: AppColors.primary,
            ),
            onPressed: () {
              Share.share('قم بتحميل تطبيق برايدل : https://onelink.to/8xtp2j');
            },
          ),
        ],
      ),
    );
  }

  buildItemWidget(Item item) {
    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (_) {
          return DetailImage(heroTag: item.id.toString(), image: item.imageUrl);
        }));
      },
      child: Card(
        child: ListTile(
          title: Text(item.name ?? ' '),
          subtitle: Text(item.description ?? ' '),
          leading: ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: CachedNetworkImage(
              imageUrl: item.imageUrl,
              width: 80,
              height: 80,
              fit: BoxFit.cover,
            ),
          ),
          trailing: Text(item.price != null ? item.price.toString() + ' ' + item.getCurrencyText() : 'غير محدد'),
        ),
      ),
    );
  }
}
