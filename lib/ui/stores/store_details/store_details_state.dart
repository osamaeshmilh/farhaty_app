import 'package:farhaty_app/models/managed_user_vm.dart';
import 'package:farhaty_app/models/store.dart';
import 'package:farhaty_app/models/store_image.dart';

abstract class StoreDetailsState{}

class StoreDetailsInitial extends StoreDetailsState {}

class StoreDetailsLoaded extends StoreDetailsState{
  final Store store;
  final List<StoreImage> storeImages;

  StoreDetailsLoaded(this.store, this.storeImages);
}

class StoreDetailsLoading extends StoreDetailsState{}