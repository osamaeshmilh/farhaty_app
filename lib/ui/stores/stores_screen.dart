import 'package:cached_network_image/cached_network_image.dart';
import 'package:farhaty_app/core/provider/change_notifier_provider_widget.dart';
import 'package:farhaty_app/core/provider/state_consumer.dart';
import 'package:farhaty_app/core/state/list_state.dart';
import 'package:farhaty_app/models/favorite.dart';
import 'package:farhaty_app/models/store.dart';
import 'package:farhaty_app/models/store_filter_args.dart';
import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/routes/router.gr.dart' as R;
import 'package:farhaty_app/ui/stores/store_model.dart';
import 'package:farhaty_app/ui/widgets/error_widget.dart';
import 'package:farhaty_app/ui/widgets/list_builder.dart';
import 'package:farhaty_app/ui/widgets/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:like_button/like_button.dart';
import 'package:provider/provider.dart';

class StoresScreen extends ChangeNotifierProviderWidget<StoreModel> {
  final StoreFilterArgs storeFilterArgs;
  final queryData = Map<String, dynamic>();

  var liked;

  StoresScreen(this.storeFilterArgs);

  @override
  onModelReady(StoreModel model) {
    if (storeFilterArgs != null) {
      queryData..['vip.equals'] = storeFilterArgs?.vip;
      queryData..['categoryId.equals'] = storeFilterArgs?.categoryId;
    }
    model.query(queryData: queryData);
  }

  @override
  Widget build(BuildContext context, StoreModel storeModel) {
    return Scaffold(
      appBar: AppBar(
        title: Material(
          borderRadius: BorderRadius.all(Radius.circular(32.0)),
          child: SizedBox(
            height: 40,
            child: TextField(
              onChanged: (text) => queryData..['name.contains'] = text,
              cursorColor: AppColors.primary,
              decoration: InputDecoration(
                hintText: 'بحث ...',
                contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 2.0),
                suffixIcon: Padding(
                  padding: const EdgeInsets.only(left: 2),
                  child: IconButton(
                    onPressed: () => storeModel.query(queryData: queryData),
                    icon: Icon(Icons.search, color: AppColors.primary),
                  ),
                ),
                border: InputBorder.none,
              ),
            ),
          ),
        ),
      ),
      body: StateConsumer<StoreModel>(builder: (model, state) {
        if (state is ListLoading) return LoadingWidget();
        if (state is ListLoaded) return buildList(state.items, context);
        return ErrorRetryWidget(onRetry: model.query(queryData: queryData));
      }),
    );
  }

  Widget buildList(List<Store> stores, BuildContext context) {
    final storeModel = Provider.of<StoreModel>(context, listen: false);
    return ListBuilder<Store>(
      padding: EdgeInsets.only(top: 16),
      items: stores,
      builder: (store) => Stack(
        children: <Widget>[
          Card(
            elevation: 0.5,
            margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
            child: ListTile(
              contentPadding: EdgeInsetsDirectional.only(start: 100),
              title: Text(
                store.name,
                overflow: TextOverflow.fade,
                softWrap: false,
                maxLines: 1,
              ),
              subtitle: Text(
                store.address,
                overflow: TextOverflow.fade,
                softWrap: false,
                maxLines: 2,
              ),
              trailing: Container(
                width: 35,
                margin: EdgeInsets.only(left: 14),
                child: FutureBuilder<bool>(
                    future: storeModel.isInFavorites(store.id),
                    builder: (context, snapshot) {
                      return LikeButton(
                        size: 28,
                        isLiked: snapshot?.data == true,
                        onTap: (isLiked) async {
                          print(isLiked);
                          storeModel.toggleFavoriteStatus(Favorite(store: store, isFavorite: isLiked));

                          return !isLiked;
                        },
                      );
                    }),
              ),
              onTap: () {
                R.Router.navigator.pushNamed(
                  R.Router.storeDetailsScreen,
                  arguments: store.id.toString(),
                );
              },
            ),
          ),
          Positioned(
            right: 24,
            top: 0,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: CachedNetworkImage(
                imageUrl: store.logoUrl,
                width: 80,
                height: 80,
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
