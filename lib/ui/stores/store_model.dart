import 'package:dio/dio.dart';
import 'package:event_bus/event_bus.dart';
import 'package:farhaty_app/core/provider/view_model.dart';
import 'package:farhaty_app/core/state/list_state.dart';
import 'package:farhaty_app/data/local/dao/favorite_dao.dart';
import 'package:farhaty_app/data/remote/services/store_service.dart';
import 'package:farhaty_app/models/favorite.dart';
import 'package:farhaty_app/ui/home/pages/favorite/favorite_model.dart';
import 'package:injectable/injectable.dart';

@injectable
class StoreModel extends ChangeNotiferModel<ListState> {
  final StoreService storeService;
  final FavoriteDao favoriteDao;
  final EventBus eventBus;

  StoreModel(this.storeService, this.favoriteDao, this.eventBus);

  @override
  ListState get initialState => ListUninitialized();

  query({Map<String, dynamic> queryData}) async {
    try {
      state = ListLoading();
      final res = await storeService.queryAll(queryData: queryData);

      state = ListLoaded(res);
    } on DioError {
      state = ListError();
    }
  }

  void toggleFavoriteStatus(Favorite favorite) async {
    if (favorite.isFavorite) {
      await removeFromFavorites(favorite.store.id);
    } else {
      await addToFavorites(favorite);
    }

    eventBus.fire(LoadFavoritesListEvent());
  }

  Future addToFavorites(Favorite favorite) async {
    favorite.store.items.clear();
    await favoriteDao.insert(favorite);
  }

  Future removeFromFavorites(num id) {
    return favoriteDao.deleteByStoreId(id);
  }

  Future<bool> isInFavorites(num storeId) {
    return favoriteDao.storeExists(storeId);
  }
}
