import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/res/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AboutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("حول"),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              vS8,
              Text(
                'مرحبا بكم في تطبيق برايدل',
                style: TextStyle(color: Colors.black87, fontWeight: FontWeight.bold),
              ),
              vS8,
              Text(
                  'تطبيق خدمي صمم عن طريق شركة الازدهار لخدمات الحاسب الالي وتقنية المعلومات، يستهدف الاشخاص المقبلين على عمل مناسبات اجتماعية ويضم التطبيق صالات المناسبات وكافة المتاجر التي تحتاجها لاتمام مناسبتك.'),
              vS8,
              Text('جميع الحقوق محفوطة'),
              vS8,
              Text(
                'Copyright © 2020 Bridal Ly',
                style: TextStyle(color: AppColors.primary, fontWeight: FontWeight.bold),
              ),
              vS8,
            ],
          ),
        ),
      ),
    );
  }
}
