import 'package:cached_network_image/cached_network_image.dart';
import 'package:farhaty_app/models/extra.dart';
import 'package:farhaty_app/models/hall.dart';
import 'package:farhaty_app/models/hall_filter_args.dart';
import 'package:farhaty_app/models/hall_image.dart';
import 'package:farhaty_app/models/room.dart';
import 'package:farhaty_app/models/type.dart';
import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/res/styles.dart';
import 'package:farhaty_app/routes/router.gr.dart' as R;
import 'package:farhaty_app/ui/halls/hall_details/hall_images_widget.dart';
import 'package:farhaty_app/ui/home/pages/account/account_model.dart';
import 'package:farhaty_app/ui/widgets/DetailImage.dart';
import 'package:farhaty_app/ui/widgets/loading_overlay.dart';
import 'package:farhaty_app/utils/farhaty_app_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

class HallDetailsWidget extends StatefulWidget {
  final Hall hall;
  final HallFilterArgs hallFilterArgs;
  final List<HallImage> hallImages;

  const HallDetailsWidget(this.hall, this.hallImages, this.hallFilterArgs);

  @override
  _HallDetailsWidgetState createState() => _HallDetailsWidgetState();
}

class _HallDetailsWidgetState extends State<HallDetailsWidget> with SingleTickerProviderStateMixin {
  int _tabIndex = 0;
  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 3, initialIndex: _tabIndex, vsync: this);
    _tabController.addListener(_handleTabSelection);
    super.initState();
  }

  _handleTabSelection() {
    if (_tabController.indexIsChanging) {
      setState(() {
        _tabIndex = _tabController.index;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.9),
            borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20)),
            boxShadow: [
              BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.15), blurRadius: 5, offset: Offset(0, -2)),
            ],
          ),
          child: buildFooter()),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back_ios, color: AppColors.primary),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: Text(''),
            floating: true,
            elevation: 0,
            primary: true,
            expandedHeight: 250.0,
            backgroundColor: Colors.white,
            flexibleSpace: FlexibleSpaceBar(
              background: Container(
                height: 200,
                child: Swiper(
                  viewportFraction: 1,
                  layout: SwiperLayout.DEFAULT,
                  itemCount: widget.hallImages.length,
                  // transformer: PageTransformer(),
                  itemHeight: 200.0,
                  autoplay: true,
                  itemBuilder: (_, int index) => HallImagesWidget(widget.hallImages[index]),
                ),
              ),
            ),
            bottom: TabBar(
                controller: _tabController,
                indicatorSize: TabBarIndicatorSize.label,
                labelPadding: EdgeInsets.symmetric(horizontal: 10),
                unselectedLabelColor: Theme.of(context).accentColor,
                labelColor: Colors.white,
                indicator: BoxDecoration(borderRadius: BorderRadius.circular(50), color: Theme.of(context).accentColor),
                tabs: [
                  Tab(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(50), border: Border.all(color: Theme.of(context).accentColor.withOpacity(0.2), width: 1)),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text('تفاصيل'),
                      ),
                    ),
                  ),
                  Tab(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(50), border: Border.all(color: Theme.of(context).accentColor.withOpacity(0.2), width: 1)),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text('القاعات'),
                      ),
                    ),
                  ),
                  Tab(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(50), border: Border.all(color: Theme.of(context).accentColor.withOpacity(0.2), width: 1)),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text('المناسبات'),
                      ),
                    ),
                  ),
                ]),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Offstage(
                  offstage: 0 != _tabIndex,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            vS8,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(widget.hall.name, overflow: TextOverflow.fade, softWrap: false, maxLines: 1, style: defaultStyle(16, true)),
                                widget.hall.vip
                                    ? Container(
                                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                                        decoration: BoxDecoration(color: Colors.orange, borderRadius: BorderRadius.circular(24)),
                                        child: Row(
                                          children: <Widget>[
                                            Text(
                                              'مميز',
                                              style: TextStyle(color: Colors.white),
                                            ),
                                            hS8,
                                            Icon(
                                              FontAwesomeIcons.solidStar,
                                              color: Colors.white,
                                              size: 16,
                                            )
                                          ],
                                        ))
                                    : SizedBox(),
                              ],
                            ),
                            Divider(indent: 8),
                            ListTile(
                              leading: Icon(FontAwesome.map_marker, color: AppColors.lightPurple),
                              title: Text(widget.hall.address ?? ''),
                              trailing: (widget.hall.lat != null && widget.hall.lng != null)
                                  ? SizedBox(
                                      width: 42,
                                      height: 42,
                                      child: IconButton(
                                        padding: EdgeInsets.all(0),
                                        iconSize: 24,
                                        icon: new Image.asset('assets/images/google-maps.png'),
                                        onPressed: () {
                                          launch('https://www.google.com/maps/search/?api=1&query=' + widget.hall.lat.toString() + ',' + widget.hall.lng.toString());
                                        },
                                      ),
                                    )
                                  : SizedBox(),
                            ),
                            vS16,
                            Text(
                              'تواصل معنا عبر',
                              style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                            ),
                            Divider(indent: 8),
                            ListTile(
                              onTap: () {
                                launch("tel:" + widget.hall.phone);
                              },
                              leading: Icon(FontAwesome.phone, size: 24, color: AppColors.lightPurple),
                              title: Text(widget.hall.phone),
                            ),
                            ListTile(
                              onTap: () {
                                launch("mailto:" + widget.hall.email);
                              },
                              leading: Icon(Icons.email, size: 24, color: AppColors.lightPurple),
                              title: Text(widget.hall.email),
                            ),
                            Divider(indent: 8),
                            Text('الشروط والأحكام', style: defaultStyle(16, true)),
                            Text(widget.hall.rules ?? ''),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Offstage(
                  offstage: 1 != _tabIndex,
                  child: Column(
                    children: widget.hall.rooms.map<Widget>((Room room) => buildRoomWidget(room)).toList(),
                  ),
                ),
                Offstage(
                  offstage: 2 != _tabIndex,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('أسعار المناسبات', style: defaultStyle(16, true)),
                          ],
                        ),
                        Divider(indent: 8),
                        Column(
                          children: widget.hall.types.map<Widget>((Type type) => buildTypeWidget(type)).toList(),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('أسعار الاضافات', style: defaultStyle(16, true)),
                          ],
                        ),
                        Divider(indent: 8),
                        Column(
                          children: widget.hall.extras.map<Widget>((Extra extra) => buildExtraWidget(extra)).toList(),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildFooter() {
    return Row(
      children: <Widget>[
        Expanded(
          child: RaisedButton(
            color: AppColors.primary,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
            padding: EdgeInsets.symmetric(vertical: 8),
            onPressed: () {
              if (!Provider.of<AccountModel>(context, listen: false).isAuth()) {
                R.Router.navigator.pushNamed(R.Router.loginScreen);
              } else {
                if (widget.hallFilterArgs.dateTime != null && widget.hallFilterArgs.canReserve) {
                  R.Router.navigator.pushNamed(R.Router.newReservationScreen, arguments: HallFilterArgs(dateTime: widget.hallFilterArgs.dateTime, hall: widget.hall));
                } else {
                  R.Router.navigator.pushNamed(
                    R.Router.reservationCalender,
                    arguments: HallFilterArgs(hall: widget.hall),
                  );
                }
              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  FarhatyApp.calendar_check,
                  color: Colors.white,
                  size: 21,
                ),
                hS8,
                widget.hallFilterArgs.canReserve != null && widget.hallFilterArgs.canReserve && widget.hallFilterArgs.dateTime != null
                    ? Text("احجز يوم " + widget.hallFilterArgs.dateTime.day.toString() + "-" + widget.hallFilterArgs.dateTime.month.toString() + " الآن !", style: TextStyle(color: Colors.white))
                    : Text("عرض المواعيد المتاحة", style: TextStyle(color: Colors.white)),
              ],
            ),
          ),
        ),
        hS32,
        hS8,
        IconButton(
          icon: Icon(
            FarhatyApp.share_alt,
            color: AppColors.lightPurple,
          ),
          onPressed: () {
            //Share.share('قم بتحميل تطبيق قفة : https://apps.apple.com/us/app/guffa/id1493237677');
            Share.share('قم بتحميل تطبيق برايدل : https://onelink.to/8xtp2j');
          },
        ),
      ],
    );
  }

  Widget buildRoomWidget(Room room) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 16, top: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(room.name, style: defaultStyle(16, true)),
            ],
          ),
        ),
        Divider(indent: 8),
        Container(
          height: 200,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: widget.hallImages.length,
            itemBuilder: (context, index) {
              return widget.hallImages.elementAt(index).roomId == room.id
                  ? InkWell(
                      splashColor: Theme.of(context).accentColor.withOpacity(0.8),
                      highlightColor: Colors.transparent,
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (_) {
                          return DetailImage(heroTag: widget.hallImages.elementAt(index).id.toString(), image: widget.hallImages.elementAt(index).imageUrl);
                        }));
                      },
                      child: widget.hallImages.elementAt(index) == null
                          ? LoadingOverlay()
                          : Container(
                              width: 250,
                              margin: EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 20),
                              decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(color: Theme.of(context).accentColor.withOpacity(0.1), blurRadius: 15, offset: Offset(0, 5)),
                                ],
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                                child: CachedNetworkImage(
                                  fit: BoxFit.cover,
                                  imageUrl: widget.hallImages.elementAt(index).imageUrl,
                                  errorWidget: (context, url, error) => Icon(Icons.error),
                                ),
                              ),
                            ),
                    )
                  : SizedBox();
            },
          ),
        ),
      ],
    );
  }

  Widget buildExtraWidget(Extra extra) {
    return Card(
      child: ListTile(
        title: Text(extra.name),
        leading: Icon(
          Icons.add,
          color: AppColors.primary,
        ),
        trailing: Text(extra.price.toString() + ' د.ل '),
      ),
    );
  }

  Widget buildTypeWidget(Type type) {
    return Card(
      child: ExpansionTile(
        backgroundColor: Colors.white,
        leading: Icon(
          FarhatyApp.rings_wedding,
          color: AppColors.primary,
        ),
        title: Text(type.name),
        children: <Widget>[
          ListTile(
            title: Text('يوم السبت'),
            trailing: Text(type.satPrice.toString() + ' د.ل '),
          ),
          ListTile(
            title: Text('يوم الأحد'),
            trailing: Text(type.sunPrice.toString() + ' د.ل '),
          ),
          ListTile(
            title: Text('يوم الاتنين'),
            trailing: Text(type.monPrice.toString() + ' د.ل '),
          ),
          ListTile(
            title: Text('يوم التلاتاء'),
            trailing: Text(type.tuePrice.toString() + ' د.ل '),
          ),
          ListTile(
            title: Text('يوم الاربعاء'),
            trailing: Text(type.wedPrice.toString() + ' د.ل '),
          ),
          ListTile(
            title: Text('يوم الخميس'),
            trailing: Text(type.thrPrice.toString() + ' د.ل '),
          ),
          ListTile(
            title: Text('يوم الجمعة'),
            trailing: Text(type.friPrice.toString() + ' د.ل '),
          ),
        ],
      ),
    );
  }
}
