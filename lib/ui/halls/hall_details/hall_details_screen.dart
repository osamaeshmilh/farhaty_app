import 'package:farhaty_app/core/provider/change_notifier_provider_widget.dart';
import 'package:farhaty_app/core/provider/state_consumer.dart';
import 'package:farhaty_app/models/hall_filter_args.dart';
import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/ui/halls/hall_details/hall_details_model.dart';
import 'package:farhaty_app/ui/halls/hall_details/hall_details_state.dart';
import 'package:farhaty_app/ui/halls/hall_details/hall_details_widget.dart';
import 'package:farhaty_app/ui/widgets/error_widget.dart';
import 'package:farhaty_app/ui/widgets/loading_widget.dart';
import 'package:flutter/material.dart';

class HallDetailsScreen extends ChangeNotifierProviderWidget<HallDetailsModel> {
  final HallFilterArgs hallFilterArgs;
  final queryData = Map<String, dynamic>();

  HallDetailsScreen(this.hallFilterArgs);

  @override
  onModelReady(HallDetailsModel model) {
    queryData..['hallId.equals'] = hallFilterArgs.hallId.toString();
    model.query(queryData: queryData);
  }

  @override
  Widget build(BuildContext context, HallDetailsModel hallModel) {
    return Scaffold(
      backgroundColor: AppColors.background,
      body: StateConsumer<HallDetailsModel>(builder: (model, state) {
        if (state is HallDetailsLoading) return LoadingWidget();
        if (state is HallDetailsLoaded)
          return HallDetailsWidget(
              state.hall, state.hallImages, hallFilterArgs);
        return ErrorRetryWidget(onRetry: model.query(queryData: queryData));
      }),
    );
  }
}
