import 'package:farhaty_app/core/provider/view_model.dart';
import 'package:farhaty_app/core/state/list_state.dart';
import 'package:farhaty_app/data/remote/services/hall_images_service.dart';
import 'package:farhaty_app/data/remote/services/hall_service.dart';
import 'package:farhaty_app/ui/halls/hall_details/hall_details_state.dart';
import 'package:injectable/injectable.dart';
import 'package:dio/dio.dart';

@injectable
class HallDetailsModel extends ChangeNotiferModel<HallDetailsState> {
  final HallService hallService;
  final HallImagesService hallImagesService;

  HallDetailsModel(this.hallService, this.hallImagesService);

  @override
  HallDetailsState get initialState => HallDetailsInitial();

  query({Map<String, dynamic> queryData}) async {
    try {
      //await PreferenceManager.getToken().then((v) => {print(v)});

      state = HallDetailsLoading();
      final hall = await hallService.getOne(queryData['hallId.equals'].toString());
      final hallImages = await hallImagesService.queryAll(queryData: queryData);
      final roomImages = await hallImagesService.queryAll(queryData: queryData);

      state = HallDetailsLoaded(hall, hallImages, null);
    } on DioError {
    }
  }
}