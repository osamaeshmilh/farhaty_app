import 'package:cached_network_image/cached_network_image.dart';
import 'package:farhaty_app/models/hall_image.dart';
import 'package:farhaty_app/ui/widgets/DetailImage.dart';
import 'package:flutter/material.dart';

class HallImagesWidget extends StatefulWidget {
  final HallImage hallImage;
  final Function(num id) onTap;

  const HallImagesWidget(this.hallImage, {this.onTap});

  @override
  _HallImagesWidgetState createState() => _HallImagesWidgetState();
}

class _HallImagesWidgetState extends State<HallImagesWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 0),
      child: InkWell(
        child: Stack(
          children: <Widget>[
            CachedNetworkImage(
              imageUrl: widget.hallImage.imageUrl,
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: 60,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      Color(0xfffafafa),
                      Color(0xfffafafa),
                      Color(0xfffafafa),
                    ]),
                  ),
                ))
          ],
        ),
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (_) {
            return DetailImage(heroTag: widget.hallImage.id.toString(), image: widget.hallImage.imageUrl);
          }));
          //Navigator.of(context).pushNamed(ProductsScreen.route, arguments: ProductsFilterArguments(bannerId: widget.banner.id));
        },
      ),
    );
  }
}
