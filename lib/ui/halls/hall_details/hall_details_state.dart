import 'package:farhaty_app/models/managed_user_vm.dart';
import 'package:farhaty_app/models/hall.dart';
import 'package:farhaty_app/models/hall_image.dart';
import 'package:farhaty_app/models/room_image.dart';

abstract class HallDetailsState{}

class HallDetailsInitial extends HallDetailsState {}

class HallDetailsLoaded extends HallDetailsState{
  final Hall hall;
  final List<HallImage> hallImages;
  final List<RoomImage> roomImages;

  HallDetailsLoaded(this.hall, this.hallImages, this.roomImages);
}

class HallDetailsLoading extends HallDetailsState{}