import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/res/styles.dart';
import 'package:farhaty_app/utils/farhaty_app_icons.dart';
import 'package:flutter/material.dart';

class EmptyHallWidget extends StatelessWidget {
  final String message;

  const EmptyHallWidget({Key key, this.message = "لايوجد صالات افراح متاحة"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            FarhatyApp.landmark,
            size: 70,
            color: AppColors.primary.withOpacity(0.5),
          ),
          vS16,
          Text(message),
        ],
      ),
    );
  }
}
