import 'package:cached_network_image/cached_network_image.dart';
import 'package:farhaty_app/core/provider/change_notifier_provider_widget.dart';
import 'package:farhaty_app/core/provider/state_consumer.dart';
import 'package:farhaty_app/core/state/list_state.dart';
import 'package:farhaty_app/models/hall.dart';
import 'package:farhaty_app/models/hall_filter_args.dart';
import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/routes/router.gr.dart' as R;
import 'package:farhaty_app/ui/home/pages/halls/halls_model.dart';
import 'package:farhaty_app/ui/widgets/error_widget.dart';
import 'package:farhaty_app/ui/widgets/list_builder.dart';
import 'package:farhaty_app/ui/widgets/loading_widget.dart';
import 'package:farhaty_app/utils/farhaty_app_icons.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'empty_halls_widget.dart';

class HallsScreen extends ChangeNotifierProviderWidget<HallsModel> {
  final HallFilterArgs hallFilterArgs;
  final queryData = Map<String, dynamic>();
  DateTime dateTime;

  HallsScreen(this.hallFilterArgs);

  @override
  onModelReady(HallsModel model) {
    if (hallFilterArgs != null) queryData..['vip.equals'] = hallFilterArgs?.vip;
    if (hallFilterArgs != null) queryData..['requestedDate.equals'] = hallFilterArgs?.date;
    if (hallFilterArgs != null) dateTime = hallFilterArgs?.dateTime;

    model.query(queryData: queryData);
  }

  @override
  Widget build(BuildContext context, HallsModel hallModel) {
    return Scaffold(
      appBar: AppBar(
        title: Material(
          borderRadius: BorderRadius.all(Radius.circular(32.0)),
          child: SizedBox(
            height: 40,
            child: TextField(
              onChanged: (text) => queryData..['name.contains'] = text,
              cursorColor: AppColors.primary,
              decoration: InputDecoration(
                hintText: 'بحث ...',
                contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 2.0),
                suffixIcon: Padding(
                  padding: const EdgeInsets.only(left: 2),
                  child: IconButton(
                    onPressed: () {
                      dateTime = null;
                      hallModel.query(queryData: queryData);
                    },
                    icon: Icon(Icons.search, color: AppColors.primary),
                  ),
                ),
                border: InputBorder.none,
              ),
            ),
          ),
        ),
        actions: <Widget>[
          InkWell(
            onTap: () {
              showDatePicker(
                context: context,
                locale: Locale('en'),
                initialDate: dateTime,
                firstDate: DateTime(2017, 8),
                lastDate: DateTime(2040),
              ).then((value) {
                final DateTime now = value;
                final DateFormat formatter = DateFormat('yyyy-MM-dd');
                final String date = formatter.format(now);
                dateTime = value;
                queryData..['requestedDate.equals'] = date;
                hallModel.query(queryData: queryData);
              });
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 16.0, top: 3, bottom: 3),
              child: Icon(
                FarhatyApp.calendar_check,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
      body: StateConsumer<HallsModel>(builder: (model, state) {
        if (state is ListLoading) return LoadingWidget();
        if (state is ListLoaded) return buildList(state.items, context);
        return ErrorRetryWidget(onRetry: model.query(queryData: queryData));
      }),
    );
  }

  Widget buildList(List<Hall> halls, BuildContext context) {
    return ListBuilder<Hall>(
      placeHolder: EmptyHallWidget(),
      padding: EdgeInsets.only(top: 16),
      items: halls,
      builder: (hall) => Stack(
        children: <Widget>[
          Card(
            elevation: 0.5,
            margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
            child: ListTile(
              contentPadding: EdgeInsetsDirectional.only(start: 100),
              title: Text(
                hall.name,
                overflow: TextOverflow.fade,
                softWrap: false,
                maxLines: 1,
              ),
              subtitle: Text(
                hall.address,
                overflow: TextOverflow.fade,
                softWrap: false,
                maxLines: 2,
              ),
              trailing: dateTime != null && hall.canReserve != null
                  ? hall.canReserve
                      ? Text(dateTime.month.toString() + "-" + dateTime.day.toString() + " متاح ", style: TextStyle(color: Colors.green))
                      : Text(
                          dateTime.month.toString() + "-" + dateTime.day.toString() + " غير متاح ",
                          style: TextStyle(color: Colors.red),
                        )
                  : SizedBox(),
              onTap: () {
                R.Router.navigator.pushNamed(
                  R.Router.hallDetailsScreen,
                  arguments: HallFilterArgs(hallId: hall.id, dateTime: dateTime, canReserve: hall.canReserve),
                );
              },
            ),
          ),
          Positioned(
            right: 24,
            top: 0,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: CachedNetworkImage(
                imageUrl: hall.logoUrl,
                width: 80,
                height: 80,
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
