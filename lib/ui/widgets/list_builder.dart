import 'package:farhaty_app/ui/widgets/place_holder_widget.dart';
import 'package:flutter/cupertino.dart';

class ListBuilder<T> extends StatelessWidget {
  final List<T> items;
  final Widget Function(T item) builder;
  final Widget placeHolder;
  final Axis direction;
  final bool shrinkWrap;
  final EdgeInsets padding;

  const ListBuilder({
    Key key,
    this.items,
    this.builder,
    this.padding,
    this.placeHolder = const PlaceHolderWidget(),
    this.direction = Axis.vertical,
    this.shrinkWrap = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return items.isEmpty
        ? placeHolder
        : ListView.builder(
            scrollDirection: direction,
            padding: padding,
            shrinkWrap: shrinkWrap,
            itemCount: items.length,
            itemBuilder: (_, index) => builder(items[index]),
          );
  }
}
