import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'loading_overlay.dart';


class LoaderContainer extends StatelessWidget {
  final bool isLoading;
  final Widget child;
  final Widget loadingWidget;

  LoaderContainer({@required this.child, @required this.isLoading, this.loadingWidget = const LoadingOverlay()});

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      child,
      if (isLoading) ...[Positioned.fill(child: loadingWidget)]
    ]);
  }
}
