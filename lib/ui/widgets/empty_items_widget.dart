import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/res/styles.dart';
import 'package:farhaty_app/utils/farhaty_app_icons.dart';
import 'package:flutter/material.dart';

class EmptyItemsWidget extends StatelessWidget {
  final String message;

  const EmptyItemsWidget(
      {Key key, this.message = "لايوجد منتجات او خدمات متاحة"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            FarhatyApp.rings_wedding,
            size: 70,
            color: AppColors.light,
          ),
          vS16,
          Text(message),
        ],
      ),
    );
  }
}
