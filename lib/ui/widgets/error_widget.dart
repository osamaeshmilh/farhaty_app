import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/res/styles.dart';
import 'package:flutter/material.dart';

class ErrorRetryWidget extends StatelessWidget {
  final Function() onRetry;

  const ErrorRetryWidget({Key key, @required this.onRetry}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.wifi,
              size: 120,
              color: AppColors.accent.withOpacity(0.7),
            ),
            vS16,
            Text('فشل تحميل البيانات'),
            RaisedButton(
              onPressed: onRetry,
              child: Text("اعادة المحاولة"),
            )
          ],
        ),
      ),
    );
  }
}
