import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class LoadingWidget extends StatelessWidget {
  const LoadingWidget();
  @override
  Widget build(BuildContext context) {
    var x =
        ((MediaQuery.of(context).size.height - kToolbarHeight) / 70).floor();
    print(x);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        elevation: 0.5,
      ),
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
        child: Shimmer.fromColors(
          baseColor: Colors.grey[200],
          highlightColor: Colors.grey[100],
          direction: ShimmerDirection.rtl,
          enabled: true,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: List.generate(
                ((MediaQuery.of(context).size.height) / 70).floor() - 2,
                (_) => Container(
                      height: 50,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          bone(width: 48.0, height: 48.0),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 8.0),
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                bone(width: double.infinity, height: 8.0),
                                SizedBox(height: 8),
                                bone(width: double.infinity, height: 8.0),
                                SizedBox(height: 8),
                                bone(width: 80.0, height: 8.0),
                              ],
                            ),
                          )
                        ],
                      ),
                    )).toList(),
          ),
        ),
      ),
    );
  }

  Widget bone({double width, double height, double radius = 12}) => Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(radius)));
}
