import 'package:farhaty_app/res/app_colors.dart';
import 'package:flutter/material.dart';

class PlaceHolderWidget extends StatelessWidget {
  final String message;

  const PlaceHolderWidget({Key key, this.message = "لا توجد بيانات"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.list,
            size: 70,
            color: AppColors.light,
          ),
          Text(message),
        ],
      ),
    );
  }
}
