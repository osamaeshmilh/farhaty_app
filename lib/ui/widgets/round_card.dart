import 'package:flutter/material.dart';

class RoundCard extends StatelessWidget {
  final Color color;
  final double radius;
  final double verticalPadding;
  final double horizontalPadding;
  final double verticalMargin;
  final double horizontalMargin;
  final double elevation;
  final Widget child;
  final EdgeInsets padding;
  final double width;

  RoundCard(
      {this.color = Colors.transparent,
      this.radius = 16.0,
      this.verticalPadding = 8.0,
      this.horizontalPadding = 8.0,
      this.verticalMargin = 8.0,
      this.horizontalMargin = 16.0,
      this.padding,
      this.width,
      this.elevation = 0.5,
      this.child});

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: elevation,
        clipBehavior: Clip.antiAlias,
        margin: EdgeInsets.symmetric(vertical: verticalMargin, horizontal: horizontalMargin),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(radius)),
        child: Container(
          width: width,
          color: this.color ?? Colors.white,
          padding: padding ?? EdgeInsets.symmetric(vertical: verticalPadding, horizontal: horizontalPadding),
          child: child,
        ));
  }
}
