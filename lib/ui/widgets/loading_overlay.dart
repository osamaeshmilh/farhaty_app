import 'package:flutter/material.dart';

class LoadingOverlay extends StatelessWidget {
  const LoadingOverlay();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        width: double.infinity,
        color: Colors.black26,
        child: Center(child: CircularProgressIndicator()),
      ),
    );
  }
}
