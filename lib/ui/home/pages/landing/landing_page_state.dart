import 'package:farhaty_app/models/banner.dart';
import 'package:farhaty_app/models/category.dart';
import 'package:farhaty_app/models/store.dart';

abstract class LandingPageState {}

class LandingPageUninitialized extends LandingPageState {}

class LandingPageLoading extends LandingPageState {}

class LandingPageLoaded extends LandingPageState {
  final List<Banner> banners;
  final List<Category> categories;
  final List<Store> vips;

  LandingPageLoaded({this.categories, this.banners, this.vips});
}

class LandingPageError extends LandingPageState {}
