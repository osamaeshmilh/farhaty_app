import 'package:cached_network_image/cached_network_image.dart';
import 'package:farhaty_app/models/banner.dart' as ban;
import 'package:farhaty_app/routes/router.gr.dart' as R;
import 'package:flutter/material.dart';

class BannerWidget extends StatefulWidget {
  final ban.Banner banner;
  final Function(num id) onTap;

  const BannerWidget(this.banner, {this.onTap});

  @override
  _BannerWidgetState createState() => _BannerWidgetState();
}

class _BannerWidgetState extends State<BannerWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 0),
      child: InkWell(
        child: CachedNetworkImage(
          imageUrl: widget.banner.imageUrl,
          imageBuilder: (context, imageProvider) => Container(
            margin: EdgeInsets.all(16),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
              ),
            ),
          ),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ),
        onTap: () {
          if (widget.banner.storeId != null) Navigator.of(context).pushNamed(R.Router.storeDetailsScreen, arguments: widget.banner.storeId.toString());
        },
      ),
    );
  }
}
