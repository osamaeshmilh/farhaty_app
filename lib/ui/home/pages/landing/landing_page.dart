import 'package:cached_network_image/cached_network_image.dart';
import 'package:farhaty_app/core/provider/change_notifier_provider_widget.dart';
import 'package:farhaty_app/core/provider/state_consumer.dart';
import 'package:farhaty_app/models/store.dart';
import 'package:farhaty_app/models/store_filter_args.dart';
import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/res/styles.dart';
import 'package:farhaty_app/routes/router.gr.dart' as R;
import 'package:farhaty_app/ui/home/pages/landing/landing_page_state.dart';
import 'package:farhaty_app/ui/widgets/error_widget.dart';
import 'package:farhaty_app/ui/widgets/list_builder.dart';
import 'package:farhaty_app/ui/widgets/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

import 'banner_widget.dart';
import 'landing_model.dart';

class LandingPage extends ChangeNotifierProviderWidget<LandingModel> {
  @override
  onModelReady(LandingModel model) {
    model.fetch();
  }

  @override
  Widget build(BuildContext context, LandingModel model) {
    return StateConsumer<LandingModel>(builder: (model, state) {
      if (state is LandingPageLoading) return LoadingWidget();
      if (state is LandingPageLoaded) return buildSliverBody(state, context);
      return ErrorRetryWidget(onRetry: model.fetch);
    });
  }

  Widget buildSliverBody(LandingPageLoaded state, BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverList(
          delegate: SliverChildListDelegate([
            Container(
              height: 220,
              child: Swiper(
                itemCount: state.banners.length,
                // transformer: PageTransformer(),
                itemHeight: 200.0,
                itemWidth: MediaQuery.of(context).size.width - 60,
                autoplay: true,
                autoplayDelay: 5000,
                itemBuilder: (_, int index) => BannerWidget(state.banners[index]),
              ),
            ),
            vS8,
            Padding(
              padding: const EdgeInsets.only(right: 20.0, left: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('عروض خاصة'),
                  FlatButton(
                    onPressed: () {
                      R.Router.navigator.pushNamed(R.Router.storesScreen, arguments: StoreFilterArgs(vip: true));
                    },
                    child: Text(
                      'عرض الكل',
                      style: TextStyle(color: AppColors.primary),
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              indent: 16,
              endIndent: 16,
            ),
            Container(
              margin: EdgeInsets.only(right: 4, top: 0, bottom: 6),
              height: 120,
              child: buildVipList(context, state.vips),
            ),
            vS8,
            Padding(
              padding: const EdgeInsets.only(right: 20.0, left: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('التصنيفات'),
                  FlatButton(
                    onPressed: () {
                      R.Router.navigator.pushNamed(
                        R.Router.storesScreen,
                      );
                    },
                    child: Text(
                      'عرض الكل',
                      style: TextStyle(color: AppColors.primary),
                    ),
                  ),
                ],
              ),
            ),
            Divider(indent: 16)
          ]),
        ),
        SliverPadding(
          padding: const EdgeInsets.symmetric(horizontal: 2),
          sliver: SliverList(
            delegate: SliverChildBuilderDelegate(
              (_, index) {
                final category = state.categories[index];
                return InkWell(
                  child: Card(
                    margin: const EdgeInsets.fromLTRB(16, 10, 16, 0),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6.0)),
                    child: Stack(
                      children: <Widget>[
                        CachedNetworkImage(
                          imageUrl: category.imageUrl,
                          imageBuilder: (context, imageProvider) => Container(
                            height: 200,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6.0),
                              image: DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          errorWidget: (context, url, error) => Icon(Icons.error),
                        ),
                        Positioned(
                            bottom: 0,
                            left: 0,
                            right: 0,
                            child: Container(
                              padding: const EdgeInsets.symmetric(vertical: 2),
                              child: Center(
                                  child: Text(
                                category.name,
                                overflow: TextOverflow.fade,
                                softWrap: false,
                                maxLines: 1,
                                style: white(16, true),
                              )),
                              decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [Colors.black12, Colors.black26, Colors.black12]),
                              ),
                            ))
                      ],
                    ),
                  ),
                  onTap: () {
                    R.Router.navigator.pushNamed(R.Router.storesScreen, arguments: StoreFilterArgs(categoryId: category.id));
                  },
                );
              },
              childCount: state.categories.length,
            ),
          ),
        )
      ],
    );
  }

  buildVipList(BuildContext context, List<Store> stores) {
    return stores != null
        ? ListBuilder<Store>(
            items: stores,
            direction: Axis.horizontal,
            builder: (Store store) => Container(
              width: 160.0,
              margin: EdgeInsets.only(top: 0, right: 5, left: 5, bottom: 5),
              child: InkWell(
                child: Card(
                  margin: const EdgeInsets.fromLTRB(0, 10, 16, 0),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                  child: Stack(
                    children: <Widget>[
                      CachedNetworkImage(
                        imageUrl: store.logoUrl,
                        imageBuilder: (context, imageProvider) => Container(
                          height: 110,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12.0),
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                      Positioned(
                          bottom: 0,
                          left: 0,
                          right: 0,
                          child: Container(
                            padding: const EdgeInsets.symmetric(vertical: 2),
                            child: Center(
                                child: Text(
                              store.name,
                              overflow: TextOverflow.fade,
                              softWrap: false,
                              maxLines: 1,
                              style: white(14, true),
                            )),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12.0),
                              gradient: LinearGradient(colors: [Colors.black26, Colors.black45, Colors.black26]),
                            ),
                          ))
                    ],
                  ),
                ),
                onTap: () {
                  R.Router.navigator.pushNamed(R.Router.storeDetailsScreen, arguments: store.id.toString());
                },
              ),
            ),
          )
        : SizedBox();
  }
}
