import 'package:farhaty_app/core/provider/view_model.dart';
import 'package:farhaty_app/data/remote/services/banner_service.dart';
import 'package:farhaty_app/data/remote/services/category_service.dart';
import 'package:farhaty_app/data/remote/services/store_service.dart';
import 'package:farhaty_app/ui/home/pages/landing/landing_page_state.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@injectable
class LandingModel extends ChangeNotiferModel<LandingPageState> {
  final BannerService bannerService;
  final CategoryService categoryService;
  final StoreService storeService;

  LandingModel(this.bannerService, this.categoryService, this.storeService);

  @override
  LandingPageState get initialState => LandingPageUninitialized();

  fetch() async {
    try {
      //await PreferenceManager.getToken().then((v) => {print(v)});

      final queryData = Map<String, dynamic>();
      queryData..['vip.equals'] = true;

      state = LandingPageLoading();
      final res = await Future.wait([
        bannerService.fetchAll(),
        categoryService.fetchAll(),
        storeService.queryAll(queryData: queryData),
      ]);

      state = LandingPageLoaded(banners: res[0], categories: res[1], vips: res[2]);
    } on DioError {
      state = LandingPageError();
    }
  }
}
