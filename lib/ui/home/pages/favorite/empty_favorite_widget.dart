import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/res/styles.dart';
import 'package:farhaty_app/utils/farhaty_app_icons.dart';
import 'package:flutter/material.dart';

class EmptyFavoriteWidget extends StatelessWidget {
  final String message;

  const EmptyFavoriteWidget({Key key, this.message = "لاتوجد عناصر بالمفضلة"}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(FarhatyApp.heart, size: 70, color: AppColors.primary.withOpacity(0.7),),
          vS16,
          Text(message),
        ],
      ),
    );
  }
}
