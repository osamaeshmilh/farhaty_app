import 'package:event_bus/event_bus.dart';
import 'package:farhaty_app/core/provider/view_model.dart';
import 'package:farhaty_app/core/state/list_state.dart';
import 'package:farhaty_app/data/local/dao/favorite_dao.dart';
import 'package:injectable/injectable.dart';

@injectable
class FavoritesModel extends ChangeNotiferModel<ListState> {
  final FavoriteDao favoriteDao;

  FavoritesModel(this.favoriteDao, EventBus eventBus) {
    eventBus.on<LoadFavoritesListEvent>().listen((_) => loadFavorites());
  }

  @override
  ListState get initialState => ListUninitialized();

  loadFavorites() async {
    try {
      state = ListLoading();
      final favorites = await favoriteDao.getAll();
      state = ListLoaded(favorites);
    } catch (err) {
      ListError();
    }
  }

  Future updateCartItem(favorite) {
    return favoriteDao.update(favorite).then((_) => loadFavorites());
  }

  Future deleteAll() {
    return favoriteDao.deleteAll().then((_) => loadFavorites());
  }
}

class LoadFavoritesListEvent {}
