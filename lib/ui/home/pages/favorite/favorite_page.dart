import 'package:farhaty_app/core/provider/change_notifier_provider_widget.dart';
import 'package:farhaty_app/core/state/list_state.dart';
import 'package:farhaty_app/ui/widgets/error_widget.dart';
import 'package:farhaty_app/ui/widgets/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:farhaty_app/core/provider/state_consumer.dart';

import 'favorite_model.dart';
import 'favorite_widget.dart';

class FavoritesPage extends ChangeNotifierProviderWidget<FavoritesModel> {
  @override
  onModelReady(FavoritesModel model) {
    model.loadFavorites();
  }

  @override
  Widget build(BuildContext context, model) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: StateConsumer<FavoritesModel>(
        builder: (model, state) {
          if (state is ListLoading) return LoadingWidget();
          if (state is ListLoaded) return FavoriteWidget(state.items);
          return ErrorRetryWidget(onRetry: model.loadFavorites());
        },
      ),
    );
  }
}
