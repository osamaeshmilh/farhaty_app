import 'package:cached_network_image/cached_network_image.dart';
import 'package:farhaty_app/models/favorite.dart';
import 'package:farhaty_app/routes/router.gr.dart' as R;
import 'package:farhaty_app/ui/stores/store_model.dart';
import 'package:farhaty_app/ui/widgets/list_builder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'empty_favorite_widget.dart';

class FavoriteWidget extends StatefulWidget {
  final List<Favorite> favorites;

  const FavoriteWidget(this.favorites);

  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  @override
  Widget build(BuildContext context) {
    final storeModel = Provider.of<StoreModel>(context, listen: false);

    return ListBuilder<Favorite>(
      placeHolder: EmptyFavoriteWidget(),
      padding: EdgeInsets.only(top: 16),
      items: widget.favorites,
      builder: (favorite) => Card(
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        child: Dismissible(
          key: Key(favorite.store.name),
          onDismissed: (direction) {
            setState(() {
              widget.favorites.remove(favorite);
            });
            storeModel.toggleFavoriteStatus(Favorite(store: favorite.store, isFavorite: true));
            Scaffold.of(context).showSnackBar(SnackBar(content: Text("تمت الازالة من المفضلة")));
          },
          child: Stack(
            children: <Widget>[
              Card(
                elevation: 0.5,
                margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
                clipBehavior: Clip.antiAlias,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                child: ListTile(
                  contentPadding: EdgeInsetsDirectional.only(start: 100),
                  title: Text(favorite.store.name),
                  subtitle: Text(favorite.store.address),
                  onTap: () {
                    R.Router.navigator.pushNamed(
                      R.Router.storeDetailsScreen,
                      arguments: favorite.store.id.toString(),
                    );
                  },
                ),
              ),
              Positioned(
                right: 24,
                top: 0,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: CachedNetworkImage(
                    imageUrl: favorite.store.logoUrl,
                    width: 80,
                    height: 80,
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
