import 'package:cached_network_image/cached_network_image.dart';
import 'package:farhaty_app/core/provider/change_notifier_provider_widget.dart';
import 'package:farhaty_app/core/provider/state_consumer.dart';
import 'package:farhaty_app/core/state/list_state.dart';
import 'package:farhaty_app/models/hall.dart';
import 'package:farhaty_app/models/hall_filter_args.dart';
import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/res/styles.dart';
import 'package:farhaty_app/routes/router.gr.dart' as R;
import 'package:farhaty_app/ui/widgets/error_widget.dart';
import 'package:farhaty_app/ui/widgets/list_builder.dart';
import 'package:farhaty_app/ui/widgets/loading_widget.dart';
import 'package:farhaty_app/utils/farhaty_app_icons.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'empty_halls_page_widget.dart';
import 'halls_model.dart';

class HallsPage extends ChangeNotifierProviderWidget<HallsModel> {
  final queryData = Map<String, dynamic>();

  @override
  onModelReady(HallsModel model) {
    model.query(queryData: queryData);
  }

  @override
  Widget build(BuildContext context, HallsModel model) {
    return Scaffold(
      body: StateConsumer<HallsModel>(
        builder: (model, state) {
          if (state is ListLoading) return LoadingWidget();
          if (state is ListLoaded) return buildSliverBody(state, context);
          return ErrorRetryWidget(onRetry: model.query);
        },
      ),
    );
  }

  Widget buildSliverBody(ListLoaded state, BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverList(
          delegate: SliverChildListDelegate([
            buildHeader(context),
            vS8,
            Padding(
              padding: const EdgeInsets.only(right: 20.0, left: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('عروض خاصة'),
                  FlatButton(
                    onPressed: () {
                      R.Router.navigator.pushNamed(R.Router.hallsScreen, arguments: HallFilterArgs(vip: true));
                    },
                    child: Text(
                      'عرض الكل',
                      style: TextStyle(color: AppColors.primary),
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              indent: 16,
              endIndent: 16,
            ),
            Container(
              margin: EdgeInsets.only(right: 4, top: 0, bottom: 6),
              height: 120,
              child: buildVibList(context, state.items),
            ),
            vS8,
            Padding(
              padding: const EdgeInsets.only(right: 20.0, left: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('صالات الأفراح'),
                  FlatButton(
                    onPressed: () {
                      R.Router.navigator.pushNamed(
                        R.Router.hallsScreen,
                      );
                    },
                    child: Text(
                      'عرض الكل',
                      style: TextStyle(color: AppColors.primary),
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              indent: 16,
              endIndent: 16,
            ),
            if (state.items.isEmpty)
              Padding(
                padding: const EdgeInsets.only(top: 50.0),
                child: EmptyHallPageWidget(),
              ),
          ]),
        ),
        SliverPadding(
          padding: const EdgeInsets.symmetric(horizontal: 2),
          sliver: SliverList(
            delegate: SliverChildBuilderDelegate(
              (_, index) {
                final Hall hall = state.items[index];

                return InkWell(
                  child: Card(
                    margin: const EdgeInsets.fromLTRB(16, 10, 16, 0),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6.0)),
                    child: Stack(
                      children: <Widget>[
                        CachedNetworkImage(
                          imageUrl: hall.logoUrl,
                          imageBuilder: (context, imageProvider) => Container(
                            height: 200,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6.0),
                              image: DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          errorWidget: (context, url, error) => Icon(Icons.error),
                        ),
                        Positioned(
                            bottom: 0,
                            left: 0,
                            right: 0,
                            child: Container(
                              padding: const EdgeInsets.symmetric(vertical: 2),
                              child: Center(
                                  child: Text(
                                hall.name,
                                overflow: TextOverflow.fade,
                                softWrap: false,
                                maxLines: 1,
                                style: white(16, true),
                              )),
                              decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [Colors.black12, Colors.black26, Colors.black12]),
                              ),
                            ))
                      ],
                    ),
                  ),
                  onTap: () {
                    R.Router.navigator.pushNamed(R.Router.hallDetailsScreen, arguments: HallFilterArgs(hallId: hall.id));
                  },
                );
              },
              childCount: state.items.length,
            ),
          ),
        )
      ],
    );
  }

  Widget buildVibList(BuildContext context, List<Hall> halls) {
    return ListBuilder<Hall>(
      items: halls,
      direction: Axis.horizontal,
      builder: (Hall hall) => Container(
        width: 160.0,
        margin: EdgeInsets.only(top: 0, right: 5, left: 5, bottom: 5),
        child: InkWell(
          child: Card(
            margin: const EdgeInsets.fromLTRB(0, 10, 16, 0),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
            child: Stack(
              children: <Widget>[
                CachedNetworkImage(
                  imageUrl: hall.logoUrl,
                  imageBuilder: (context, imageProvider) => Container(
                    height: 110,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12.0),
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
                Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: Container(
                      padding: const EdgeInsets.symmetric(vertical: 2),
                      child: Center(
                          child: Text(
                        hall.name,
                        overflow: TextOverflow.fade,
                        softWrap: false,
                        maxLines: 1,
                        style: white(14, true),
                      )),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12.0),
                        gradient: LinearGradient(colors: [Colors.black26, Colors.black45, Colors.black26]),
                      ),
                    ))
              ],
            ),
          ),
          onTap: () {
            R.Router.navigator.pushNamed(R.Router.hallDetailsScreen, arguments: HallFilterArgs(hall: hall, hallId: hall.id));
          },
        ),
      ),
    );
  }

  Widget buildHeader(context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(bottomRight: Radius.circular(40), bottomLeft: Radius.circular(40)),
      child: Container(
        decoration: BoxDecoration(color: AppColors.primary),
        alignment: Alignment.center,
        padding: EdgeInsets.only(bottom: 40.0, top: 20),
        child: Column(
          children: <Widget>[
            vS16,
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 30.0),
              child: Material(
                elevation: 4.0,
                borderRadius: BorderRadius.all(
                  Radius.circular(30.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          R.Router.navigator.pushNamed(
                            R.Router.hallsScreen,
                          );
                        },
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(right: 8.0, top: 3, bottom: 3),
                              child: Icon(
                                FarhatyApp.search,
                                color: AppColors.primary,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 8.0, top: 3, bottom: 3),
                              child: Text(
                                "بحث عن قاعة أفراح أو مواعيد متاحة ...",
                                style: TextStyle(color: Colors.grey.shade600, fontSize: 14),
                              ),
                            ),
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          showDatePicker(
                            context: context,
                            locale: Locale('en'),
                            initialDate: DateTime.now(),
                            firstDate: DateTime(2017, 8),
                            lastDate: DateTime(2040),
                          ).then((value) {
                            final DateTime now = value;
                            final DateFormat formatter = DateFormat('yyyy-MM-dd');
                            final String date = formatter.format(now);
                            R.Router.navigator.pushNamed(R.Router.hallsScreen, arguments: HallFilterArgs(date: date, dateTime: value));
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(left: 8.0, top: 3, bottom: 3),
                          child: Icon(
                            FarhatyApp.calendar_check,
                            color: AppColors.primary,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

//  Widget buildList(List<Category> items, BuildContext context) {
//    return ListBuilder<Category>(
//      items: items,
//      builder: (category) => CategoryWidget(
//        category,
//        onTap: (id) {
//          Navigator.of(context).pushNamed(ProductsScreen.route);
//        },
//      ),
//    );
//  }
}
