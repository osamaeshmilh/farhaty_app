import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/res/styles.dart';
import 'package:farhaty_app/utils/farhaty_app_icons.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class EmptyHallPageWidget extends StatelessWidget {
  final String message;

  const EmptyHallPageWidget({Key key, this.message = "قم بتحديد الموعد لعرض الصالات المتاحة"}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(FarhatyApp.search, size: 70, color: AppColors.primary,),
          vS16,
          Text(message),
        ],
      ),
    );
  }
}
