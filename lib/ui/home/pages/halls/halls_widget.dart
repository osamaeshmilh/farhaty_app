import 'package:cached_network_image/cached_network_image.dart';
import 'package:farhaty_app/models/hall.dart';
import 'package:farhaty_app/routes/router.gr.dart' as R;
import 'package:farhaty_app/ui/widgets/list_builder.dart';
import 'package:flutter/material.dart';

class HallWidget extends StatefulWidget {
  final List<Hall> halls;

  const HallWidget(this.halls);

  @override
  _HallWidgetState createState() => _HallWidgetState();
}

class _HallWidgetState extends State<HallWidget> {
  @override
  Widget build(BuildContext context) {
    return ListBuilder<Hall>(
        padding: EdgeInsets.only(top: 16),
        items: widget.halls,
        builder: (hall) => Stack(
              children: <Widget>[
                Card(
                  elevation: 0.5,
                  margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
                  clipBehavior: Clip.antiAlias,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: ListTile(
                    contentPadding: EdgeInsetsDirectional.only(start: 105),
                    title: Text(hall.name),
                    subtitle: Text(hall.address),
                    onTap: () {
                      R.Router.navigator.pushNamed(
                        R.Router.hallDetailsScreen,
                        arguments: hall.id.toString(),
                      );
                    },
                  ),
                ),
                Positioned(
                  right: 24,
                  top: 0,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: CachedNetworkImage(
                      imageUrl: hall.logoUrl,
                      width: 80,
                      height: 80,
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                ),
              ],
            ));

//    return CalendarCarousel<Event>(
//      locale: 'ar',
//      weekendTextStyle: TextStyle(
//        color: Colors.red,
//      ),
//      thisMonthDayBorderColor: Colors.grey,
//      customDayBuilder: (
//        /// you can provide your own build function to make custom day containers
//        bool isSelectable,
//        int index,
//        bool isSelectedDay,
//        bool isToday,
//        bool isPrevMonthDay,
//        TextStyle textStyle,
//        bool isNextMonthDay,
//        bool isThisMonthDay,
//        DateTime day,
//      ) {
//        /// If you return null, [CalendarCarousel] will build container for current [day] with default function.
//        /// This way you can build custom containers for specific days only, leaving rest as default.
//
//        // Example: every 15th of month, we have a flight, we can place an icon in the container like that:
//        if (day.day == 15) {
//          return Center(
//            child: Icon(Icons.local_airport),
//          );
//        } else {
//          return null;
//        }
//      },
//      weekFormat: false,
//      height: 420.0,
//      daysHaveCircularBorder: null,
//
//      /// null for not rendering any border, true for circular border, false for rectangular border
//    );
  }
}
