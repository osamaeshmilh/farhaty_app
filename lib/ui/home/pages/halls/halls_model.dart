import 'package:farhaty_app/core/provider/view_model.dart';
import 'package:farhaty_app/core/state/list_state.dart';
import 'package:farhaty_app/data/remote/services/account_service.dart';
import 'package:farhaty_app/data/remote/services/hall_service.dart';
import 'package:farhaty_app/models/hall.dart';
import 'package:injectable/injectable.dart';
import 'package:dio/dio.dart';

@injectable
class HallsModel extends ChangeNotiferModel<ListState> {
  final HallService hallService;

  HallsModel(this.hallService);

  @override
  ListState get initialState => ListUninitialized();

  query({Map<String, dynamic> queryData}) async {
    try {
      state = ListLoading();
      final halls = await hallService.queryAll(queryData: queryData);
      state = ListLoaded<Hall>(halls);
    } on DioError {
      state = ListError();
    }
  }
}
