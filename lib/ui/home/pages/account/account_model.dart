import 'package:farhaty_app/core/provider/view_model.dart';
import 'package:farhaty_app/data/local/preferences_manager.dart';
import 'package:farhaty_app/data/remote/services/account_service.dart';
import 'package:farhaty_app/models/jwt_token.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class AccountModel extends ChangeNotiferModel<AccountState> {
  final AccountService accountService;
  final PreferenceManager preferenceManager;

  AccountModel(this.accountService, this.preferenceManager);

  get initialState => AccountStateInitial();

  Future authenticate() async {
    if (accountService.isAuthenticated()) {
      state = AccountStateAuthenticated();
    } else {
      state = AccountStateUnAuthenticated();
    }
  }

  bool isAuth() {
    return accountService.isAuthenticated();
  }

  get isAuthenticated => state == AccountStateAuthenticated();

  submit({Map<String, dynamic> data}) async {}

  void logout() {
    preferenceManager.storeToken(JWTToken(null));
    state = AccountStateUnAuthenticated();
  }
}

abstract class AccountState {}

class AccountStateInitial extends AccountState {}

class AccountStateAuthenticated extends AccountState {}

class AccountStateUnAuthenticated extends AccountState {}
