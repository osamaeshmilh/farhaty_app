import 'package:farhaty_app/core/provider/change_notifier_provider_widget.dart';
import 'package:farhaty_app/core/provider/state_consumer.dart';
import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/res/styles.dart';
import 'package:farhaty_app/routes/router.gr.dart' as R;
import 'package:farhaty_app/ui/widgets/error_widget.dart';
import 'package:farhaty_app/utils/farhaty_app_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import 'account_model.dart';

class AccountPage extends ChangeNotifierProviderWidget<AccountModel> {
  final bool isLoggedIn = false;

  @override
  onModelReady(AccountModel model) {
    model.authenticate();
    print(model.hashCode);
  }

  @override
  Widget build(BuildContext context, AccountModel model) {
    return Container(
      child: StateConsumer<AccountModel>(builder: (model, state) {
        if (state is AccountStateUnAuthenticated) return notLoggedInWidget(context);
        if (state is AccountStateAuthenticated) return loggedInWidget(context);
        return ErrorRetryWidget(onRetry: model.authenticate);
      }),
    );
  }

  Widget loggedInWidget(BuildContext context) {
    return ListView(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(24),
          child: Text('حسابي'),
        ),
        ListTile(
          leading: Icon(Icons.date_range),
          title: Text('حجوزاتي'),
          trailing: Icon(Icons.arrow_forward_ios, size: 16),
          onTap: () {
            R.Router.navigator.pushNamed(R.Router.reservationScreen);
          },
        ),
        Divider(indent: 22, height: 0),
        ListTile(
          leading: Icon(Icons.account_circle),
          title: Text('الملف الشخصي'),
          trailing: Icon(Icons.arrow_forward_ios, size: 16),
          onTap: () {
            R.Router.navigator.pushNamed(R.Router.profileScreen);
          },
        ),
        Divider(indent: 22, height: 0),
        ListTile(
          leading: Icon(Icons.notifications),
          title: Text('الاشعارات'),
          trailing: Icon(Icons.arrow_forward_ios, size: 16),
          onTap: () {
            R.Router.navigator.pushNamed(R.Router.notificationScreen);
          },
        ),
        Divider(indent: 22, height: 0),
        ListTile(
          leading: Icon(Icons.info),
          title: Text('حول'),
          trailing: Icon(Icons.arrow_forward_ios, size: 16),
          onTap: () {
            R.Router.navigator.pushNamed(R.Router.aboutScreen);
          },
        ),
        Divider(indent: 22, height: 0),
        ListTile(
          leading: Icon(Icons.settings_power),
          title: Text('تسجيل خروج'),
          onTap: () {
            Provider.of<AccountModel>(context, listen: false).logout();
          },
        ),
        footer(),
      ],
    );
  }

  Widget notLoggedInWidget(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Padding(
                    padding: const EdgeInsets.only(top: 30, left: 16, right: 16, bottom: 16),
                    child: Column(
                      children: <Widget>[
                        Icon(
                          FarhatyApp.user_circle,
                          size: 60,
                          color: AppColors.primary,
                        ),
                        vS8,
                        FlatButton(
                          child: Text(
                            'تسجيل الدخول',
                            style: TextStyle(color: AppColors.primary, fontSize: 16),
                          ),
                          onPressed: () {
                            R.Router.navigator.pushNamed(R.Router.loginScreen);
                          },
                        )
                      ],
                    )),
              ),
            ],
          ),
        ),
        ListTile(
          leading: Icon(Icons.info),
          title: Text('حول'),
          trailing: Icon(
            Icons.arrow_forward_ios,
            size: 16,
          ),
          onTap: () {
            R.Router.navigator.pushNamed(R.Router.aboutScreen);
          },
        ),
        footer(),
      ],
    );
  }

  Widget footer() {
    return Column(
      children: <Widget>[
        Divider(),
        vS32,
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              IconButton(
                  icon: new Icon(
                    FontAwesome.facebook,
                    size: 25.0,
                    color: Colors.blueGrey,
                  ),
                  onPressed: () {
                    launch("https://www.facebook.com/bridal4ly/");
                  }),
              Padding(
                padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                child: IconButton(
                    icon: new Icon(
                      FontAwesome.whatsapp,
                      size: 25.0,
                      color: Colors.blueGrey,
                    ),
                    onPressed: () {
                      print("Pressed");
                    }),
              ),
              IconButton(
                  icon: new Icon(
                    FontAwesome.instagram,
                    size: 25.0,
                    color: Colors.blueGrey,
                  ),
                  onPressed: () {
                    launch("https://instagram.com/bridal_ezdehar/");
                  }),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'الاصدار 1.0.0',
                style: TextStyle(color: Colors.blueGrey),
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'جميع الحقوق محفوظة © BRIDAL 2020',
                style: TextStyle(color: Colors.blueGrey),
              )
            ],
          ),
        )
      ],
    );
  }
}
