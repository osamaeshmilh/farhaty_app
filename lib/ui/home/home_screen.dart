import 'package:farhaty_app/generated/i18n.dart';
import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/routes/router.gr.dart' as R;
import 'package:farhaty_app/ui/home/pages/account/account_page.dart';
import 'package:farhaty_app/ui/home/pages/favorite/favorite_page.dart';
import 'package:farhaty_app/ui/home/pages/halls/halls_page.dart';
import 'package:farhaty_app/ui/home/pages/landing/landing_page.dart';
import 'package:farhaty_app/utils/farhaty_app_icons.dart';
import 'package:flutter/material.dart';

import '../../firebase_notification_handler.dart';

class HomeScreen extends StatefulWidget {
  static const route = '/home';

  @override
  State<StatefulWidget> createState() => TabsPage();
}

class TabsPage extends State<HomeScreen> with SingleTickerProviderStateMixin {
  int _selectedTab = 0;
  final _pages = [
    LandingPage(),
    HallsPage(),
    FavoritesPage(),
    AccountPage(),
  ];

  @override
  void initState() {
    // PreferenceManager.isFirstLaunch().then((val) {
    //   if (val) {
    //     Navigator.of(context).pushNamed(R.Router.introScreen);
    //   }
    // });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    new FirebaseNotifications(context).setUpFirebase();

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        title: Padding(
          padding: const EdgeInsets.all(14.0),
          child: Image.asset(
            'assets/images/bridal-logo.png',
            width: 100,
          ),
        ),
        backgroundColor: Colors.white,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              FarhatyApp.search,
              color: AppColors.primary,
              size: 22,
            ),
            onPressed: () {
              R.Router.navigator.pushNamed(R.Router.storesScreen);
            },
          ),
          IconButton(
            icon: Icon(
              FarhatyApp.bell,
              color: AppColors.primary,
            ),
            onPressed: () {
              R.Router.navigator.pushNamed(R.Router.notificationScreen);
            },
          ),
        ],
      ),
      body: IndexedStack(
        index: _selectedTab,
        children: _pages,
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: AppColors.background,
        currentIndex: _selectedTab,
        type: BottomNavigationBarType.shifting,
        selectedItemColor: AppColors.primary,
        unselectedItemColor: Colors.grey.shade400,
        onTap: (int index) {
          setState(() {
            _selectedTab = index;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(FarhatyApp.home_lg_alt),
            title: Text(
              S.of(context).home,
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(FarhatyApp.landmark),
            title: Text(
              "صالات الافراح",
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(FarhatyApp.heart),
            title: Text(
              "المفضلة",
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(FarhatyApp.user_circle),
            title: Text(
              S.of(context).account,
            ),
          ),
        ],
      ),
    );
  }
}
