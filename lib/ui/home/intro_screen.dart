import 'package:farhaty_app/res/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';

class IntroScreen extends StatefulWidget {
  static final route = "/intro";

  IntroScreen({Key key}) : super(key: key);

  @override
  IntroScreenState createState() => new IntroScreenState();
}

class IntroScreenState extends State<IntroScreen> {
  List<Slide> slides = new List();

  @override
  void initState() {
    super.initState();

    slides.add(
      new Slide(
        title: "مرحبا بكم في تطبيق برايدل",
        styleTitle: TextStyle(color: AppColors.primary, fontWeight: FontWeight.bold, fontSize: 24),
        styleDescription: TextStyle(color: Colors.grey, fontSize: 24),
        description: "استمتع بالتسوق كما يجب ان يكون",
        pathImage: "assets/images/one.png",
        backgroundColor: Colors.white,
      ),
    );
    slides.add(
      new Slide(
        title: "ابحث في التصنيفات",
        styleTitle: TextStyle(color: AppColors.primary, fontWeight: FontWeight.bold, fontSize: 24),
        styleDescription: TextStyle(color: Colors.grey, fontSize: 24),
        description: "اكتشف معنا مجموعة واسعة ومختلفة من فئات المنتجات",
        pathImage: "assets/images/two.png",
        backgroundColor: Colors.white,
      ),
    );
    slides.add(
      new Slide(
        title: "برايدل",
        styleTitle: TextStyle(color: AppColors.primary, fontWeight: FontWeight.bold, fontSize: 24),
        styleDescription: TextStyle(color: Colors.grey, fontSize: 24),
        description: "برايدل",
        pathImage: "assets/images/three.png",
        backgroundColor: Colors.white,
      ),
    );
  }

  void onDonePress() {
    Navigator.of(context).pop();
    // Do what you want
  }

  @override
  Widget build(BuildContext context) {
    return IntroSlider(
      slides: this.slides,
      onDonePress: this.onDonePress,
      colorDoneBtn: AppColors.primary,
      colorActiveDot: AppColors.primary,
      colorDot: Colors.grey,
      colorPrevBtn: AppColors.primary,
      colorSkipBtn: AppColors.primary,
      nameDoneBtn: 'الانتهاء',
      nameSkipBtn: 'تخطي',
      nameNextBtn: 'التالي',
      namePrevBtn: 'رجوع',
    );
  }
}
