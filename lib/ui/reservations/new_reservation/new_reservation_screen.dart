import 'package:farhaty_app/models/extra.dart';
import 'package:farhaty_app/models/hall.dart';
import 'package:farhaty_app/models/hall_filter_args.dart';
import 'package:farhaty_app/models/room.dart';
import 'package:farhaty_app/models/type.dart';
import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/res/styles.dart';
import 'package:farhaty_app/utils/farhaty_app_icons.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:sweetalert/sweetalert.dart';

import 'new_reservation_model.dart';

class NewReservationScreen extends StatefulWidget {
  final HallFilterArgs typedArgs;
  NewReservationScreen(this.typedArgs);

  @override
  _NewReservationScreenState createState() => _NewReservationScreenState();
}

class _NewReservationScreenState extends State<NewReservationScreen> {
  final _formState = GlobalKey<FormState>();

  final _scaffold = GlobalKey<ScaffoldState>();

  final _formData = Map<String, dynamic>();

  int currentStep = 0;
  bool complete = false;

  var _index = 0;

  final attendeesTypes = <String, String>{
    "ALL": "الكل",
    "WOMEN": "نساء",
    "MEN": "رجال",
  };

  Hall hall;
  Room selectedRoom;
  Type selectedType;
  List<Extra> selectedExtras = [];
  String attendeesTypesValue;
  double total = 0.0;

  @override
  void initState() {
    hall = widget.typedArgs.hall;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final model = Provider.of<NewReservationModel>(context, listen: false);

    return Scaffold(
      appBar: AppBar(title: Text("حجز يوم " + widget.typedArgs.dateTime.day.toString() + "-" + widget.typedArgs.dateTime.month.toString())),
      body: Column(
        children: [
          Expanded(
            child: Stepper(
              currentStep: _index,
              type: StepperType.vertical,
              onStepContinue: _validateCurrentStep() ? _handleNextButton : null,
              steps: [
                Step(
                  title: Text("القاعة"),
                  state: _getStepState(0),
                  content: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'اختر القاعة',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Divider(),
                      ...hall.rooms.map((Room room) {
                        isAvailable(room.id).then((value) {
                          if (value == "0") {
                            room.canReserve = false;
                          } else if (value == "1") {
                            room.canReserve = true;
                          } else {
                            room.canReserve = false;
                          }
                        });
                        return Card(
                          child: ListTile(
                            leading: Icon(
                              FontAwesomeIcons.landmark,
                              color: room.canReserve != null && room.canReserve ? Colors.green : Colors.redAccent,
                            ),
                            title: Text(room.name),
                            subtitle: room.price != null ? Text("السعر الاضافي: " + room.price.toString() + " د.ل ") : SizedBox(),
                            trailing: selectedRoom?.id == room.id ? Icon(FontAwesomeIcons.check, color: AppColors.primary) : null,
                            onTap: () {
                              setState(() {
                                if (room.price != null) total += room.price;
                                if (room.canReserve) {
                                  selectedRoom = room;
                                } else {
                                  SweetAlert.show(context,
                                      title: "قاعة غير متاحة",
                                      subtitle: "هذه القاعة غير متاحة في هذا الموعد, اختر فاعة اخرى!",
                                      confirmButtonText: "حسنا",
                                      confirmButtonColor: AppColors.primary,
                                      style: SweetAlertStyle.confirm);
                                }
                              });
                            },
                          ),
                        );
                      }).toList()
                    ],
                  ),
                ),
                Step(
                    title: Text("المناسبة"),
                    state: _getStepState(1),
                    content: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              'اختر نوع المناسبة',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        Divider(),
                        ...hall.types.map((Type type) {
                          return Card(
                            child: ListTile(
                              leading: Icon(
                                FarhatyApp.rings_wedding,
                                color: AppColors.primary,
                              ),
                              title: Text(type.name),
                              subtitle: Text(type.getDayOfWeekPrice(widget.typedArgs.dateTime).toString() + " د.ل "),
                              trailing: selectedType?.id == type.id ? Icon(FontAwesomeIcons.check, color: AppColors.primary) : null,
                              onTap: () {
                                setState(() {
                                  selectedType = type;
                                });
                              },
                            ),
                          );
                        }).toList()
                      ],
                    )),
                Step(
                  title: Text("البيانات"),
                  state: _getStepState(2),
                  content: Column(
                    children: <Widget>[
                      TextFormField(
                        onChanged: (val) => _formData['customerName'] = val,
                        decoration: InputDecoration(labelText: 'الاسم التلاتي للزبون'),
                      ),
                      TextFormField(
                        validator: MultiValidator([
                          PatternValidator(
                            r'^(0?(9[0-9]{8}))$',
                            errorText: 'رقم هاتف غير صحيح',
                          )
                        ]),
                        keyboardType: TextInputType.number,
                        onChanged: (val) => _formData['customerPhone'] = val,
                        decoration: InputDecoration(labelText: 'رقم هاتف الزبون'),
                      ),
                      DropdownButton<String>(
                        hint: attendeesTypesValue == null
                            ? Text('اختر نوع الحضور')
                            : Text(
                                attendeesTypesValue,
                              ),
                        isExpanded: true,
                        items: attendeesTypes.keys
                            .map(
                              (key) => DropdownMenuItem<String>(value: key, child: Text(attendeesTypes[key])),
                            )
                            .toList(),
                        onChanged: (value) {
                          _formData['attendeesType'] = value;
                        },
                        value: attendeesTypes.keys.toList()[0],
                      ),
                      TextFormField(
                        keyboardType: TextInputType.number,
                        onChanged: (val) => _formData['attendeesNo'] = val,
                        decoration: InputDecoration(labelText: 'عدد الحضور المتوقع'),
                      ),
                      TextFormField(
                        maxLines: 20,
                        minLines: 3,
                        decoration: InputDecoration(labelText: 'ملاحظات'),
                        onChanged: (val) => _formData['notes'] = val,
                      ),
                    ],
                  ),
                ),
                Step(
                    title: Text("الاضافات"),
                    state: _getStepState(3),
                    content: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              'اختر الاضافات',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        Divider(),
                        ...hall.extras.map((Extra extra) {
                          return Card(
                            child: ListTile(
                              leading: Icon(
                                Icons.add,
                                color: AppColors.primary,
                              ),
                              title: Text(extra.name),
                              subtitle: Text(extra.price.toString() + " د.ل "),
                              trailing: extra.checked ? Icon(FontAwesomeIcons.check, color: AppColors.primary) : null,
                              onTap: () {
                                setState(() {
                                  extra.checked = !extra.checked;
                                  if (extra.checked) {
                                    selectedExtras.add(extra);
                                  } else {
                                    selectedExtras.remove(extra);
                                  }
                                });
                              },
                            ),
                          );
                        }).toList()
                      ],
                    )),
                Step(
                    title: Text("مراجعة"),
                    state: _getStepState(4),
                    content: Container(
                      width: double.infinity,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("الصالة: " + hall.name),
                          hS8,
                          selectedRoom != null ? Text("القاعة: " + selectedRoom.name) : SizedBox(),
                          hS8,
                          selectedType != null ? Text("المناسبة: " + selectedType.name) : SizedBox(),
                          hS8,
                          Text('تاريخ الحجز: ' + DateFormat('yyyy-MM-dd').format(widget.typedArgs.dateTime)),
                          hS8,
                          Text('صلاحية الحجز المبدئي: ' + hall.pendingAppointmentPeriod.toString() + " ايام "),
                        ],
                      ),
                    )),
              ],
            ),
          ),
          Container(
            // width: double.infinity,
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.9),
              borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20)),
              boxShadow: [
                BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.15), blurRadius: 5, offset: Offset(0, -2)),
              ],
            ),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: RaisedButton(
                    color: AppColors.primary,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
                    padding: EdgeInsets.symmetric(vertical: 8),
                    onPressed: _validateCurrentStep() ? _handleNextButton : null,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          _index == 4 ? "تأكيد" : "التالي",
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ),
                hS32,
                Text(
                  "الاجمالي: " + (getTotal()).toString() + " د.ل ",
                  style: TextStyle(fontWeight: FontWeight.bold),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  StepState _getStepState(int stepIndex) {
    if (_index == stepIndex) return StepState.editing;
    if (_index > stepIndex)
      return StepState.complete;
    else
      return StepState.disabled;
  }

  _validateCurrentStep() {
    if (_index == 0)
      return selectedRoom != null;
    else if (_index == 1)
      return selectedType != null;
    else if (_index == 2)
      return true;
    else if (_index == 3)
      return true;
    else
      return true;
  }

  _handleNextButton() {
    switch (_index) {
      case 0:
        setState(() {
          _index = 1;
        });
        break;
      case 1:
        setState(() {
          _index = 2;
        });
        break;
      case 2:
        setState(() {
          _index = 3;
        });
        break;
      case 3:
        setState(() {
          _index = 4;
        });
        break;
      case 4:
        setState(() {
          _submit();
        });
        break;
    }
  }

  double selectedExtrasTotal() {
    double etotal = 0;
    selectedExtras.forEach((Extra extra) {
      etotal += extra.price;
    });
    return etotal;
  }

  void _submit() {
    final formState = Form.of(context);
    if (formState.validate()) {
      SweetAlert.show(context, subtitle: "جاري ارسال الطلب...", style: SweetAlertStyle.loading);

      final formState = Form.of(context);
      _formData['roomId'] = selectedRoom.id;
      _formData['typeId'] = selectedType.id;
      _formData['hallId'] = widget.typedArgs.hall.id;
      _formData['reservationDate'] = DateFormat('yyyy-MM-dd').format(widget.typedArgs.dateTime);
      _formData['reservationStatus'] = 'PENDING';
      _formData['total'] = getTotal();
      _formData['period'] = 'MORNING';
      _formData['extras'] = selectedExtras;

      //_formData['extrasTotal'] = selectedExtrasTotal();

      // if (formState.validate()) {
      //   formState.save();
      //   final newReservationModel =
      //       Provider.of<NewReservationModel>(context, listen: false);
      //   newReservationModel
      //       .submit(data: newReservationModel.formData)
      //       .then((_) {});
      // }

      Provider.of<NewReservationModel>(context, listen: false).submit(data: _formData).then((res) async {
        if (res != null) {
          SweetAlert.show(context, title: "حجز جديد", subtitle: "تم تسجيل الحجز بنجاح!", style: SweetAlertStyle.success, confirmButtonColor: AppColors.primary, confirmButtonText: 'موافق',
              onPress: (bool isConfirm) {
            Navigator.of(context).pop();
            return true;
          });
        } else {
          SweetAlert.show(context, title: "حجز جديد", subtitle: "فشل ارسال الحجز!", style: SweetAlertStyle.error);
        }
      });
    }
  }

  Future<String> isAvailable(num roomId) {
    final data = Map<String, dynamic>();
    data['roomId.equals'] = roomId;
    data['reservationDate.equals'] = DateFormat('yyyy-MM-dd').format(widget.typedArgs.dateTime);
    return Provider.of<NewReservationModel>(context, listen: false).isAvailable(data: data);
  }

  double getTotal() {
    return total + (selectedType != null ? selectedType.getDayOfWeekPrice(widget.typedArgs.dateTime) : 0) + selectedExtrasTotal();
  }
}
