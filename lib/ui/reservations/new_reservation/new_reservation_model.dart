import 'package:dio/dio.dart';
import 'package:farhaty_app/core/failure/failure.dart';
import 'package:farhaty_app/core/provider/view_model.dart';
import 'package:farhaty_app/core/state/general_form_state.dart';
import 'package:farhaty_app/data/remote/services/reservation_service.dart';
import 'package:farhaty_app/models/error.dart';
import 'package:farhaty_app/models/reservation.dart';
import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';

@injectable
class NewReservationModel extends ChangeNotiferModel<GeneralFormState> {
  final ReservationService reservationService;
  final formState = ValueNotifier<GeneralFormState>(FormInitial());
  final formData = Map<String, dynamic>();

  NewReservationModel(this.reservationService);

  @override
  GeneralFormState get initialState => FormInitial();

  init({Map<String, dynamic> data}) async {}

  Future<Reservation> submit({Map<String, dynamic> data}) async {
    try {
      formState.value = FormLoading();
      print(data);
      var res = await reservationService.save(data);
      print(res);
      formState.value = FormSuccess();
      return res;
    } on DioError catch (err) {
      if (err.type == DioErrorType.RESPONSE) {
        print(err.response.data);
        final error = ResponseError();
        formState.value = FormError(ResponseFailure(error));
      } else {
        formState.value = FormError(NetworkFailure());
      }
      return null;
    }
  }

  Future<String> isAvailable({Map<String, dynamic> data}) async {
    try {
      state = FormLoading();
      var res = await reservationService.isRoomAvailable(data);
      print(res);
      state = FormSuccess();
      return res;
    } on DioError catch (err) {
      if (err.type == DioErrorType.RESPONSE) {
        print(err.response.data);
        final error = ResponseError();
        state = FormError(ResponseFailure(error));
      } else {
        state = FormError(NetworkFailure());
      }
      return null;
    }
  }
}
