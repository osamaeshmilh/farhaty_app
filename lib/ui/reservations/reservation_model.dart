import 'package:farhaty_app/core/provider/view_model.dart';
import 'package:farhaty_app/core/state/list_state.dart';
import 'package:farhaty_app/data/remote/services/reservation_service.dart';
import 'package:injectable/injectable.dart';
import 'package:dio/dio.dart';

@injectable
class ReservationModel extends ChangeNotiferModel<ListState> {
  final ReservationService reservationService;

  ReservationModel(this.reservationService);

  @override
  ListState get initialState => ListUninitialized();

  query({Map<String, dynamic> queryData}) async {
    try {
      //await PreferenceManager.getToken().then((v) => {print(v)});

      state = ListLoading();
      final res = await reservationService.fetchAll();

      state = ListLoaded(res);
    } on DioError {
      state = ListError();
    }
  }
}