import 'package:farhaty_app/core/provider/change_notifier_provider_widget.dart';
import 'package:farhaty_app/core/provider/state_consumer.dart';
import 'package:farhaty_app/core/state/list_state.dart';
import 'package:farhaty_app/models/reservation.dart' as notif;
import 'package:farhaty_app/models/reservation_filter_args.dart';
import 'package:farhaty_app/routes/router.gr.dart' as R;
import 'package:farhaty_app/ui/widgets/error_widget.dart';
import 'package:farhaty_app/ui/widgets/list_builder.dart';
import 'package:farhaty_app/ui/widgets/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'reservation_model.dart';

class ReservationScreen extends ChangeNotifierProviderWidget<ReservationModel> {
  ReservationScreen();

  @override
  onModelReady(ReservationModel model) {
    model.query();
  }

  @override
  Widget build(BuildContext context, ReservationModel reservationModel) {
    return Scaffold(
      appBar: AppBar(title: Text('حجوزاتي')),
      body: StateConsumer<ReservationModel>(builder: (model, state) {
        if (state is ListLoading) return LoadingWidget();
        if (state is ListLoaded) return buildList(state.items, context);
        return ErrorRetryWidget(onRetry: model.query());
      }),
    );
  }

  Widget buildList(List<notif.Reservation> reservations, BuildContext context) {
    return ListBuilder<notif.Reservation>(
      padding: EdgeInsets.only(top: 16),
      items: reservations,
      builder: (reservation) => Card(
        elevation: 0.5,
        clipBehavior: Clip.antiAlias,
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        child: ListTile(
          trailing: Text(reservation.total.toString() + ' د.ل ', style: TextStyle(fontWeight: FontWeight.bold)),
          leading: Text(
            reservation.id.toString() + "#",
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
          ),
          title: Text(
            reservation.getReservationStatusString(),
            style: TextStyle(color: reservation.getReservationStatusColor(), fontWeight: FontWeight.bold),
          ),
          subtitle: Text('تاريخ الحجز: ' + DateFormat('dd-MM-yyyy').format(reservation.reservationDate)),
          onTap: () {
            R.Router.navigator.pushNamed(
              R.Router.reservationDetailsScreen,
              arguments: ReservationFilterArgs(reservation: reservation),
            );
          },
        ),
      ),
    );
  }
}
