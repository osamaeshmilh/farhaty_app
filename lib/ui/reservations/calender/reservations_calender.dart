import 'package:farhaty_app/models/hall_filter_args.dart';
import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/routes/router.gr.dart' as R;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:intl/intl.dart';

class ReservationCalender extends StatefulWidget {
  final HallFilterArgs hallFilterArgs;

  ReservationCalender(this.hallFilterArgs);

  @override
  _ReservationCalenderState createState() => _ReservationCalenderState();
}

class _ReservationCalenderState extends State<ReservationCalender> {
  final queryData = Map<String, dynamic>();

  EventList<Event> _markedDateMap = new EventList<Event>();

  @override
  void initState() {
    final daysToGenerate = widget.hallFilterArgs.hall.availableTo.difference(DateTime.now().add(Duration(days: 1))).inDays;
    List.generate(daysToGenerate, (i) {
      _markedDateMap.add(
          DateTime(DateTime.now().add(Duration(days: 1)).year, DateTime.now().add(Duration(days: 1)).month, DateTime.now().add(Duration(days: 1)).day + (i)),
          new Event(
              title:
                  DateFormat('yyyy-MM-dd').format(DateTime(DateTime.now().add(Duration(days: 1)).year, DateTime.now().add(Duration(days: 1)).month, DateTime.now().add(Duration(days: 1)).day + (i))),
              date: DateTime(DateTime.now().add(Duration(days: 1)).year, DateTime.now().add(Duration(days: 1)).month, DateTime.now().add(Duration(days: 1)).day + (i)),
              icon: Container(
                decoration: BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.all(
                    Radius.circular(1000),
                  ),
                ),
                child: Center(
                  child: Text(
                    DateTime(DateTime.now().add(Duration(days: 1)).year, DateTime.now().add(Duration(days: 1)).month, DateTime.now().add(Duration(days: 1)).day + (i)).day.toString(),
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              )));
    });

    widget.hallFilterArgs.hall.reservedDates.forEach((element) {
      if (element[1] >= widget.hallFilterArgs.hall.rooms.length) {
        _markedDateMap.removeAll(DateTime.parse(element[0]));
        _markedDateMap.add(
            DateTime.parse(element[0]),
            Event(
                date: DateTime.parse(element[0]),
                icon: Container(
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.all(
                      Radius.circular(1000),
                    ),
                  ),
                  child: Center(
                    child: Text(
                      DateTime.parse(element[0]).day.toString(),
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                )));
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('المواعيد المتاحة'),
      ),
      body: ListView(
        children: [
          Center(
            child: CalendarCarousel<Event>(
              showOnlyCurrentMonthDate: true,
              firstDayOfWeek: 0,
              headerTextStyle: TextStyle(color: AppColors.primary, fontFamily: 'sky', fontSize: 20),
              weekdayTextStyle: TextStyle(color: AppColors.primary, fontFamily: 'sky', fontSize: 15),
              locale: 'ar',
              weekendTextStyle: TextStyle(
                color: Colors.red,
              ),
              markedDatesMap: _markedDateMap,
              thisMonthDayBorderColor: Colors.grey,
              markedDateShowIcon: true,
              markedDateIconBuilder: (event) {
                return event.icon;
              },
              weekFormat: false,
              daysHaveCircularBorder: null,
              height: 420,
              todayTextStyle: TextStyle(
                color: Colors.blue,
              ),
              minSelectedDate: DateTime.now(),
              todayButtonColor: null,
              markedDateIconMaxShown: 1,
              markedDateMoreShowTotal: null, // null for
              todayBorderColor: Colors.green,
              onDayPressed: (DateTime date, List<Event> events) {
                events.forEach((event) {
                  if (event.title != null)
                    R.Router.navigator.pushNamed(R.Router.newReservationScreen, arguments: HallFilterArgs(dateTime: DateTime.parse(event.title), hall: widget.hallFilterArgs.hall));
                });
              }, // not showing hidden events indicator
              /// null for not rendering any border, true for circular border, false for rectangular border
            ),
          ),
          ListTile(
            leading: new CircleAvatar(
              backgroundColor: Colors.green,
              radius: MediaQuery.of(context).size.height * 0.022,
            ),
            title: new Text(
              "موعد متاح",
            ),
          ),
          ListTile(
            leading: new CircleAvatar(
              backgroundColor: Colors.red,
              radius: MediaQuery.of(context).size.height * 0.022,
            ),
            title: new Text(
              "موعد محجوز",
            ),
          ),
          SizedBox(
            height: 20,
          )
        ],
      ),
    );
  }
}
