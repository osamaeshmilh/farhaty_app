import 'package:farhaty_app/models/extra.dart';
import 'package:farhaty_app/models/invoice.dart';
import 'package:farhaty_app/models/reservation.dart';
import 'package:farhaty_app/models/reservation_filter_args.dart';
import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/res/styles.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

class ReservationDetailsScreen extends StatefulWidget {
  final ReservationFilterArgs reservationFilterArgs;

  const ReservationDetailsScreen(this.reservationFilterArgs);

  @override
  _ReservationDetailsScreenState createState() => _ReservationDetailsScreenState();
}

class _ReservationDetailsScreenState extends State<ReservationDetailsScreen> with SingleTickerProviderStateMixin {
  Reservation reservation;

  @override
  void initState() {
    reservation = widget.reservationFilterArgs.reservation;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('تفاصيل الحجز'),
      ),
      body: ListView(
        padding: EdgeInsets.all(8),
        children: [
          vS8,
          Padding(
            padding: const EdgeInsets.only(top: 4.0, left: 8, right: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("حجز رقم: " + reservation.id.toString(), style: defaultStyle(16, true)),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                  decoration: BoxDecoration(color: reservation.getReservationStatusColor(), borderRadius: BorderRadius.circular(24)),
                  child: Row(
                    children: <Widget>[
                      Text(
                        reservation.getReservationStatusString(),
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 4.0, left: 8, right: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("تاريخ الحجز : " + DateFormat('dd-MM-yyyy').format(reservation.reservationDate), style: defaultStyle(16, true)),
                OutlineButton(
                  color: AppColors.primary,
                  onPressed: () {},
                  child: Row(
                    children: <Widget>[
                      Text(
                        'تحميل',
                        style: TextStyle(color: AppColors.primary),
                      ),
                      hS8,
                      Icon(
                        FontAwesomeIcons.download,
                        color: AppColors.primary,
                        size: 16,
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Divider(indent: 8),
          Card(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(reservation.hallName, style: defaultStyle(16, true)),
                  Divider(indent: 8),
                  vS8,
                  Text("القاعة: " + reservation.roomName),
                  vS8,
                  Text("نوع المناسبة: " + reservation.typeName),
                  vS8,
                  Text("نوع الحضور: " + reservation.getAttendeesTypeString()),
                  vS8,
                  Text("عدد الحضور: " + reservation.attendeesNo.toString()),
                  vS8,
                  Text("الاضافات: " + getExtras(reservation.extras)),
                  vS8,
                  reservation.notes != null ? Text("ملاحظات: " + reservation.notes) : SizedBox(),
                  vS8,
                  Divider(indent: 8),
                  Text("الاجمالي: " + reservation.total.toString() + " د.ل ", style: defaultStyle(16, true)),
                  vS8,
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('سندات القبض والدفع', style: defaultStyle(16, true)),
                Divider(indent: 8),
              ],
            ),
          ),
          Column(
            children: reservation.invoices.map<Widget>((Invoice invoice) => buildInvoiceWidget(invoice)).toList(),
          )
        ],
      ),
    );
  }

  Widget buildInvoiceWidget(Invoice invoice) {
    return Card(
      child: ListTile(
        title: Row(
          children: [
            Text(
              invoice.getInvoiceTypeString(),
              style: TextStyle(color: invoice.getInvoiceTypeColor(), fontWeight: FontWeight.bold),
            ),
          ],
        ),
        subtitle: Text(DateFormat('yyyy-MM-dd').format(invoice.invoiceDate)),
        leading: Text(
          invoice.id.toString() + "#",
          style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
        ),
        trailing: Text(invoice.amount.toString() + ' د.ل ', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
      ),
    );
  }

  String getExtras(List<Extra> extras) {
    String ext = '';
    extras.forEach((Extra extra) {
      ext = extra.name + "، " + ext;
    });
    return ext;
  }
}
