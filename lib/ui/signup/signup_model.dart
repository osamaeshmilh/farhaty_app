import 'package:dio/dio.dart';
import 'package:farhaty_app/core/failure/failure.dart';
import 'package:farhaty_app/core/provider/view_model.dart';
import 'package:farhaty_app/core/state/general_form_state.dart';
import 'package:farhaty_app/data/local/preferences_manager.dart';
import 'package:farhaty_app/data/remote/services/account_service.dart';
import 'package:farhaty_app/models/error.dart';
import 'package:farhaty_app/ui/home/pages/account/account_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';

@injectable
class SignupModel extends ViewModel {
  final PreferenceManager preferenceManager;
  final formState = ValueNotifier<GeneralFormState>(FormInitial());
  // final _scaffold = GlobalKey<ScaffoldState>();
  final formData = Map<String, dynamic>();
  final AccountService accountService;
  final AccountModel accountModel;

  SignupModel(this.accountService, this.preferenceManager, this.accountModel);

  @override
  GeneralFormState get initialState => FormInitial();


  Future submit({Map<String, dynamic> data}) async {
    try {
      formState.value = FormLoading();
      await accountService.register(data);
      formState.value = FormSuccess();
    } on DioError catch (err) {
      if (err.type == DioErrorType.RESPONSE) {
        print(err.response.data);
        final error = ResponseError();
        formState.value = FormError(ResponseFailure(error));
      } else {
        formState.value = FormError(NetworkFailure());
      }
    }
  }
}
