import 'package:farhaty_app/core/failure/failure.dart';
import 'package:farhaty_app/core/provider/listenable_builder.dart';
import 'package:farhaty_app/core/provider/provider_widget.dart';
import 'package:farhaty_app/core/state/general_form_state.dart';
import 'package:farhaty_app/generated/i18n.dart';
import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/res/styles.dart';
import 'package:farhaty_app/ui/home/pages/account/account_model.dart';
import 'package:farhaty_app/ui/login/login_model.dart';
import 'package:farhaty_app/ui/widgets/loader-container.dart';
import 'package:farhaty_app/utils/alert.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:provider/provider.dart';

import 'signup_model.dart';

class SignupScreen extends ProviderWidget<SignupModel> {
  final _formState = GlobalKey<FormState>();
  final _scaffold = GlobalKey<ScaffoldState>();
  final _formData = Map<String, dynamic>();

  @override
  Widget build(BuildContext rootContext, SignupModel model) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(rootContext).signup),
      ),
      key: _scaffold,
      body: Form(
        child: ValueBuilder<GeneralFormState>(
          listenable: model.formState,
          builder: (ctx, state) {
            return LoaderContainer(
              isLoading: state is FormLoading,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: ListView(
                  children: <Widget>[
                    if (state is FormLoading) CircularProgressIndicator(),
                    TextFormField(
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        labelText: 'الاسم',
                        labelStyle: TextStyle(color: AppColors.primary),
                        contentPadding: EdgeInsets.all(8),
                        hintText: 'محمد علي',
                        hintStyle: TextStyle(color: AppColors.light),
                        prefixIcon: Icon(Icons.account_circle, color: AppColors.primary),
                        border: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primary)),
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primary)),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primary)),
                      ),
                      onSaved: (val) => _formData['firstName'] = val,
                      onChanged: (c) {
                        // if (_profileSettingsFormKey.currentState.validate()) {
                        //   _profileSettingsFormKey.currentState.save();
                        // }
                      },
                    ),
                    vS16,
                    TextFormField(
                      keyboardType: TextInputType.number,
                      validator: MultiValidator([
                        PatternValidator(
                          r'^(0?(9[0-9]{8}))$',
                          errorText: 'رقم هاتف غير صحيح',
                        )
                      ]),
                      decoration: InputDecoration(
                        labelText: 'رقم الهاتف',
                        labelStyle: TextStyle(color: AppColors.primary),
                        contentPadding: EdgeInsets.all(8),
                        hintText: '0912345678',
                        hintStyle: TextStyle(color: AppColors.light),
                        prefixIcon: Icon(Icons.phone, color: AppColors.primary),
                        border: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primary)),
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primary)),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primary)),
                      ),
                      onSaved: (val) => _formData['phone'] = val,
                      onChanged: (c) {
                        // if (_profileSettingsFormKey.currentState.validate()) {
                        //   _profileSettingsFormKey.currentState.save();
                        // }
                      },
                    ),
                    vS16,
                    TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        labelText: 'البريد الالكتروني',
                        labelStyle: TextStyle(color: AppColors.primary),
                        contentPadding: EdgeInsets.all(8),
                        hintText: 'bridal@test.com',
                        hintStyle: TextStyle(color: AppColors.light),
                        prefixIcon: Icon(Icons.alternate_email, color: AppColors.primary),
                        border: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primary)),
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primary)),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primary)),
                      ),
                      onSaved: (val) {
                        _formData['email'] = val;
                        _formData['login'] = val;
                      },
                      onChanged: (c) {
                        // if (_profileSettingsFormKey.currentState.validate()) {
                        //   _profileSettingsFormKey.currentState.save();
                        // }
                      },
                    ),
                    vS16,
                    TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(
                        labelText: 'كلمة المرور',
                        labelStyle: TextStyle(color: AppColors.primary),
                        contentPadding: EdgeInsets.all(8),
                        hintText: '*****',
                        hintStyle: TextStyle(color: AppColors.light),
                        prefixIcon: Icon(Icons.lock, color: AppColors.primary),
                        border: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primary)),
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primary)),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primary)),
                      ),
                      onSaved: (val) => _formData['password'] = val,
                      onChanged: (c) {
                        // if (_profileSettingsFormKey.currentState.validate()) {
                        //   _profileSettingsFormKey.currentState.save();
                        // }
                      },
                    ),
                    vS16,
                    TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(
                        labelText: 'تأكيد كلمة المرور',
                        labelStyle: TextStyle(color: AppColors.primary),
                        contentPadding: EdgeInsets.all(8),
                        hintText: '*****',
                        hintStyle: TextStyle(color: AppColors.light),
                        prefixIcon: Icon(Icons.lock, color: AppColors.primary),
                        border: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primary)),
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primary)),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primary)),
                      ),
                      onChanged: (c) {
                        // if (_profileSettingsFormKey.currentState.validate()) {
                        //   _profileSettingsFormKey.currentState.save();
                        // }
                      },
                    ),
                    vS16,
                    FlatButton(
                      color: AppColors.primary,
                      textColor: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text('تسجيل'),
                      ),
                      onPressed: () => _submit(ctx),
                    ),
                  ],
                ),
              ),
            );
          },
          onBuilt: (ctx, state) {
            if (state is FormError) {
              final failure = state.failure;
              if (failure is NetworkFailure) showSnackOfScaffold(ctx, 'خطأ في الاتصال !');
              if (failure is ResponseFailure) showSnackOfScaffold(ctx, 'بيانات خاطئة !');
            }
            if (state is FormSuccess) {
              _formData['username'] = _formData['login'];
              Provider.of<LoginModel>(ctx, listen: false).submit(data: _formData).then((_) {
                Provider.of<AccountModel>(ctx, listen: false).authenticate();
                Navigator.of(ctx).pop();
              });
              Navigator.of(ctx).pop();
            }
          },
        ),
      ),
    );
  }

  void _submit(BuildContext context) {
    final formState = Form.of(context);
    if (formState.validate()) {
      formState.save();
      final signupModel = Provider.of<SignupModel>(context, listen: false);
      signupModel.submit(data: _formData).then((_) {});
    }
  }
}
