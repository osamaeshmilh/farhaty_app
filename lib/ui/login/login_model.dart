import 'package:dio/dio.dart';
import 'package:farhaty_app/core/failure/failure.dart';
import 'package:farhaty_app/core/provider/view_model.dart';
import 'package:farhaty_app/core/state/general_form_state.dart';
import 'package:farhaty_app/data/local/preferences_manager.dart';
import 'package:farhaty_app/data/remote/services/account_service.dart';
import 'package:farhaty_app/models/error.dart';
import 'package:farhaty_app/ui/home/pages/account/account_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as h;
import 'package:injectable/injectable.dart';

@injectable
class LoginModel extends ViewModel {
  final PreferenceManager preferenceManager;
  final formState = ValueNotifier<GeneralFormState>(FormInitial());

  // final _scaffold = GlobalKey<ScaffoldState>();
  final formData = Map<String, dynamic>();
  final AccountService accountService;
  final AccountModel accountModel;

  LoginModel(this.accountService, this.preferenceManager, this.accountModel);

  Future submit({Map<String, dynamic> data}) async {
    try {
      formState.value = FormLoading();
      final token = await accountService.login(data);
      preferenceManager.storeToken(token);
      accountModel.authenticate();
      formState.value = FormSuccess();
    } on DioError catch (err) {
      if (err.type == DioErrorType.RESPONSE) {
        print(err.response.data);
        final error = ResponseError();
        formState.value = FormError(ResponseFailure(error));
      } else {
        formState.value = FormError(NetworkFailure());
      }
    }
  }

  Future<bool> showFb() async {
    try {
      var response = await h.get("https://bridal.com.ly/show-fb.html");
      if (int.parse(response.body) == 0)
        return true;
      else
        return false;
    } catch (e) {
      print(e);
    }
  }

  Future changePassword({Map<String, dynamic> data}) async {
    try {
      formState.value = FormLoading();
      await accountService.changePassword(data);
      formState.value = FormSuccess();
    } on DioError catch (err) {
      if (err.type == DioErrorType.RESPONSE) {
        print(err.response.data);
        final error = ResponseError();
        formState.value = FormError(ResponseFailure(error));
      } else {
        formState.value = FormError(NetworkFailure());
      }
    }
  }

  Future loginWithFacebook() async {
    try {
      //facebookLogin.loginBehavior = FacebookLoginBehavior.webViewOnly;

      // final facebookLogin = FacebookLogin();
      // final fAuth = await facebookLogin.logIn(['email']);
      // if (fAuth.status == FacebookLoginStatus.loggedIn) {
      //   formState.value = FormLoading();
      //   var token = await accountService.loginWithFacebook(fAuth.accessToken.token);
      //   preferenceManager.storeToken(token);
      //   accountModel.authenticate();
      //   formState.value = FormSuccess();
      // }
    } on DioError catch (err) {
      print(err);
    }
  }

  Future loginWithGoogle() async {
    try {
      GoogleSignIn googleSignIn = GoogleSignIn(scopes: ['email', 'profile']);
      try {
        GoogleSignInAccount googleUser = await googleSignIn.signIn();
        GoogleSignInAuthentication gAuth = await googleUser.authentication;
        formState.value = FormLoading();
        var token = await accountService.loginWithGoogle(gAuth.idToken);
        preferenceManager.storeToken(token);
        accountModel.authenticate();
        formState.value = FormSuccess();
      } catch (err) {
        print(err);
      }
    } on DioError catch (err) {
      print(err);
    }
  }

  void resetPassword({data}) async {
    try {
      formState.value = FormLoading();
      await accountService.resetPassword(data);
      formState.value = FormSuccess();
    } on DioError catch (err) {
      if (err.type == DioErrorType.RESPONSE) {
        print(err.response.data);
        final error = ResponseError();
        formState.value = FormError(ResponseFailure(error));
      } else {
        formState.value = FormError(NetworkFailure());
      }
    }
  }
}
