//import 'package:flutter/material.dart';
//import 'package:farhaty_app/core/provider/state_consumer.dart';
//import 'package:farhaty_app/res/app_colors.dart';
//
//
//class LoginScreen extends StatelessWidget {
//  static final route = "/login";
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      body: StateConsumer<LoginModel>(
//        builder: (model, state) => Container(
//          height: MediaQuery.of(context).size.height,
//          decoration: BoxDecoration(
//            color: AppColors.background,
//          ),
//          child: Column(
//            crossAxisAlignment: CrossAxisAlignment.start,
//            mainAxisSize: MainAxisSize.max,
//            children: <Widget>[
//              ClipPath(
//                clipper: MyClipper(),
//                child: Container(
//                  decoration: BoxDecoration(color: Colors.green),
//                  alignment: Alignment.center,
//                  padding: EdgeInsets.only(top: 60.0, bottom: 80.0),
//                  child: Column(
//                    children: <Widget>[
//                      Text(
//                        "تجاري",
//                        style: TextStyle(fontSize: 50.0, fontWeight: FontWeight.bold, color: Colors.white),
//                      ),
//                      Text(
//                        "تسجيل الدخول",
//                        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.white),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              Padding(
//                padding: const EdgeInsets.only(right: 40.0),
//                child: Text(
//                  "اسم المستخدم",
//                  style: TextStyle(color: Colors.grey, fontSize: 16.0),
//                ),
//              ),
//              Container(
//                decoration: BoxDecoration(
//                  border: Border.all(
//                    color: Colors.grey.withOpacity(0.5),
//                    width: 1.0,
//                  ),
//                  borderRadius: BorderRadius.circular(20.0),
//                ),
//                margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
//                child: Row(
//                  children: <Widget>[
//                    Padding(
//                      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
//                      child: Icon(
//                        Icons.person_outline,
//                        color: Colors.grey,
//                      ),
//                    ),
//                    Container(
//                      height: 30.0,
//                      width: 1.0,
//                      color: Colors.grey.withOpacity(0.5),
//                      margin: const EdgeInsets.only(left: 10.0, right: 00.0),
//                    ),
//                    Expanded(
//                      child: TextField(
//                        decoration: InputDecoration(
//                          border: InputBorder.none,
//                          hintText: 'ادخل اسم المستخدم',
//                          hintStyle: TextStyle(color: Colors.grey),
//                        ),
//                      ),
//                    )
//                  ],
//                ),
//              ),
//              Padding(
//                padding: const EdgeInsets.only(right: 40.0),
//                child: Text(
//                  "كلمة المرور",
//                  style: TextStyle(color: Colors.grey, fontSize: 16.0),
//                ),
//              ),
//              Container(
//                decoration: BoxDecoration(
//                  border: Border.all(
//                    color: Colors.grey.withOpacity(0.5),
//                    width: 1.0,
//                  ),
//                  borderRadius: BorderRadius.circular(20.0),
//                ),
//                margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
//                child: Row(
//                  children: <Widget>[
//                    Padding(
//                      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
//                      child: Icon(
//                        Icons.lock_open,
//                        color: Colors.grey,
//                      ),
//                    ),
//                    Container(
//                      height: 30.0,
//                      width: 1.0,
//                      color: Colors.grey.withOpacity(0.5),
//                      margin: const EdgeInsets.only(left: 10.0, right: 00.0),
//                    ),
//                    Expanded(
//                      child: TextField(
//                        decoration: InputDecoration(
//                          border: InputBorder.none,
//                          hintText: 'ادخل كلمة المرور',
//                          hintStyle: TextStyle(color: Colors.grey),
//                        ),
//                      ),
//                    )
//                  ],
//                ),
//              ),
//              Container(
//                margin: const EdgeInsets.only(top: 20.0),
//                padding: const EdgeInsets.only(left: 20.0, right: 20.0),
//                child: Row(
//                  children: <Widget>[
//                    Expanded(
//                      child: FlatButton(
//                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
//                        splashColor: AppColors.primary,
//                        color: AppColors.primary,
//                        child: Row(
//                          children: <Widget>[
//                            Padding(
//                              padding: const EdgeInsets.only(right: 20.0),
//                              child: Text(
//                                "تسجيل الدخول",
//                                style: TextStyle(color: Colors.white),
//                              ),
//                            ),
//                            Expanded(
//                              child: Container(),
//                            ),
//                            Transform.translate(
//                              offset: Offset(15.0, 0.0),
//                              child: Container(
//                                padding: const EdgeInsets.all(5.0),
//                                child: FlatButton(
//                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(28.0)),
//                                  splashColor: Colors.white,
//                                  color: Colors.white,
//                                  child: Icon(
//                                    Icons.arrow_forward,
//                                    color: AppColors.primary,
//                                  ),
//                                  onPressed: () => {},
//                                ),
//                              ),
//                            )
//                          ],
//                        ),
//                        onPressed: () => {},
//                      ),
//                    ),
//                  ],
//                ),
//              ),
//              Container(
//                margin: const EdgeInsets.only(top: 10.0),
//                padding: const EdgeInsets.only(left: 20.0, right: 20.0),
//                child: Row(
//                  children: <Widget>[
//                    Expanded(
//                      child: FlatButton(
//                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
//                        splashColor: Color(0xFF3B5998),
//                        color: Color(0xff3B5998),
//                        child: Row(
//                          children: <Widget>[
//                            Padding(
//                              padding: const EdgeInsets.only(right: 20.0),
//                              child: Text(
//                                "تسجيل الدخول باستخدام فيس بوك",
//                                style: TextStyle(color: Colors.white),
//                              ),
//                            ),
//                            Expanded(
//                              child: Container(),
//                            ),
//                            Transform.translate(
//                              offset: Offset(15.0, 0.0),
//                              child: Container(
//                                padding: const EdgeInsets.all(5.0),
//                                child: FlatButton(
//                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(28.0)),
//                                  splashColor: Colors.white,
//                                  color: Colors.white,
//                                  child: Icon(
//                                    const IconData(0xea90, fontFamily: 'icomoon'),
//                                    color: Color(0xff3b5998),
//                                  ),
//                                  onPressed: () => {},
//                                ),
//                              ),
//                            )
//                          ],
//                        ),
//                        onPressed: () => {},
//                      ),
//                    ),
//                  ],
//                ),
//              ),
//              Container(
//                margin: const EdgeInsets.only(top: 20.0),
//                padding: const EdgeInsets.only(left: 20.0, right: 20.0),
//                child: Row(
//                  children: <Widget>[
//                    Expanded(
//                      child: FlatButton(
//                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
//                        color: Colors.transparent,
//                        child: Container(
//                          padding: const EdgeInsets.only(right: 20.0),
//                          alignment: Alignment.center,
//                          child: Text(
//                            "ليس لديك حساب ؟",
//                            style: TextStyle(color: AppColors.primary),
//                          ),
//                        ),
//                        onPressed: () => {},
//                      ),
//                    ),
//                  ],
//                ),
//              ),
//            ],
//          ),
//        ),
//      ),
//    );
//  }
//}
//
//class MyClipper extends CustomClipper<Path> {
//  @override
//  Path getClip(Size size) {
//    Path p = Path();
//    p.lineTo(size.width, 0.0);
//    p.lineTo(size.width, size.height * 0.85);
//    p.arcToPoint(
//      Offset(0.0, size.height * 0.85),
//      radius: const Radius.elliptical(50.0, 10.0),
//      rotation: 0.0,
//    );
//    p.lineTo(0.0, 0.0);
//    p.close();
//    return p;
//  }
//
//  @override
//  bool shouldReclip(CustomClipper oldClipper) {
//    return true;
//  }
//}
