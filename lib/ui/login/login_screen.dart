import 'dart:io';

import 'package:farhaty_app/core/failure/failure.dart';
import 'package:farhaty_app/core/provider/listenable_builder.dart';
import 'package:farhaty_app/core/provider/provider_widget.dart';
import 'package:farhaty_app/core/state/general_form_state.dart';
import 'package:farhaty_app/generated/i18n.dart';
import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/res/styles.dart';
import 'package:farhaty_app/routes/router.gr.dart' as R;
import 'package:farhaty_app/ui/widgets/loader-container.dart';
import 'package:farhaty_app/utils/alert.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'login_model.dart';

class LoginScreen extends ProviderWidget<LoginModel> {
  var showFb = false;

  @override
  Widget build(BuildContext rootContext, LoginModel model) {
    model.showFb().then((value) => showFb);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.primary,
        title: Text(
          S.of(rootContext).login,
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Form(
        child: ValueBuilder<GeneralFormState>(
          listenable: model.formState,
          builder: (ctx, state) {
            return LoaderContainer(
              isLoading: state is FormLoading,
              child: ListView(
                children: <Widget>[
                  vS16,
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Image.asset(
                      'assets/images/bridal.png',
                      width: 130,
                      height: 130,
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                          child: Icon(
                            Icons.person_outline,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          height: 30.0,
                          width: 1.0,
                          color: Colors.grey.withOpacity(0.5),
                          margin: const EdgeInsets.only(left: 10.0, right: 00.0),
                        ),
                        Expanded(
                          child: TextFormField(
                            onSaved: (val) => model.formData['username'] = val,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'ادخل اسم المستخدم',
                              hintStyle: TextStyle(color: Colors.grey),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                          child: Icon(
                            Icons.lock_open,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          height: 30.0,
                          width: 1.0,
                          color: Colors.grey.withOpacity(0.5),
                          margin: const EdgeInsets.only(left: 10.0, right: 00.0),
                        ),
                        Expanded(
                          child: TextFormField(
                            obscureText: true,
                            onSaved: (val) => model.formData['password'] = val,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'ادخل كلمة المرور',
                              hintStyle: TextStyle(color: Colors.grey),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20.0),
                    padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: FlatButton(
                            textColor: Colors.white,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(28.0)),
                            splashColor: AppColors.primary,
                            color: AppColors.primary,
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Text(
                                'تسجيل الدخول',
                              ),
                            ),
                            onPressed: () => _submit(ctx),
                          ),
                        ),
                      ],
                    ),
                  ),
                  !Platform.isIOS
                      ? Container(
                    margin: const EdgeInsets.only(top: 10.0),
                    padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: FlatButton(
                            padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 20.0),
                            textColor: Colors.white,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                            color: Colors.red,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Text('قوقل'),
                                SizedBox(
                                    width: 24.0,
                                    height: 24.0,
                                    child: Image.asset(
                                      'assets/images/google.svg',
                                      color: Colors.white,
                                    )),
                              ],
                            ),
                            onPressed: () {
                              Provider.of<LoginModel>(ctx, listen: false).loginWithGoogle();
                            },
                          ),
                        ),
                        SizedBox(
                          width: 16,
                        ),
                        Expanded(
                          child: FlatButton(
                            padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 20.0),
                            textColor: Colors.white,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                            color: Colors.blueAccent[400],
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Text(
                                  'فيس بوك',
                                ),
                                SizedBox(
                                    width: 24.0,
                                    height: 24.0,
                                    child: Image.asset(
                                      'assets/images/facebook.svg',
                                      color: Colors.white,
                                    )),
                              ],
                            ),
                            onPressed: () {
                              Provider.of<LoginModel>(ctx, listen: false).loginWithFacebook();
                            },
                          ),
                        ),
                      ],
                    ),
                  )
                      : SizedBox(),
                  Container(
                    margin: const EdgeInsets.only(top: 20.0),
                    padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: FlatButton(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                            color: Colors.transparent,
                            child: Container(
                              padding: const EdgeInsets.only(right: 20.0),
                              alignment: Alignment.center,
                              child: Text(
                                "ليس لديك حساب ؟",
                                style: TextStyle(color: AppColors.primary),
                              ),
                            ),
                            onPressed: () => {R.Router.navigator.pushReplacementNamed(R.Router.signupScreen)},
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
          onBuilt: (ctx, state) {
            if (state is FormError) {
              final failure = state.failure;
              if (failure is NetworkFailure) showSnackOfScaffold(ctx, 'خطأ في الاتصال !');
              if (failure is ResponseFailure) showSnackOfScaffold(ctx, 'بيانات خاطئة !');
            }
            if (state is FormSuccess) {
              Navigator.of(ctx).pop();
            }
          },
        ),
      ),
    );
  }

  void _submit(BuildContext context) {
    final formState = Form.of(context);
    if (formState.validate()) {
      formState.save();
      final loginModel = Provider.of<LoginModel>(context, listen: false);
      loginModel.submit(data: loginModel.formData).then((_) {});
    }
  }
}
