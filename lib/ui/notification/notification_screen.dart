import 'package:farhaty_app/core/provider/change_notifier_provider_widget.dart';
import 'package:farhaty_app/core/provider/state_consumer.dart';
import 'package:farhaty_app/core/state/list_state.dart';
import 'package:farhaty_app/models/notification.dart' as notif;
import 'package:farhaty_app/ui/widgets/error_widget.dart';
import 'package:farhaty_app/ui/widgets/list_builder.dart';
import 'package:farhaty_app/ui/widgets/loading_widget.dart';
import 'package:flutter/material.dart';

import 'notification_model.dart';

class NotificationScreen extends ChangeNotifierProviderWidget<NotificationModel> {
  NotificationScreen();

  @override
  onModelReady(NotificationModel model) {
    model.query();
  }

  @override
  Widget build(BuildContext context, NotificationModel notificationModel) {
    return Scaffold(
      appBar: AppBar(title: Text('الاشعارات')),
      body: StateConsumer<NotificationModel>(builder: (model, state) {
        if (state is ListLoading) return LoadingWidget();
        if (state is ListLoaded) return buildList(state.items, context);
        return ErrorRetryWidget(onRetry: model.query());
      }),
    );
  }

  Widget buildList(List<notif.Notification> notifications, BuildContext context) {
    return ListBuilder<notif.Notification>(
      padding: EdgeInsets.only(top: 16),
      items: notifications,
      builder: (notification) => Card(
        elevation: 0.5,
        clipBehavior: Clip.antiAlias,
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        child: ListTile(
          trailing: Icon(
            Icons.notifications,
            color: Colors.grey.shade400,
          ),
          title: Text(notification.title),
          subtitle: Text(notification.description),
          onTap: () {
//            R.Router.navigator.pushNamed(
//              R.Router.storeDetailsScreen,
//              arguments: store.id.toString(),
//            );
          },
        ),
      ),
    );
  }
}
