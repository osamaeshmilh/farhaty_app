import 'package:dio/dio.dart';
import 'package:farhaty_app/core/provider/view_model.dart';
import 'package:farhaty_app/core/state/list_state.dart';
import 'package:farhaty_app/data/remote/services/notification_service.dart';
import 'package:injectable/injectable.dart';

@injectable
class NotificationModel extends ChangeNotiferModel<ListState> {
  final NotificationService notificationService;

  NotificationModel(this.notificationService);

  @override
  ListState get initialState => ListUninitialized();

  query({Map<String, dynamic> queryData}) async {
    try {
      //await PreferenceManager.getToken().then((v) => {print(v)});
      final queryData = Map<String, dynamic>();

      state = ListLoading();
      final res = await notificationService.queryAll(queryData: queryData);

      state = ListLoaded(res);
    } on DioError {
      state = ListError();
    }
  }
}
