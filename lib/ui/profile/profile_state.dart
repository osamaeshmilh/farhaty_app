import 'package:farhaty_app/models/managed_user_vm.dart';

abstract class ProfileState{}

class ProfileInitial extends ProfileState {}

class ProfileLoaded extends ProfileState{
  final ManagedUserVM profile;

  ProfileLoaded(this.profile);
}

class ProfileLoading extends ProfileState{}