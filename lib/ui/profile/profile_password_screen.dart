import 'package:farhaty_app/core/provider/listenable_builder.dart';
import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/ui/login/login_model.dart';
import 'package:flutter/material.dart';
import 'package:farhaty_app/core/failure/failure.dart';
import 'package:farhaty_app/core/provider/state_consumer.dart';
import 'package:farhaty_app/core/state/general_form_state.dart';
import 'package:farhaty_app/res/styles.dart';
import 'package:farhaty_app/utils/alert.dart';
import 'package:provider/provider.dart';

class PasswordScreen extends StatefulWidget {
  @override
  _PasswordScreenState createState() => _PasswordScreenState();
}

class _PasswordScreenState extends State<PasswordScreen> {
  final _formState = GlobalKey<FormState>();
  final _scaffold = GlobalKey<ScaffoldState>();
  final _formData = Map<String, dynamic>();

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('تغيير كلمة المرور'),
//       ),
//       key: _scaffold,
//       body: ValueBuilder<GeneralFormState>(
//         listenable: mod,
//         builder: (model, state) => Form(
//           key: _formState,
//           child: Padding(
//             padding:
//                 EdgeInsets.only(top: 25.0, left: 16, right: 16, bottom: 16),
//             child: ListView(
//               children: <Widget>[
//                 if (state is FormLoading) CircularProgressIndicator(),
//                 TextFormField(
//                     decoration: defaultDecoration('كلمة المرور القديمة'),
//                     obscureText: true,
//                     onSaved: (val) => {_formData['currentPassword'] = val}
// //                  validator: RequiredValidator(errorText: 'this field is required'),
//                     ),
//                 vS16,
//                 TextFormField(
//                   decoration: defaultDecoration('كلمة المرور الجديدة'),
//                   obscureText: true,
//                   onSaved: (val) => _formData['newPassword'] = val,
// //                  validator: RequiredValidator(errorText: 'this field is required'),
//                 ),
//                 vS16,
//                 TextFormField(
//                   decoration: defaultDecoration('تأكيد كلمة المرور'),
//                   obscureText: true,
//                   onSaved: (val) => val,
// //                  validator: RequiredValidator(errorText: 'this field is required'),
//                 ),
//                 vS16,
//                 FlatButton(
//                   color: AppColors.primary,
//                   textColor: Colors.white,
//                   child: Padding(
//                     padding: const EdgeInsets.all(16.0),
//                     child: Text('حفظ'),
//                   ),
//                   onPressed: _submit,
//                 ),
//               ],
//             ),
//           ),
//         ),
//         onBuilt: (state) {
//           if (state is FormError) {
//             final failure = state.failure;
//             if (failure is NetworkFailure)
//               showSnack(_scaffold, 'Something went wrong');
//             if (failure is ResponseFailure)
//               showSnack(_scaffold, 'Response Error');
//           }
//         },
//       ),
//     );
//   }

  void _submit() {
    if (_formState.currentState.validate()) {
      _formState.currentState.save();
      Provider.of<LoginModel>(context).changePassword(data: _formData);
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container();
  }
}
