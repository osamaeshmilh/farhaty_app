import 'package:farhaty_app/models/managed_user_vm.dart';
import 'package:farhaty_app/res/app_colors.dart';
import 'package:farhaty_app/res/styles.dart';
import 'package:farhaty_app/routes/router.gr.dart' as R;
import 'package:farhaty_app/ui/widgets/round_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class ProfileWidget extends StatefulWidget {
  final ManagedUserVM profile;
  final Function(num id) onTap;

  const ProfileWidget(this.profile, {this.onTap});

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 60.0),
        child: RoundCard(
            color: Colors.grey.shade200,
            child: Padding(
              padding: const EdgeInsets.only(left: 60.0, right: 60.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    FontAwesome5.user_circle,
                    color: AppColors.primary,
                    size: 60,
                  ),
                  vS32,
                  Text(
                    'الاسم',
                    style: TextStyle(color: AppColors.primary),
                  ),
                  Text(widget.profile.firstName),
                  vS16,
                  Text(
                    'البريد الالكتروني',
                    style: TextStyle(color: AppColors.primary),
                  ),
                  Text(widget.profile.email),
                  vS16,
                  Text(
                    'رقم الهاتف',
                    style: TextStyle(color: AppColors.primary),
                  ),
                  Text(widget.profile.phone ?? ''),
                  vS16,
                  FlatButton(
                    child: Text('تغيير كلمة المرور'),
                    color: AppColors.primary,
                    textColor: Colors.white,
                    onPressed: () {
                      R.Router.navigator.pushNamed(R.Router.passwordScreen);
                    },
                  )
                ],
              ),
            )),
      ),
    );
  }
}
