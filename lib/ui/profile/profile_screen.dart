import 'package:farhaty_app/core/provider/change_notifier_provider_widget.dart';
import 'package:flutter/material.dart';
import 'package:farhaty_app/core/provider/state_consumer.dart';
import 'package:farhaty_app/generated/i18n.dart';
import 'package:farhaty_app/ui/profile/profile_state.dart';
import 'package:farhaty_app/ui/widgets/error_widget.dart';
import 'package:farhaty_app/ui/widgets/loading_widget.dart';

import 'profile_model.dart';
import 'profile_widget.dart';

class ProfileScreen extends ChangeNotifierProviderWidget<ProfileModel> {
  @override
  onModelReady(ProfileModel model) {
    model.getAccount();
  }

  @override
  Widget build(BuildContext context, ProfileModel model) {
    return Scaffold(
      appBar: AppBar(title: Text(S.of(context).account)),
      body: StateConsumer<ProfileModel>(builder: (model, state) {
        if (state is ProfileLoading) return LoadingWidget();
        if (state is ProfileLoaded) return ProfileWidget(state.profile);
        return ErrorRetryWidget(onRetry: model.getAccount);
      }),
    );
  }
}
