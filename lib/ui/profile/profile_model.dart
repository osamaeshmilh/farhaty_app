import 'package:dio/dio.dart';
import 'package:farhaty_app/core/provider/view_model.dart';
import 'package:farhaty_app/data/remote/services/account_service.dart';
import 'package:farhaty_app/ui/profile/profile_state.dart';
import 'package:injectable/injectable.dart';

@injectable
class ProfileModel extends ChangeNotiferModel<ProfileState> {
  final AccountService accountService;

  ProfileModel(this.accountService);

  @override
  ProfileState get initialState => ProfileInitial();

  List get items => null;

  getAccount() async {
    try {
      state = ProfileLoading();
      final profile = await accountService.getAccount();
      state = ProfileLoaded(profile);
    } on DioError {}
  }
}
