import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class FirebaseNotifications {
  FirebaseMessaging _firebaseMessaging;
  final BuildContext context;

  FirebaseNotifications(this.context);

  void setUpFirebase() {
    _firebaseMessaging = FirebaseMessaging();
    firebaseCloudMessaging_Listeners();

  }

  void firebaseCloudMessaging_Listeners() {
    if (Platform.isIOS) iOS_Permission();

    _firebaseMessaging.getToken().then((token) {
      print(token);
    });

    _firebaseMessaging.subscribeToTopic('all');

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: ListTile(
              title: Text(message['notification']['title']),
              subtitle: Text(message['notification']['body']),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        );

        //FlutterRingtonePlayer.playNotification();

      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
    );
  }

  void iOS_Permission() {

    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  Future sendNotification(title, description) async {
    var DATA = '{"notification": {"body": "$description","title": "$title"}, "priority": "high", "data": {"click_action": "FLUTTER_NOTIFICATION_CLICK", "id": "1", "status": "done"}, "to": "/topics/all"}';

    var response = await http.post("https://fcm.googleapis.com/fcm/send", body: DATA, headers: {
      "Authorization": "key=AAAAwpzQ5jo:APA91bFkt-TcDbE_ObKLTo7ctY011cBEDdsGeXE_CstJ884hjR3BpJ--HhKVsoQrSjBqbXQ3pobDU5za0-4JhdL7vWkIcl6Ymxcj4R2qTkn30-859PIVvJ_986Ih2-lahp5ukLWh1g2U",
      "Content-Type": "application/json"
    });

  }


}