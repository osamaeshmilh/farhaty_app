import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

showSnack(GlobalKey<ScaffoldState> scaffold, String message,
    {Color color = Colors.black}) {
  scaffold.currentState.showSnackBar(
    SnackBar(
        backgroundColor: color,
        content: Text(message, textAlign: TextAlign.center),
        duration: Duration(milliseconds: 1500)),
  );
}

showSnackOfScaffold(BuildContext ctx, String message,
    {Color color = Colors.black}) {
  Scaffold.of(ctx).showSnackBar(
    SnackBar(
        backgroundColor: color,
        content: Text(message, textAlign: TextAlign.center),
        duration: Duration(milliseconds: 1500)),
  );
}

getFormattedPrice(num price) {
  return NumberFormat.decimalPattern("en").format(price);
}

//const toastPosition = ToastPosition(align: Alignment.bottomCenter, offset: -60);
//
//showSuccessToast(String message) {
//  showToast(message,
//      backgroundColor: AppColors.accent,
//      textStyle: TextStyle(fontFamily: primaryFont),
//      textDirection: TextDirection.rtl,
//      position: toastPosition,
//      textPadding: EdgeInsets.symmetric(horizontal: 22, vertical: 8),
//      radius: 30);
//}
//
//showErrorToast(String message) {
//  showToast(message,
//      backgroundColor: Colors.redAccent.shade100,
//      textStyle: TextStyle(fontFamily: primaryFont),
//      textDirection: TextDirection.rtl,
//      position: toastPosition,
//      textPadding: EdgeInsets.symmetric(horizontal: 22, vertical: 8),
//      radius: 30);
//}

//showAlert(BuildContext context,
//    {String title,
//    @required String message,
//    Function positiveAction,
//    String positiveLabel,
//    Function negativeAction,
//    String negativeLabel}) {
//  return showDialog(
//    context: context,
//    builder: (_) => AlertDialog(
//      title: title != null ? LocaleText(title) : null,
//      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
//      content: Container(
//        child: LocaleText(message),
//      ),
//      actions: <Widget>[
//        if (negativeAction != null)
//          FlatButton(
//              textColor: AppColors.primary,
//              onPressed: () {
//                Navigator.of(context).pop();
//                negativeAction();
//              },
//              child: LocaleText(negativeLabel ?? 'cancel')),
//        Padding(
//          padding: const EdgeInsets.symmetric(horizontal: 8),
//          child: FlatButton(
//              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
//              textColor: AppColors.primary,
//              onPressed: () {
//                Navigator.of(context).pop();
//                if (positiveAction != null) positiveAction();
//              },
//              child: LocaleText(positiveLabel ?? 'ok')),
//        ),
//      ],
//    ),
//  );
//}
