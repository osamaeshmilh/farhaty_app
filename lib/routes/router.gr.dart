// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart';
import 'package:farhaty_app/models/hall_filter_args.dart';
import 'package:farhaty_app/models/reservation_filter_args.dart';
import 'package:farhaty_app/models/store_filter_args.dart';
import 'package:farhaty_app/ui/about/about_screen.dart';
import 'package:farhaty_app/ui/halls/hall_details/hall_details_screen.dart';
import 'package:farhaty_app/ui/halls/halls_screen.dart';
import 'package:farhaty_app/ui/home/home_screen.dart';
import 'package:farhaty_app/ui/home/intro_screen.dart';
import 'package:farhaty_app/ui/login/login_screen.dart';
import 'package:farhaty_app/ui/notification/notification_screen.dart';
import 'package:farhaty_app/ui/profile/profile_password_screen.dart';
import 'package:farhaty_app/ui/profile/profile_screen.dart';
import 'package:farhaty_app/ui/reservations/calender/reservations_calender.dart';
import 'package:farhaty_app/ui/reservations/new_reservation/new_reservation_screen.dart';
import 'package:farhaty_app/ui/reservations/reservation_details/reservation_details_screen.dart';
import 'package:farhaty_app/ui/reservations/reservation_screen.dart';
import 'package:farhaty_app/ui/signup/signup_screen.dart';
import 'package:farhaty_app/ui/stores/store_details/store_details_screen.dart';
import 'package:farhaty_app/ui/stores/stores_screen.dart';
import 'package:flutter/material.dart';

class Router {
  static const homeScreen = '/';
  static const loginScreen = '/login-screen';
  static const passwordScreen = '/password-screen';
  static const aboutScreen = '/about-screen';
  static const profileScreen = '/profile-screen';
  static const signupScreen = '/signup-screen';
  static const storesScreen = '/stores-screen';
  static const storeDetailsScreen = '/store-details-screen';
  static const notificationScreen = '/notification-screen';
  static const hallDetailsScreen = '/hall-details-screen';
  static const reservationScreen = '/reservation-screen';
  static const hallsScreen = '/halls-screen';
  static const reservationCalender = '/reservation-calender';
  static const newReservationScreen = '/new-reservation-screen';
  static const reservationDetailsScreen = '/reservation-details-screen';
  static const introScreen = '/intro-screen';

  static final navigator = ExtendedNavigator();

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Router.homeScreen:
        return MaterialPageRoute<dynamic>(
          builder: (_) => HomeScreen(),
          settings: settings,
        );
      case Router.loginScreen:
        return MaterialPageRoute<dynamic>(
          builder: (_) => LoginScreen(),
          settings: settings,
        );
      case Router.passwordScreen:
        return MaterialPageRoute<dynamic>(
          builder: (_) => PasswordScreen(),
          settings: settings,
        );
      case Router.aboutScreen:
        return MaterialPageRoute<dynamic>(
          builder: (_) => AboutScreen(),
          settings: settings,
        );
      case Router.profileScreen:
        return MaterialPageRoute<dynamic>(
          builder: (_) => ProfileScreen(),
          settings: settings,
        );
      case Router.signupScreen:
        return MaterialPageRoute<dynamic>(
          builder: (_) => SignupScreen(),
          settings: settings,
        );
      case Router.storesScreen:
        if (hasInvalidArgs<StoreFilterArgs>(args)) {
          return misTypedArgsRoute<StoreFilterArgs>(args);
        }
        final typedArgs = args as StoreFilterArgs;
        return MaterialPageRoute<dynamic>(
          builder: (_) => StoresScreen(typedArgs),
          settings: settings,
        );
      case Router.storeDetailsScreen:
        if (hasInvalidArgs<String>(args)) {
          return misTypedArgsRoute<String>(args);
        }
        final typedArgs = args as String;
        return MaterialPageRoute<dynamic>(
          builder: (_) => StoreDetailsScreen(typedArgs),
          settings: settings,
        );
      case Router.notificationScreen:
        return MaterialPageRoute<dynamic>(
          builder: (_) => NotificationScreen(),
          settings: settings,
        );

      case Router.hallDetailsScreen:
        if (hasInvalidArgs<HallFilterArgs>(args)) {
          return misTypedArgsRoute<HallFilterArgs>(args);
        }
        final typedArgs = args as HallFilterArgs;
        return MaterialPageRoute<dynamic>(
          builder: (_) => HallDetailsScreen(typedArgs),
          settings: settings,
        );

      case Router.reservationScreen:
        return MaterialPageRoute<dynamic>(
          builder: (_) => ReservationScreen(),
          settings: settings,
        );
      case Router.hallsScreen:
        if (hasInvalidArgs<HallFilterArgs>(args)) {
          return misTypedArgsRoute<HallFilterArgs>(args);
        }
        final typedArgs = args as HallFilterArgs;
        return MaterialPageRoute<dynamic>(
          builder: (_) => HallsScreen(typedArgs),
          settings: settings,
        );

      case Router.reservationCalender:
        if (hasInvalidArgs<HallFilterArgs>(args)) {
          return misTypedArgsRoute<HallFilterArgs>(args);
        }
        final typedArgs = args as HallFilterArgs;
        return MaterialPageRoute<dynamic>(
          builder: (_) => ReservationCalender(typedArgs),
          settings: settings,
        );

      case Router.newReservationScreen:
        if (hasInvalidArgs<HallFilterArgs>(args)) {
          return misTypedArgsRoute<HallFilterArgs>(args);
        }
        final typedArgs = args as HallFilterArgs;
        return MaterialPageRoute<dynamic>(
          builder: (_) => NewReservationScreen(typedArgs),
          settings: settings,
        );
      case Router.introScreen:
        return MaterialPageRoute<dynamic>(
          builder: (_) => IntroScreen(),
          settings: settings,
        );
      case Router.reservationDetailsScreen:
        if (hasInvalidArgs<ReservationFilterArgs>(args)) {
          return misTypedArgsRoute<ReservationFilterArgs>(args);
        }
        final typedArgs = args as ReservationFilterArgs;
        return MaterialPageRoute<dynamic>(
          builder: (_) => ReservationDetailsScreen(typedArgs),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}
