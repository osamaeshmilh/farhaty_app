import 'package:auto_route/auto_route_annotations.dart';
import 'package:farhaty_app/ui/halls/hall_details/hall_details_screen.dart';
import 'package:farhaty_app/ui/halls/halls_screen.dart';
import 'package:farhaty_app/ui/home/intro_screen.dart';
import 'package:farhaty_app/ui/notification/notification_screen.dart';
import 'package:farhaty_app/ui/reservations/calender/reservations_calender.dart';
import 'package:farhaty_app/ui/reservations/new_reservation/new_reservation_screen.dart';
import 'package:farhaty_app/ui/reservations/reservation_details/reservation_details_screen.dart';
import 'package:farhaty_app/ui/reservations/reservation_screen.dart';
import 'package:farhaty_app/ui/stores/store_details/store_details_screen.dart';
import 'package:farhaty_app/ui/stores/stores_screen.dart';

import '../ui/about/about_screen.dart';
import '../ui/home/home_screen.dart';
import '../ui/login/login_screen.dart';
import '../ui/profile/profile_password_screen.dart';
import '../ui/profile/profile_screen.dart';
import '../ui/signup/signup_screen.dart';

@MaterialAutoRouter()
class $Router {
  @initial
  HomeScreen homeScreen;
  LoginScreen loginScreen;
  PasswordScreen passwordScreen;
  AboutScreen aboutScreen;
  ProfileScreen profileScreen;
  SignupScreen signupScreen;
  StoresScreen storesScreen;
  StoreDetailsScreen storeDetailsScreen;
  NotificationScreen notificationScreen;
  HallDetailsScreen hallDetailsScreen;
  ReservationScreen reservationScreen;
  HallsScreen hallsScreen;
  ReservationCalender reservationCalender;
  NewReservationScreen newReservationScreen;
  ReservationDetailsScreen reservationDetailsScreen;
  IntroScreen introScreen;
}
