import 'package:dio/native_imp.dart';
import 'package:farhaty_app/data/remote/constants.dart';
import 'package:farhaty_app/data/remote/interceptors/auth_interceptor.dart';
import 'package:injectable/injectable.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

@lazySingleton
class ApiClient extends DioForNative {
  ApiClient(AuthInterceptor authInterceptor) {
    options.baseUrl = baseUrl;
    interceptors.add(authInterceptor);
     interceptors.add(PrettyDioLogger(compact: false));
  }
}
