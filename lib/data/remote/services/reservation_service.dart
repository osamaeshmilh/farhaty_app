import 'package:farhaty_app/models/reservation.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

import '../api_client.dart';

const String resource = "/reservations";

@injectable
class ReservationService {
  final ApiClient client;
  ReservationService(this.client);

  Future<List<Reservation>> fetchAll() {
    return client.get(resource).then((res) =>
        res.data.map<Reservation>((it) => Reservation.fromJson(it)).toList());
  }

  Future<Reservation> getOne(id) {
    print(id);
    return client.get(resource + '/' + id).then((res) {
      return Reservation.fromJson(res.data);
    });
  }

  query({Map<String, dynamic> queryData}) {}

  Future<List<Reservation>> queryAll({Map<String, dynamic> queryData}) {
    //queryData..['status.equals'] = true;
    queryData..['size'] = 100;
    queryData?.removeWhere((_, v) => isNull(v));
    return client.get(resource, queryParameters: queryData).then((res) =>
        res.data.map<Reservation>((it) => Reservation.fromJson(it)).toList());
  }

  bool isNull(dynamic value) {
    if (value == null) return true;
    if (value is String && value.isEmpty) return true;
    if (value is List && value.isEmpty) return true;
    return false;
  }

  Future<Reservation> save(Map<String, dynamic> data) {
    debugPrint(data.toString());
    return client.post(resource, data: data).then((res) {
      return Reservation.fromJson(res.data);
    });
  }

  Future<String> isRoomAvailable(Map<String, dynamic> data) {
    return client
        .get("/public${resource}/is-room-available", queryParameters: data)
        .then((res) => res.data);
  }
}
