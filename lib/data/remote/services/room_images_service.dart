import 'package:farhaty_app/models/room.dart';
import 'package:farhaty_app/models/room_image.dart';
import 'package:injectable/injectable.dart';

import '../api_client.dart';

const String resource = "/public/room-images";

@injectable
class RoomImagesService {
  final ApiClient client;
  RoomImagesService(this.client);

  Future<List<RoomImage>> fetchAll() {
    return client.get(resource).then((res) =>
        res.data.map<RoomImage>((it) => RoomImage.fromJson(it)).toList());
  }

  Future<RoomImage> getOne(id) {
    return client
        .get(resource + '/' + id)
        .then((res) => res.data.map<RoomImage>((it) => Room.fromJson(it)));
  }

  query({Map<String, dynamic> queryData}) {

  }

  Future<List<RoomImage>> queryAll({Map<String, dynamic> queryData}) {
    //queryData..['status.equals'] = true;
    queryData..['size'] = 100;
    queryData?.removeWhere((_, v) => isNull(v));
    return client
        .get(resource, queryParameters: queryData)
        .then((res) => res.data.map<RoomImage>((it) => RoomImage.fromJson(it)).toList());
  }

  bool isNull(dynamic value) {
    if (value == null) return true;
    if (value is String && value.isEmpty) return true;
    if (value is List && value.isEmpty) return true;
    return false;
  }
}
