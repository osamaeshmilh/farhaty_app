import 'package:farhaty_app/models/hall.dart';
import 'package:injectable/injectable.dart';

import '../api_client.dart';

const String resource = "/public/halls";

@injectable
class HallService {
  final ApiClient client;
  HallService(this.client);

  Future<List<Hall>> fetchAll() {
    return client.get(resource).then((res) =>
        res.data.map<Hall>((it) => Hall.fromJson(it)).toList());
  }

  Future<Hall> getOne(id) {
    print(id);
    return client.get(resource + '/' + id).then((res) {
      return Hall.fromJson(res.data);
    });
  }



  query({Map<String, dynamic> queryData}) {

  }

  Future<List<Hall>> queryAll({Map<String, dynamic> queryData}) {
    //queryData..['vip.equals'] = true;
    queryData..['size'] = 100;
    queryData?.removeWhere((_, v) => isNull(v));
    return client.get(resource, queryParameters: queryData).then((res) => res.data.map<Hall>((it) => Hall.fromJson(it)).toList());
  }

  bool isNull(dynamic value) {
    if (value == null) return true;
    if (value is String && value.isEmpty) return true;
    if (value is List && value.isEmpty) return true;
    return false;
  }
}
