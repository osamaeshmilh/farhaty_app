import 'package:farhaty_app/models/notification.dart';
import 'package:injectable/injectable.dart';

import '../api_client.dart';

const String resource = "/public/notifications";

@injectable
class NotificationService {
  final ApiClient client;
  NotificationService(this.client);

  Future<List<Notification>> fetchAll() {
    return client.get(resource).then((res) => res.data.map<Notification>((it) => Notification.fromJson(it)).toList());
  }

  Future<Notification> getOne(id) {
    print(id);
    return client.get(resource + '/' + id).then((res) {
      return Notification.fromJson(res.data);
    });
  }

  query({Map<String, dynamic> queryData}) {}

  Future<List<Notification>> queryAll({Map<String, dynamic> queryData}) {
    //queryData..['status.equals'] = true;
    queryData..['size'] = 200;
    queryData..['sort'] = 'id,desc';
    queryData?.removeWhere((_, v) => isNull(v));
    return client.get(resource, queryParameters: queryData).then((res) => res.data.map<Notification>((it) => Notification.fromJson(it)).toList());
  }

  bool isNull(dynamic value) {
    if (value == null) return true;
    if (value is String && value.isEmpty) return true;
    if (value is List && value.isEmpty) return true;
    return false;
  }
}
