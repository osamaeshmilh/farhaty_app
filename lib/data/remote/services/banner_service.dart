import 'package:farhaty_app/models/banner.dart';
import 'package:injectable/injectable.dart';

import '../api_client.dart';

const String resource = "/public/banners";

@injectable
class BannerService {
  final ApiClient client;
  BannerService(this.client);

  Future<List<Banner>> fetchAll() {
    return client.get(resource).then(
        (res) => res.data.map<Banner>((it) => Banner.fromJson(it)).toList());
  }

  Future<Banner> getOne(id) {
    return client
        .get(resource + '/' + id)
        .then((res) => res.data.map<Banner>((it) => Banner.fromJson(it)));
  }
}
