import 'package:farhaty_app/data/local/preferences_manager.dart';
import 'package:farhaty_app/models/jwt_token.dart';
import 'package:farhaty_app/models/managed_user_vm.dart';
import 'package:injectable/injectable.dart';

import '../api_client.dart';

const String authResource = "/authenticate";
const String registerResource = "/register";
const String userResource = "/account";
const String passwordResource = "/account/change-password";
const String passwordResetResource = "/account/reset-password/init";

@injectable
class AccountService {
  final ApiClient client;
  final PreferenceManager preferenceManager;

  AccountService(this.client, this.preferenceManager);

  Future<JWTToken> login(Map<String, dynamic> data) {
    return client.post(authResource, data: data).then((res) => JWTToken.fromJson(res.data));
  }

  Future<String> register(Map<String, dynamic> data) {
    return client.post(registerResource, data: data).then((res) => res.data);
  }

  Future<ManagedUserVM> getAccount() {
    return client.get(userResource).then((res) {
      return ManagedUserVM.fromJson(res.data);
    });
  }

  bool isAuthenticated() {
    return preferenceManager.getToken() != null;
  }

  Future<void> changePassword(Map<String, dynamic> data) {
    return client.post(passwordResource, data: data).then((res) => res.data);
  }

  Future<JWTToken> loginWithGoogle(String token) {
    return client.post("/authenticate/appGoogle", data: token).then((res) => JWTToken.fromJson(res.data));
  }

  Future<JWTToken> loginWithFacebook(String token) {
    return client.post("/authenticate/appFacebook", data: token).then((res) => JWTToken.fromJson(res.data));
  }

  resetPassword(data) {
    return client.post(passwordResetResource, data: data).then((res) => res.data);
  }
}
