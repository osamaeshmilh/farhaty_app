import 'package:farhaty_app/models/category.dart';
import 'package:farhaty_app/models/store.dart';
import 'package:injectable/injectable.dart';

import '../api_client.dart';

const String resource = "/public/stores";

@injectable
class StoreService {
  final ApiClient client;
  StoreService(this.client);

  Future<List<Store>> fetchAll() {
    return client.get(resource).then((res) =>
        res.data.map<Store>((it) => Store.fromJson(it)).toList());
  }

  Future<Store> getOne(id) {
    print(id);
    return client.get(resource + '/' + id).then((res) {
      return Store.fromJson(res.data);
    });
  }

  query({Map<String, dynamic> queryData}) {

  }

  Future<List<Store>> queryAll({Map<String, dynamic> queryData}) {
    //queryData..['status.equals'] = true;
    queryData..['size'] = 100;
    queryData?.removeWhere((_, v) => isNull(v));
    return client
        .get(resource, queryParameters: queryData)
        .then((res) => res.data.map<Store>((it) => Store.fromJson(it)).toList());
  }

  bool isNull(dynamic value) {
    if (value == null) return true;
    if (value is String && value.isEmpty) return true;
    if (value is List && value.isEmpty) return true;
    return false;
  }
}
