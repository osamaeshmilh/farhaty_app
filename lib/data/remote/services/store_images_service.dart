import 'package:farhaty_app/models/category.dart';
import 'package:farhaty_app/models/store.dart';
import 'package:farhaty_app/models/store_image.dart';
import 'package:injectable/injectable.dart';

import '../api_client.dart';

const String resource = "/public/store-images";

@injectable
class StoreImagesService {
  final ApiClient client;
  StoreImagesService(this.client);

  Future<List<StoreImage>> fetchAll() {
    return client.get(resource).then((res) =>
        res.data.map<StoreImage>((it) => StoreImage.fromJson(it)).toList());
  }

  Future<StoreImage> getOne(id) {
    return client
        .get(resource + '/' + id)
        .then((res) => res.data.map<StoreImage>((it) => Store.fromJson(it)));
  }

  query({Map<String, dynamic> queryData}) {

  }

  Future<List<StoreImage>> queryAll({Map<String, dynamic> queryData}) {
    //queryData..['status.equals'] = true;
    queryData..['size'] = 100;
    queryData?.removeWhere((_, v) => isNull(v));
    return client
        .get(resource, queryParameters: queryData)
        .then((res) => res.data.map<StoreImage>((it) => StoreImage.fromJson(it)).toList());
  }

  bool isNull(dynamic value) {
    if (value == null) return true;
    if (value is String && value.isEmpty) return true;
    if (value is List && value.isEmpty) return true;
    return false;
  }
}
