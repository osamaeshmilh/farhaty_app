import 'package:farhaty_app/models/hall.dart';
import 'package:farhaty_app/models/hall_image.dart';
import 'package:injectable/injectable.dart';

import '../api_client.dart';

const String resource = "/public/hall-images";

@injectable
class HallImagesService {
  final ApiClient client;
  HallImagesService(this.client);

  Future<List<HallImage>> fetchAll() {
    return client.get(resource).then((res) =>
        res.data.map<HallImage>((it) => HallImage.fromJson(it)).toList());
  }

  Future<HallImage> getOne(id) {
    return client
        .get(resource + '/' + id)
        .then((res) => res.data.map<HallImage>((it) => Hall.fromJson(it)));
  }

  query({Map<String, dynamic> queryData}) {

  }

  Future<List<HallImage>> queryAll({Map<String, dynamic> queryData}) {
    //queryData..['status.equals'] = true;
    queryData..['size'] = 100;
    queryData?.removeWhere((_, v) => isNull(v));
    return client
        .get(resource, queryParameters: queryData)
        .then((res) => res.data.map<HallImage>((it) => HallImage.fromJson(it)).toList());
  }

  bool isNull(dynamic value) {
    if (value == null) return true;
    if (value is String && value.isEmpty) return true;
    if (value is List && value.isEmpty) return true;
    return false;
  }
}
