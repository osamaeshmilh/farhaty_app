import 'package:farhaty_app/models/category.dart';
import 'package:injectable/injectable.dart';

import '../api_client.dart';

const String resource = "/public/categories";

@injectable
class CategoryService {
  final ApiClient client;
  CategoryService(this.client);

  Future<List<Category>> fetchAll() {
    return client.get(resource).then((res) =>
        res.data.map<Category>((it) => Category.fromJson(it)).toList());
  }

  Future<Category> getOne(id) {
    return client
        .get(resource + '/' + id)
        .then((res) => res.data.map<Category>((it) => Category.fromJson(it)));
  }
}
