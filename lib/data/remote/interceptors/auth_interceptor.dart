import 'package:dio/dio.dart';
import 'package:farhaty_app/data/local/preferences_manager.dart';
import 'package:injectable/injectable.dart';

@injectable
class AuthInterceptor extends Interceptor {
  final PreferenceManager preferenceManager;

  AuthInterceptor(this.preferenceManager);
  @override
  Future onRequest(RequestOptions options) async {
    final token = preferenceManager.getToken();
    if (token != null) {
      if (!(options.method == 'POST' && options.path == '/authenticate')) {
        options.headers['Authorization'] = 'Bearer $token';
      }
      return super.onRequest(options);
    }
  }
}

//    @override
//    Future onError(DioError error) {
//      if (error.type == DioErrorType.RESPONSE) {
//        if (error.response.statusCode == 401 && error.response.request.path != '/authenticate')
////        eventBus.fire(LoggedOut());
//          print("error");
//      }
//      return super.onError(error);
//    }
//
