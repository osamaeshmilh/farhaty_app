import 'package:farhaty_app/models/favorite.dart';
import 'package:farhaty_app/models/store.dart' as ss;
import 'package:injectable/injectable.dart';
import 'package:sembast/sembast.dart';

import '../app_database.dart';

@lazySingleton
class FavoriteDao {
  static const String CART_ITEMS = 'stores';
  final _favoriteStore = intMapStoreFactory.store(CART_ITEMS);

  Future<Database> get _db async => await AppDatabase.instance.database;

  Future insert(Favorite favorite) async {
    List<Favorite> favorites = await find(favorite);
    if (favorites.isNotEmpty)
      return null;
    else
      return _favoriteStore.add(await _db, favorite.toJson());
  }

  Future update(Favorite favorite) async {
    final finder = Finder(filter: Filter.byKey(favorite.id));
    return _favoriteStore.update(
      await _db,
      favorite.toJson(),
      finder: finder,
    );
  }

  Future deleteByStoreId(num storeId) async {
    final finder = Finder(filter: Filter.equals('id', storeId));
    return _favoriteStore.delete(
      await _db,
      finder: finder,
    );
  }

  Future<bool> storeExists(num storeId) async {
    final finder = Finder(
      filter: Filter.equals('id', storeId),
    );
    return _favoriteStore
        .find(await _db, finder: finder)
        .then((res) => res.isNotEmpty);
  }

  Future<List<Favorite>> getAll() async {
    final recordSnapshots = await _favoriteStore.find(await _db);

    return recordSnapshots.map((snapshot) {
      final favorite = Favorite.fromJson(snapshot.value);
      favorite.id = snapshot.key;
      return favorite;
    }).toList();
  }

  Future<List<Favorite>> find(Favorite favorite) async {
    final filter = Filter.and([
      Filter.equals('store.id', favorite.store.id),
    ]);
    var finder = Finder(filter: filter);

    final recordSnapshots =
        await _favoriteStore.find(await _db, finder: finder);

    return recordSnapshots.map((snapshot) {
      final favorite = Favorite.fromJson(snapshot.value);
      favorite.id = snapshot.key;
      return favorite;
    }).toList();
  }

  deleteAll() async {
    await _favoriteStore.drop(await _db);
  }
}
