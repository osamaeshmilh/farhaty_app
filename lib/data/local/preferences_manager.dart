import 'package:farhaty_app/models/jwt_token.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

const TOKEN_KEY = 'JWT_TOKEN_KEY';
const FIRST_LAUNCH = 'FIRST_LAUNCH_KEY';

@injectable
class PreferenceManager {
  final SharedPreferences prefs;

  PreferenceManager(this.prefs);

  void storeToken(JWTToken token) {
    return set(TOKEN_KEY, token.id_token);
  }

  String getToken() {
    return prefs.getString(TOKEN_KEY);
  }

  void set(key, dynamic value) {
    if (value is bool) {
      prefs.setBool(key, value);
    } else {
      prefs.setString(key, value);
    }
  }

  static Future<bool> setFirst(key, dynamic value) {
    return SharedPreferences.getInstance().then((prefs) {
      if (value is bool)
        return prefs.setBool(key, value);
      else
        return prefs.setString(key, value);
    });
  }

  static Future<bool> getBool(key) {
    return SharedPreferences.getInstance().then((prefs) => prefs.getBool(key));
  }

  static Future<SharedPreferences> get get => SharedPreferences.getInstance();

  static Future<bool> isFirstLaunch() async {
    final bool firstLaunch = await getBool(FIRST_LAUNCH) ?? true;
    setFirst(FIRST_LAUNCH, false);
    return firstLaunch;
  }
}
