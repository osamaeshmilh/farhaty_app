import 'package:flutter/cupertino.dart';
import 'package:farhaty_app/models/error.dart';

@immutable
abstract class Failure {}

class NetworkFailure extends Failure {
  NetworkFailure();
}

class ResponseFailure extends Failure {
  final ResponseError error;

  ResponseFailure(this.error);
}
