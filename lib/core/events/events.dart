abstract class GlobalEvent {}

class FetchProductEvent extends GlobalEvent {
  final num id;
  FetchProductEvent(this.id);
}
