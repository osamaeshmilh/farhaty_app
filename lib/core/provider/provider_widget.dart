import 'package:farhaty_app/core/provider/view_model.dart';
import 'package:farhaty_app/di/dependencies_config.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

abstract class ProviderWidget<T extends ViewModel> extends Widget {
  @override
  Element createElement() => BaseWidgetElement<T>(this);

  @protected
  Widget build(BuildContext context, T model);

  onModelReady(T model) {}
  onModelReadyWithContext(T model, BuildContext context) {}
}

class BaseWidgetElement<T extends ViewModel> extends ComponentElement {
  /// Creates an element that uses the given widget as its configuration.
  final T _model;

  BaseWidgetElement(ProviderWidget widget)
      : _model = getIt<T>(),
        super(widget) {
    _model.onWidgetReady();
    widget.onModelReady(_model);
    widget.onModelReadyWithContext(_model, this);
  }

  @override
  ProviderWidget get widget => super.widget;

  @override
  Widget build() {
    return Provider<T>(
      dispose: (ctx, __) => {_model.dispose()},
      create: (context) => _model,
      child: LayoutBuilder(builder: (ctx, _) => widget.build(ctx, _model)),
    );
  }
}
