import 'package:flutter/material.dart';

abstract class Model<T> with ChangeNotifier {
  T _state;
  T get initialState;

  Model() {
    state = initialState;
  }

  set state(T newState) {
    _state = newState;
    notifyListeners();
  }

  T get state => _state;
}
