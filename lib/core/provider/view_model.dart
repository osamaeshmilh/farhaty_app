import 'package:flutter/material.dart';

class ViewModel {
  void onWidgetReady() {}
  void dispose() {}

  setScrollListener(ScrollController controller, Function onReachedBottom) {
    controller.addListener(() {
      if (controller.offset >= controller.position.maxScrollExtent - 0 &&
          !controller.position.outOfRange) onReachedBottom();
    });
  }
}

abstract class ChangeNotiferModel<T> extends ViewModel with ChangeNotifier {
  T _state;

  T get initialState;

  ChangeNotiferModel() {
    state = initialState;
  }

  set state(T newState) {
    if (newState == state) return;
    _state = newState;
    notifyListeners();
  }

  T get state => _state;
}
