import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class ValueBuilder<T> extends StatefulWidget {
  final Widget Function(BuildContext ctx, T value) builder;
  final void Function(BuildContext context, T value) onBuilt;
  final ValueListenable<T> listenable;
  final Widget child;

  const ValueBuilder(
      {Key key,
      @required this.builder,
      @required this.listenable,
      this.onBuilt,
      this.child})
      : super(key: key);

  @override
  _ValueBuilderState createState() => _ValueBuilderState<T>();
}

class _ValueBuilderState<T> extends State<ValueBuilder<T>> {
  T _currentValue;
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<T>(
      valueListenable: widget.listenable,
      child: widget.child,
      builder: (ctx, value, w) {
        if (_currentValue != value) {
          if (widget.onBuilt != null)
            WidgetsBinding.instance
                .addPostFrameCallback((_) => widget.onBuilt(ctx, value));
        }
        _currentValue = value;
        return widget.builder(ctx, value);
      },
    );
  }
}
