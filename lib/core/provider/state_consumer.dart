import 'package:farhaty_app/core/provider/view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class StateConsumer<T extends ChangeNotiferModel> extends StatefulWidget {
  final Widget Function(T model, dynamic state) builder;
  final void Function(dynamic state) onBuilt;

  const StateConsumer({Key key, @required this.builder, this.onBuilt})
      : super(key: key);

  @override
  _StateConsumerState<T> createState() => _StateConsumerState<T>();
}

class _StateConsumerState<T extends ChangeNotiferModel>
    extends State<StateConsumer<T>> {
  @override
  Widget build(BuildContext context) {
    return Consumer<T>(
      builder: (_, notifier, __) {
        if (widget.onBuilt != null)
          WidgetsBinding.instance
              .addPostFrameCallback((_) => widget.onBuilt(notifier.state));
        return widget.builder(notifier, notifier.state);
      },
    );
  }
}
