import 'package:farhaty_app/core/failure/failure.dart';

abstract class GeneralFormState {
  const GeneralFormState();
}

class FormInitial extends GeneralFormState {
  const FormInitial();
}

class FormLoading extends GeneralFormState {}

class FormFailure extends GeneralFormState {
  String message;

  FormFailure(this.message);
}

class FormError extends GeneralFormState {
  final Failure failure;

  FormError(this.failure);
}

class FormSuccess<T> extends GeneralFormState {
  final T result;

  FormSuccess({this.result});
}
