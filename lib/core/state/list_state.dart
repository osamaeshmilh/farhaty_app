import 'package:farhaty_app/models/store.dart';
import 'package:flutter/foundation.dart';

@immutable
abstract class ListState {}

class ListUninitialized extends ListState {}

class ListLoading extends ListState {}

class ListLoaded<T> extends ListState {
  final Map<String, dynamic> queryData;
  final List<T> items;

  ListLoaded(this.items, {this.queryData});

  ListLoaded<T> copyWith({List<T> items, Map<String, dynamic> queryData}) {
    return ListLoaded<T>(items ?? this.items, queryData: queryData ?? this.queryData);
  }
}

class ListError extends ListState {}
