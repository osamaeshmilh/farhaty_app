// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:event_bus/event_bus.dart';
import 'package:farhaty_app/data/local/dao/favorite_dao.dart';
import 'package:farhaty_app/data/local/preferences_manager.dart';
import 'package:farhaty_app/data/remote/api_client.dart';
import 'package:farhaty_app/data/remote/interceptors/auth_interceptor.dart';
import 'package:farhaty_app/data/remote/services/account_service.dart';
import 'package:farhaty_app/data/remote/services/banner_service.dart';
import 'package:farhaty_app/data/remote/services/category_service.dart';
import 'package:farhaty_app/data/remote/services/hall_images_service.dart';
import 'package:farhaty_app/data/remote/services/hall_service.dart';
import 'package:farhaty_app/data/remote/services/notification_service.dart';
import 'package:farhaty_app/data/remote/services/reservation_service.dart';
import 'package:farhaty_app/data/remote/services/room_images_service.dart';
import 'package:farhaty_app/data/remote/services/store_images_service.dart';
import 'package:farhaty_app/data/remote/services/store_service.dart';
import 'package:farhaty_app/di/dependencies_config.dart';
import 'package:farhaty_app/ui/halls/hall_details/hall_details_model.dart';
import 'package:farhaty_app/ui/home/pages/account/account_model.dart';
import 'package:farhaty_app/ui/home/pages/favorite/favorite_model.dart';
import 'package:farhaty_app/ui/home/pages/halls/halls_model.dart';
import 'package:farhaty_app/ui/home/pages/landing/landing_model.dart';
import 'package:farhaty_app/ui/login/login_model.dart';
import 'package:farhaty_app/ui/notification/notification_model.dart';
import 'package:farhaty_app/ui/profile/profile_model.dart';
import 'package:farhaty_app/ui/reservations/new_reservation/new_reservation_model.dart';
import 'package:farhaty_app/ui/reservations/reservation_model.dart';
import 'package:farhaty_app/ui/signup/signup_model.dart';
import 'package:farhaty_app/ui/stores/store_details/store_details_model.dart';
import 'package:farhaty_app/ui/stores/store_model.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> $initGetIt(GetIt g, {String environment}) async {
  final registerModule = _$RegisterModule();
  g.registerLazySingleton<EventBus>(() => registerModule.eventBus);
  final sharedPreferences = await registerModule.prefs;
  g.registerLazySingleton<SharedPreferences>(() => sharedPreferences);
  g.registerLazySingleton<FavoriteDao>(() => FavoriteDao());
  g.registerFactory<PreferenceManager>(
      () => PreferenceManager(g<SharedPreferences>()));
  g.registerFactory<AuthInterceptor>(
      () => AuthInterceptor(g<PreferenceManager>()));
  g.registerLazySingleton<ApiClient>(() => ApiClient(g<AuthInterceptor>()));
  g.registerFactory<StoreService>(() => StoreService(g<ApiClient>()));
  g.registerFactory<BannerService>(() => BannerService(g<ApiClient>()));
  g.registerFactory<ReservationService>(
      () => ReservationService(g<ApiClient>()));
  g.registerFactory<HallImagesService>(() => HallImagesService(g<ApiClient>()));
  g.registerFactory<RoomImagesService>(() => RoomImagesService(g<ApiClient>()));
  g.registerFactory<HallService>(() => HallService(g<ApiClient>()));
  g.registerFactory<NotificationService>(
      () => NotificationService(g<ApiClient>()));
  g.registerFactory<AccountService>(
      () => AccountService(g<ApiClient>(), g<PreferenceManager>()));
  g.registerFactory<StoreImagesService>(
      () => StoreImagesService(g<ApiClient>()));
  g.registerFactory<CategoryService>(() => CategoryService(g<ApiClient>()));
  g.registerFactory<LandingModel>(() => LandingModel(
      g<BannerService>(), g<CategoryService>(), g<StoreService>()));
  g.registerFactory<FavoritesModel>(
      () => FavoritesModel(g<FavoriteDao>(), g<EventBus>()));
  g.registerLazySingleton<AccountModel>(
      () => AccountModel(g<AccountService>(), g<PreferenceManager>()));
  g.registerFactory<HallsModel>(() => HallsModel(g<HallService>()));
  g.registerFactory<ReservationModel>(
      () => ReservationModel(g<ReservationService>()));
  g.registerFactory<NotificationModel>(
      () => NotificationModel(g<NotificationService>()));
  g.registerFactory<StoreDetailsModel>(
      () => StoreDetailsModel(g<StoreService>(), g<StoreImagesService>()));
  g.registerFactory<StoreModel>(() => StoreModel(
        g<StoreService>(),
        g<FavoriteDao>(),
        g<EventBus>(),
      ));
  g.registerFactory<ProfileModel>(() => ProfileModel(g<AccountService>()));
  g.registerFactory<HallDetailsModel>(
      () => HallDetailsModel(g<HallService>(), g<HallImagesService>()));
  g.registerFactory<LoginModel>(() => LoginModel(
        g<AccountService>(),
        g<PreferenceManager>(),
        g<AccountModel>(),
      ));
  g.registerFactory<SignupModel>(() => SignupModel(
        g<AccountService>(),
        g<PreferenceManager>(),
        g<AccountModel>(),
      ));

  g.registerFactory<NewReservationModel>(
      () => NewReservationModel(g<ReservationService>()));
}

class _$RegisterModule extends RegisterModule {}
