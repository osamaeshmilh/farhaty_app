import 'package:event_bus/event_bus.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dependencies_config.iconfig.dart';

final getIt = GetIt.instance;

@injectableInit
Future configureDependencies() => $initGetIt(getIt);

@registerModule
abstract class RegisterModule {
  @lazySingleton
  EventBus get eventBus => EventBus();

  @preResolve
  @lazySingleton
  Future<SharedPreferences> get prefs => SharedPreferences.getInstance();
}
