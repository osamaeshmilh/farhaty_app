// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reservation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Reservation _$ReservationFromJson(Map<String, dynamic> json) {
  return Reservation(
    json['appointmentId'] as int,
    json['appointment'] == null
        ? null
        : Appointment.fromJson(json['appointment'] as Map<String, dynamic>),
    json['attendeesNo'] as int,
    json['attendeesType'] as String,
    (json['extras'] as List)
        ?.map(
            (e) => e == null ? null : Extra.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['id'] as int,
    json['notes'] as String,
    json['reservationDate'] == null
        ? null
        : DateTime.parse(json['reservationDate'] as String),
    json['reservationStatus'] as String,
    (json['total'] as num)?.toDouble(),
    json['type'] == null
        ? null
        : Type.fromJson(json['type'] as Map<String, dynamic>),
    json['typeId'] as int,
    json['typeName'] as String,
    json['userId'] as int,
    json['userLogin'] as String,
    json['hallId'] as num,
    json['hallName'] as String,
    json['roomId'] as num,
    json['roomName'] as String,
    json['period'] as String,
    (json['invoices'] as List)
        ?.map((e) =>
            e == null ? null : Invoice.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ReservationToJson(Reservation instance) =>
    <String, dynamic>{
      'appointmentId': instance.appointmentId,
      'appointment': instance.appointment,
      'attendeesNo': instance.attendeesNo,
      'attendeesType': instance.attendeesType,
      'extras': instance.extras,
      'id': instance.id,
      'notes': instance.notes,
      'reservationDate': instance.reservationDate?.toIso8601String(),
      'reservationStatus': instance.reservationStatus,
      'total': instance.total,
      'type': instance.type,
      'typeId': instance.typeId,
      'typeName': instance.typeName,
      'userId': instance.userId,
      'userLogin': instance.userLogin,
      'hallId': instance.hallId,
      'hallName': instance.hallName,
      'roomId': instance.roomId,
      'roomName': instance.roomName,
      'period': instance.period,
      'invoices': instance.invoices,
    };
