import 'package:farhaty_app/data/remote/constants.dart';
import 'package:json_annotation/json_annotation.dart';

// required for generation
part 'item.g.dart';

@JsonSerializable(nullable: true)
class Item {
  final int id;

  final String name;

  final String currency;

  final String description;

  final double price;

  final bool active;

  final int storeId;

  final String storeName;

  String get imageUrl => '$baseUrl/public/items/image/$id';

  Item(this.id, this.name, this.currency, this.description, this.price, this.active, this.storeId, this.storeName);

  factory Item.fromJson(Map<String, dynamic> json) => _$ItemFromJson(json);

  Map<String, dynamic> toJson() => _$ItemToJson(this);

  String getCurrencyText() {
    if (currency == 'LYD') {
      return 'د.ل';
    } else if (currency == 'USD') {
      return 'دولار';
    } else if (currency == 'EUR') {
      return 'يورو';
    } else {
      return 'د.ل';
    }
  }
}
