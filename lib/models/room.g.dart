// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Room _$RoomFromJson(Map<String, dynamic> json) {
  return Room(
    json['active'] as bool,
    json['hall'] == null
        ? null
        : Hall.fromJson(json['hall'] as Map<String, dynamic>),
    json['hallId'] as int,
    json['hallName'] as String,
    json['id'] as int,
    json['maxAttendees'] as int,
    json['name'] as String,
    (json['price'] as num)?.toDouble(),
    json['canReserve'] as bool,
    json['requestedDate'] == null
        ? null
        : DateTime.parse(json['requestedDate'] as String),
    (json['reservedDates'] as List)?.map((e) => e == null ? null : e)?.toList(),
  );
}

Map<String, dynamic> _$RoomToJson(Room instance) => <String, dynamic>{
      'active': instance.active,
      'hall': instance.hall,
      'hallId': instance.hallId,
      'hallName': instance.hallName,
      'id': instance.id,
      'maxAttendees': instance.maxAttendees,
      'name': instance.name,
      'price': instance.price,
      'canReserve': instance.canReserve,
      'requestedDate': instance.requestedDate.toIso8601String(),
      'reservedDates': instance.reservedDates,
    };
