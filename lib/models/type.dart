import 'package:farhaty_app/models/hall.dart';
import 'package:json_annotation/json_annotation.dart';

part 'type.g.dart';

@JsonSerializable(nullable: true)
class Type {
  final bool active;

  final Hall hall;

  final int hallId;

  final String hallName;

  final int id;

  final String name;

  final double price;

  final double satPrice;
  final double sunPrice;
  final double monPrice;
  final double tuePrice;
  final double wedPrice;
  final double thrPrice;
  final double friPrice;

  double getDayOfWeekPrice(DateTime dateTime) {
    if (dateTime.weekday == 1)
      return monPrice;
    else if (dateTime.weekday == 2)
      return tuePrice;
    else if (dateTime.weekday == 3)
      return wedPrice;
    else if (dateTime.weekday == 4)
      return thrPrice;
    else if (dateTime.weekday == 5)
      return friPrice;
    else if (dateTime.weekday == 6)
      return satPrice;
    else if (dateTime.weekday == 7)
      return sunPrice;
    else
      return price;
  }

  Type(
      this.active,
      this.hallId,
      this.hallName,
      this.id,
      this.name,
      this.price,
      this.hall,
      this.satPrice,
      this.sunPrice,
      this.monPrice,
      this.tuePrice,
      this.wedPrice,
      this.thrPrice,
      this.friPrice);

  factory Type.fromJson(Map<String, dynamic> json) => _$TypeFromJson(json);
  Map<String, dynamic> toJson() => _$TypeToJson(this);
}
