// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_image.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomImage _$RoomImageFromJson(Map<String, dynamic> json) {
  return RoomImage(
    json['id'] as int,
    json['roomId'] as int,
    json['roomName'] as String,
  );
}

Map<String, dynamic> _$RoomImageToJson(RoomImage instance) => <String, dynamic>{
      'id': instance.id,
      'roomId': instance.roomId,
      'roomName': instance.roomName,
    };
