import 'package:farhaty_app/models/reservation.dart';

class ReservationFilterArgs {
  final num hallId;
  final Reservation reservation;

  ReservationFilterArgs({this.hallId, this.reservation});
}
