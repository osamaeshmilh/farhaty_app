import 'hall.dart';

class HallFilterArgs {
  final num hallId;
  final bool vip;
  final DateTime dateTime;
  final String date;
  final bool canReserve;
  final Hall hall;

  HallFilterArgs(
      {this.hallId,
      this.vip,
      this.dateTime,
      this.date,
      this.canReserve,
      this.hall});
}
