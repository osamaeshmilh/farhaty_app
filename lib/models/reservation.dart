import 'package:farhaty_app/models/appointment.dart';
import 'package:farhaty_app/models/extra.dart';
import 'package:farhaty_app/models/invoice.dart';
import 'package:farhaty_app/models/type.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'reservation.g.dart';

enum reservationStatusEnum {
  PENDING,
  DONE,
  APPROVED,
  CANCELED_BY_USER,
  CANCELED_BY_HALL,
}

@JsonSerializable(nullable: true)
class Reservation {
  final int appointmentId;

  final Appointment appointment;

  final int attendeesNo;

  final String attendeesType;
  //enum attendeesTypeEnum {  MEN,  WOMEN,  ALL,  };
  final List<Extra> extras;

  final int id;

  final String notes;

  final DateTime reservationDate;

  final String reservationStatus;

  final double total;

  final Type type;

  final int typeId;

  final String typeName;

  final int userId;

  final String userLogin;

  final num hallId;

  final String hallName;

  final num roomId;

  final String roomName;

  final String period;

  final List<Invoice> invoices;

  //enum period { MORNING, EVENING, FULL_DAY };

  String getReservationStatusString() {
    if (reservationStatus == 'PENDING')
      return 'حجز مبدئي';
    else if (reservationStatus == 'DONE')
      return 'حجز منتهي';
    else if (reservationStatus == 'APPROVED')
      return ('حجز مؤكد');
    else if (reservationStatus == 'CANCELED_BY_USER')
      return ('الغي من الزبون');
    else if (reservationStatus == 'CANCELED_BY_HALL')
      return ('الغي من الصالة');
    else
      return (' ');
  }

  String getAttendeesTypeString() {
    if (attendeesType == 'MEN')
      return 'رجال';
    else if (attendeesType == 'WOMEN')
      return 'نساء';
    else if (attendeesType == 'ALL')
      return ('الكل');
    else
      return (' ');
  }

  Color getReservationStatusColor() {
    if (reservationStatus == 'PENDING')
      return Colors.orange;
    else if (reservationStatus == 'DONE')
      return Colors.blue;
    else if (reservationStatus == 'APPROVED')
      return Colors.green;
    else if (reservationStatus == 'CANCELED_BY_USER')
      return Colors.red;
    else if (reservationStatus == 'CANCELED_BY_HALL')
      return Colors.red;
    else
      return Colors.orange;
  }

  Reservation(
      this.appointmentId,
      this.appointment,
      this.attendeesNo,
      this.attendeesType,
      this.extras,
      this.id,
      this.notes,
      this.reservationDate,
      this.reservationStatus,
      this.total,
      this.type,
      this.typeId,
      this.typeName,
      this.userId,
      this.userLogin,
      this.hallId,
      this.hallName,
      this.roomId,
      this.roomName,
      this.period,
      this.invoices);

  factory Reservation.fromJson(Map<String, dynamic> json) =>
      _$ReservationFromJson(json);

  String reservationStatusText() {
    if (reservationStatus == 'PENDING')
      return 'حجز مبدئي';
    else if (reservationStatus == 'APPROVED')
      return 'حجز مؤكد';
    else if (reservationStatus == 'DONE')
      return ('حجز منتهي');
    else if (reservationStatus == 'CANCELED_BY_USER')
      return ('ملغي');
    else if (reservationStatus == 'CANCELED_BY_HALL')
      return ('ملغي');
    else
      return (' ');
  }

  Map<String, dynamic> toJson() => _$ReservationToJson(this);
}
