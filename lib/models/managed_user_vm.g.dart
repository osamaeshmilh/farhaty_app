// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'managed_user_vm.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ManagedUserVM _$ManagedUserVMFromJson(Map<String, dynamic> json) {
  return ManagedUserVM(
    json['activated'] as bool,
    (json['authorities'] as List)?.map((e) => e as String)?.toList(),
    json['createdBy'] as String,
    json['createdDate'] == null
        ? null
        : DateTime.parse(json['createdDate'] as String),
    json['email'] as String,
    json['firstName'] as String,
    json['id'] as int,
    json['imageUrl'] as String,
    json['langKey'] as String,
    json['lastModifiedBy'] as String,
    json['lastModifiedDate'] == null
        ? null
        : DateTime.parse(json['lastModifiedDate'] as String),
    json['lastName'] as String,
    json['login'] as String,
    json['password'] as String,
    json['phone'] as String,
  );
}

Map<String, dynamic> _$ManagedUserVMToJson(ManagedUserVM instance) =>
    <String, dynamic>{
      'activated': instance.activated,
      'authorities': instance.authorities,
      'createdBy': instance.createdBy,
      'createdDate': instance.createdDate?.toIso8601String(),
      'email': instance.email,
      'firstName': instance.firstName,
      'id': instance.id,
      'imageUrl': instance.imageUrl,
      'langKey': instance.langKey,
      'lastModifiedBy': instance.lastModifiedBy,
      'lastModifiedDate': instance.lastModifiedDate?.toIso8601String(),
      'lastName': instance.lastName,
      'login': instance.login,
      'password': instance.password,
      'phone': instance.phone,
    };
