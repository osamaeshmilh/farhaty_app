import 'package:json_annotation/json_annotation.dart';

part 'jwt_token.g.dart';

@JsonSerializable(nullable: true)
class JWTToken {
  
  final String id_token;
  

  JWTToken(this.id_token);

  factory JWTToken.fromJson(Map<String, dynamic> json) => _$JWTTokenFromJson(json);
  Map<String, dynamic> toJson() => _$JWTTokenToJson(this);
}
