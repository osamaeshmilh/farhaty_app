// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hall_image.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HallImage _$HallImageFromJson(Map<String, dynamic> json) {
  return HallImage(
    json['id'] as int,
    json['Hall'],
    json['hallId'] as int,
    json['hallName'] as String,
    json['roomId'] as int,

  );
}

Map<String, dynamic> _$HallImageToJson(HallImage instance) => <String, dynamic>{
      'id': instance.id,
      'Hall': instance.Hall,
      'hallId': instance.hallId,
      'hallName': instance.hallName,
  'roomId': instance.roomId,

};
