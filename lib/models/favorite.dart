import 'package:farhaty_app/models/store.dart';
import 'package:json_annotation/json_annotation.dart';

part 'favorite.g.dart';

@JsonSerializable(nullable: true, explicitToJson: true)
class Favorite {
  int id;

  Store store;
  bool isFavorite;

  Favorite({this.store, this.isFavorite}) : id = store.id;

  factory Favorite.fromJson(Map<String, dynamic> json) =>
      _$FavoriteFromJson(json);

  Map<String, dynamic> toJson() => _$FavoriteToJson(this);
}
