import 'package:farhaty_app/data/remote/constants.dart';
import 'package:farhaty_app/models/extra.dart';
import 'package:farhaty_app/models/room.dart';
import 'package:farhaty_app/models/type.dart';
import 'package:farhaty_app/models/user.dart';
import 'package:json_annotation/json_annotation.dart';

part 'hall.g.dart';

@JsonSerializable(nullable: true)
class Hall {
  final String address;

  final String email;

  final int id;

  final double lat;

  final double lng;

  final String name;

  final String phone;

  final String rules;

  final int userId;

  final User user;

  final DateTime availableFrom;

  final DateTime availableTo;

  final bool vip;

  final bool canReserve;

  final DateTime requestedDate;

  final List<Room> rooms;

  final List<Type> types;

  final List<Extra> extras;

  final List reservedDates;

  final int pendingAppointmentPeriod;

  String get logoUrl => '$baseUrl/public/halls/logo/$id';

  Hall(
      this.address,
      this.email,
      this.id,
      this.lat,
      this.lng,
      this.name,
      this.phone,
      this.rules,
      this.userId,
      this.user,
      this.availableFrom,
      this.availableTo,
      this.vip,
      this.canReserve,
      this.requestedDate,
      this.rooms,
      this.types,
      this.extras,
      this.reservedDates,
      this.pendingAppointmentPeriod);

  factory Hall.fromJson(Map<String, dynamic> json) => _$HallFromJson(json);
  Map<String, dynamic> toJson() => _$HallToJson(this);
}
