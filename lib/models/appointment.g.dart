// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Appointment _$AppointmentFromJson(Map<String, dynamic> json) {
  return Appointment(
    json['appointmentDate'] == null
        ? null
        : DateTime.parse(json['appointmentDate'] as String),
    json['hallId'] as int,
    json['hallName'] as String,
    json['id'] as int,
    json['notes'] as String,
    json['period'] as String,
    (json['price'] as num)?.toDouble(),
    json['reservationId'] as int,
    json['roomId'] as int,
    json['roomName'] as String,
    json['hall'] == null
        ? null
        : Hall.fromJson(json['hall'] as Map<String, dynamic>),
    json['reservation'] == null
        ? null
        : Reservation.fromJson(json['reservation'] as Map<String, dynamic>),
    json['room'] == null
        ? null
        : Room.fromJson(json['room'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AppointmentToJson(Appointment instance) =>
    <String, dynamic>{
      'appointmentDate': instance.appointmentDate?.toIso8601String(),
      'hall': instance.hall,
      'hallId': instance.hallId,
      'hallName': instance.hallName,
      'id': instance.id,
      'notes': instance.notes,
      'period': instance.period,
      'price': instance.price,
      'reservation': instance.reservation,
      'reservationId': instance.reservationId,
      'room': instance.room,
      'roomId': instance.roomId,
      'roomName': instance.roomName,
    };
