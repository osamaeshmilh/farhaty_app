import 'package:farhaty_app/data/remote/constants.dart';
import 'package:farhaty_app/models/hall.dart';

import 'package:json_annotation/json_annotation.dart';

// required for generation
part 'hall_image.g.dart';

@JsonSerializable(nullable: true)
class HallImage {
  
  final int id;

  final Hall;
  
  final int hallId;
  
  final String hallName;

  final int roomId;

  String get imageUrl => '$baseUrl/public/hall-images/image/$id';

  HallImage(this.id, this.Hall, this.hallId, this.hallName, this.roomId);

  factory HallImage.fromJson(Map<String, dynamic> json) => _$HallImageFromJson(json);
  Map<String, dynamic> toJson() => _$HallImageToJson(this);
}
