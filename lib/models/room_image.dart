import 'package:farhaty_app/data/remote/constants.dart';
import 'package:farhaty_app/models/hall.dart';

import 'package:json_annotation/json_annotation.dart';

// required for generation
part 'room_image.g.dart';

@JsonSerializable(nullable: true)
class RoomImage {
  
  final int id;

  final int roomId;
  
  final String roomName;

  String get imageUrl => '$baseUrl/public/room-images/image/$id';

  RoomImage(this.id,  this.roomId, this.roomName);

  factory RoomImage.fromJson(Map<String, dynamic> json) => _$RoomImageFromJson(json);
  Map<String, dynamic> toJson() => _$RoomImageToJson(this);
}
