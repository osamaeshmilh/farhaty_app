import 'package:farhaty_app/data/remote/constants.dart';
import 'package:farhaty_app/models/store.dart';

import 'package:json_annotation/json_annotation.dart';

// required for generation
part 'store_image.g.dart';

@JsonSerializable(nullable: true)
class StoreImage {
  
  final int id;

  final Store;
  
  final int storeId;
  
  final String storeName;

  String get imageUrl => '$baseUrl/public/store-images/image/$id';

  StoreImage(this.id, this.Store, this.storeId, this.storeName);

  factory StoreImage.fromJson(Map<String, dynamic> json) => _$StoreImageFromJson(json);
  Map<String, dynamic> toJson() => _$StoreImageToJson(this);
}
