
import 'package:farhaty_app/models/hall.dart';
import 'package:farhaty_app/models/reservation.dart';
import 'package:farhaty_app/models/room.dart';
import 'package:json_annotation/json_annotation.dart';

// required for generation
part 'appointment.g.dart';

@JsonSerializable(nullable: true)
class Appointment {
  
  final DateTime appointmentDate;

  final Hall hall;

  final int hallId;
  
  final String hallName;
  
  final int id;
  
  final String notes;
  
  final String period;
  //enum periodEnum {  MORNING,  EVENING,  FULL_DAY,  };
  final double price;

  final Reservation reservation;

  final int reservationId;

  final Room room;

  final int roomId;
  
  final String roomName;

  Appointment(this.appointmentDate, this.hallId, this.hallName, this.id, this.notes, this.period, this.price, this.reservationId, this.roomId, this.roomName, this.hall, this.reservation, this.room);


  factory Appointment.fromJson(Map<String, dynamic> json) => _$AppointmentFromJson(json);
  Map<String, dynamic> toJson() => _$AppointmentToJson(this);
}
