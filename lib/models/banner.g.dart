// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'banner.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Banner _$BannerFromJson(Map<String, dynamic> json) {
  return Banner(
    json['active'] as bool,
    json['id'] as int,
    json['store'] == null
        ? null
        : Store.fromJson(json['store'] as Map<String, dynamic>),
    json['storeId'] as int,
    json['storeName'] as String,
    json['title'] as String,
  );
}

Map<String, dynamic> _$BannerToJson(Banner instance) => <String, dynamic>{
      'active': instance.active,
      'id': instance.id,
      'store': instance.store,
      'storeId': instance.storeId,
      'storeName': instance.storeName,
      'title': instance.title,
    };
