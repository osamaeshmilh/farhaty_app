import 'package:farhaty_app/models/user.dart';

import 'package:json_annotation/json_annotation.dart';

// required for generation
part 'notification.g.dart';

@JsonSerializable(nullable: true)
class Notification {
  
  final String description;
  
  final int id;
  
  final String title;

  final User user;

  final int userId;
  
  final String userLogin;

  Notification(this.description, this.id, this.title, this.user, this.userId, this.userLogin);

  factory Notification.fromJson(Map<String, dynamic> json) => _$NotificationFromJson(json);
  Map<String, dynamic> toJson() => _$NotificationToJson(this);
}

