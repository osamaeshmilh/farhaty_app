// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'type.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Type _$TypeFromJson(Map<String, dynamic> json) {
  return Type(
    json['active'] as bool,
    json['hallId'] as int,
    json['hallName'] as String,
    json['id'] as int,
    json['name'] as String,
    (json['price'] as num)?.toDouble(),
    json['hall'] == null
        ? null
        : Hall.fromJson(json['hall'] as Map<String, dynamic>),

    (json['satPrice'] as num)?.toDouble(),
    (json['sunPrice'] as num)?.toDouble(),
    (json['monPrice'] as num)?.toDouble(),
    (json['tuePrice'] as num)?.toDouble(),
    (json['wedPrice'] as num)?.toDouble(),
    (json['thrPrice'] as num)?.toDouble(),
    (json['friPrice'] as num)?.toDouble(),

  );
}

Map<String, dynamic> _$TypeToJson(Type instance) => <String, dynamic>{
      'active': instance.active,
      'hall': instance.hall,
      'hallId': instance.hallId,
      'hallName': instance.hallName,
      'id': instance.id,
      'name': instance.name,
  'price': instance.price,

  'satPrice': instance.satPrice,
  'sunPrice': instance.sunPrice,
  'monPrice': instance.monPrice,
  'tuePrice': instance.tuePrice,
  'wedPrice': instance.wedPrice,
  'thrPrice': instance.thrPrice,
  'friPrice': instance.friPrice,
    };
