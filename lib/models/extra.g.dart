// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'extra.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Extra _$ExtraFromJson(Map<String, dynamic> json) {
  return Extra(
    json['active'] as bool,
    json['hall'] == null
        ? null
        : Hall.fromJson(json['hall'] as Map<String, dynamic>),
    json['hallId'] as int,
    json['hallName'] as String,
    json['id'] as int,
    json['name'] as String,
    (json['price'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$ExtraToJson(Extra instance) => <String, dynamic>{
      'active': instance.active,
      'hall': instance.hall,
      'hallId': instance.hallId,
      'hallName': instance.hallName,
      'id': instance.id,
      'name': instance.name,
      'price': instance.price,
    };
