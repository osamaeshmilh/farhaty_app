import 'package:json_annotation/json_annotation.dart';

part 'managed_user_vm.g.dart';

@JsonSerializable(nullable: true)
class ManagedUserVM {

  final bool activated;

  final List<String> authorities;

  final String createdBy;

  final DateTime createdDate;

  final String email;

  final String firstName;

  final int id;

  final String imageUrl;

  final String langKey;

  final String lastModifiedBy;

  final DateTime lastModifiedDate;

  final String lastName;

  final String login;

  final String password;

  final String phone;

  ManagedUserVM(this.activated, this.authorities, this.createdBy,
      this.createdDate, this.email, this.firstName, this.id, this.imageUrl,
      this.langKey, this.lastModifiedBy, this.lastModifiedDate, this.lastName,
      this.login, this.password, this.phone);

  factory ManagedUserVM.fromJson(Map<String, dynamic> json) => _$ManagedUserVMFromJson(json);
  Map<String, dynamic> toJson() => _$ManagedUserVMToJson(this);
}
