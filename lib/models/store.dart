

import 'package:farhaty_app/data/remote/constants.dart';
import 'package:farhaty_app/models/category.dart';
import 'package:farhaty_app/models/item.dart';

import 'package:json_annotation/json_annotation.dart';

// required for generation
part 'store.g.dart';

@JsonSerializable(nullable: true)
class Store {
  
  final String address;

  final Category category;

  final int categoryId;
  
  final String categoryName;
  
  final String description;
  
  final String email;
  
  final String facebook;
  
  final int id;
  
  final String instagram;
  
  final double lat;
  
  final double lng;
  
  final String logo;
  
  final String name;
  
  final String phone;
  
  final String snapchat;
  
  final String twitter;
  
  final int userId;
  
  final bool vip;
  
  final String viper;
  
  final String whatsup;

  final List<Item> items;


  String get logoUrl => '$baseUrl/public/stores/logo/$id';

  Store(this.address, this.category, this.categoryId, this.categoryName, this.description, this.email, this.facebook, this.id, this.instagram, this.lat, this.lng, this.logo, this.name, this.phone, this.snapchat, this.twitter, this.userId, this.vip, this.viper, this.whatsup, this.items);


  factory Store.fromJson(Map<String, dynamic> json) => _$StoreFromJson(json);
  Map<String, dynamic> toJson() => _$StoreToJson(this);
}
