
import 'package:farhaty_app/data/remote/constants.dart';
import 'package:farhaty_app/models/store.dart';
import 'package:json_annotation/json_annotation.dart';

// required for generation
part 'banner.g.dart';

@JsonSerializable(nullable: true)
class Banner {
  
  final bool active;
  
  final int id;

  final Store store;

  final int storeId;
  
  final String storeName;
  
  final String title;

  Banner(this.active, this.id, this.store, this.storeId, this.storeName, this.title);

  String get imageUrl => '$baseUrl/public/banners/image/$id';

  factory Banner.fromJson(Map<String, dynamic> json) => _$BannerFromJson(json);
  Map<String, dynamic> toJson() => _$BannerToJson(this);
}

