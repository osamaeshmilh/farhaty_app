import 'package:json_annotation/json_annotation.dart';

// required for generation
part 'user.g.dart';

@JsonSerializable(nullable: true)
class User {

  final int id;

  final String email;

  final String phone;

  final String firstName;

  final String lastName;
  
  final String login;

  User(this.id, this.email, this.firstName, this.lastName, this.login, this.phone);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}
