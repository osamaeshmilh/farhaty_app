
class StoreFilterArgs {
  final bool vip;
  final DateTime dateTime;
  final num categoryId;
  StoreFilterArgs({this.categoryId, this.vip, this.dateTime});

}