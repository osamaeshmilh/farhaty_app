// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'invoice.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Invoice _$InvoiceFromJson(Map<String, dynamic> json) {
  return Invoice(
    (json['amount'] as num)?.toDouble(),
    json['details'] as String,
    json['id'] as int,
    json['invoiceDate'] == null
        ? null
        : DateTime.parse(json['invoiceDate'] as String),
    json['invoiceType'] as String,
    json['reservationId'] as int,
    json['reservation'] == null
        ? null
        : Reservation.fromJson(json['reservation'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$InvoiceToJson(Invoice instance) => <String, dynamic>{
      'amount': instance.amount,
      'details': instance.details,
      'id': instance.id,
      'invoiceDate': instance.invoiceDate?.toIso8601String(),
      'invoiceType': instance.invoiceType,
      'reservationId': instance.reservationId,
      'reservation': instance.reservation,
    };
