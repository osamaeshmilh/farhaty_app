
import 'package:farhaty_app/data/remote/constants.dart';
import 'package:json_annotation/json_annotation.dart';

// required for generation
part 'category.g.dart';

@JsonSerializable(nullable: true)
class Category {
  
  final bool active;
  
  final int id;
  
  final String name;

  Category(this.active, this.id, this.name);

  String get imageUrl => '$baseUrl/public/categories/image/$id';

  factory Category.fromJson(Map<String, dynamic> json) => _$CategoryFromJson(json);
  Map<String, dynamic> toJson() => _$CategoryToJson(this);
}

