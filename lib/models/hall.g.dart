// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hall.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Hall _$HallFromJson(Map<String, dynamic> json) {
  return Hall(
    json['address'] as String,
    json['email'] as String,
    json['id'] as int,
    (json['lat'] as num)?.toDouble(),
    (json['lng'] as num)?.toDouble(),
    json['name'] as String,
    json['phone'] as String,
    json['rules'] as String,
    json['userId'] as int,
    json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
    json['availableFrom'] == null
        ? null
        : DateTime.parse(json['availableFrom'] as String),
    json['availableTo'] == null
        ? null
        : DateTime.parse(json['availableTo'] as String),
    json['vip'] as bool,
    json['canReserve'] as bool,
    json['requestedDate'] == null
        ? null
        : DateTime.parse(json['requestedDate'] as String),
    (json['rooms'] as List)
        ?.map(
            (e) => e == null ? null : Room.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['types'] as List)
        ?.map(
            (e) => e == null ? null : Type.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['extras'] as List)
        ?.map(
            (e) => e == null ? null : Extra.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['reservedDates'] as List)?.map((e) => e == null ? null : e)?.toList(),
    json['pendingAppointmentPeriod'] as int,
  );
}

Map<String, dynamic> _$HallToJson(Hall instance) => <String, dynamic>{
      'address': instance.address,
      'email': instance.email,
      'id': instance.id,
      'lat': instance.lat,
      'lng': instance.lng,
      'name': instance.name,
      'phone': instance.phone,
      'rules': instance.rules,
      'userId': instance.userId,
      'user': instance.user,
      'availableFrom': instance.availableFrom.toIso8601String(),
      'availableTo': instance.availableTo.toIso8601String(),
      'vip': instance.vip,
      'canReserve': instance.canReserve,
      'requestedDate': instance.requestedDate.toIso8601String(),
      'rooms': instance.rooms,
      'types': instance.types,
      'extras': instance.extras,
      'reservedDates': instance.reservedDates,
      'pendingAppointmentPeriod': instance.pendingAppointmentPeriod,
    };
