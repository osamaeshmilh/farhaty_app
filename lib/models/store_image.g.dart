// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'store_image.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StoreImage _$StoreImageFromJson(Map<String, dynamic> json) {
  return StoreImage(
    json['id'] as int,
    json['Store'],
    json['storeId'] as int,
    json['storeName'] as String,
  );
}

Map<String, dynamic> _$StoreImageToJson(StoreImage instance) =>
    <String, dynamic>{
      'id': instance.id,
      'Store': instance.Store,
      'storeId': instance.storeId,
      'storeName': instance.storeName,
    };
