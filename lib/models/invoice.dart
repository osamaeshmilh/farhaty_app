import 'dart:ui';

import 'package:farhaty_app/models/reservation.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'invoice.g.dart';

@JsonSerializable(nullable: true)
class Invoice {
  final double amount;

  final String details;

  final int id;

  final DateTime invoiceDate;

  final String invoiceType;
  //enum invoiceTypeEnum {  FULL_PAYMENT,  PARTIAL_PAYMENT,  RETURN,  };
  final int reservationId;

  final Reservation reservation;

  String getInvoiceTypeString() {
    if (invoiceType == 'FULL_PAYMENT')
      return 'دفع كامل';
    else if (invoiceType == 'PARTIAL_PAYMENT')
      return 'دفع جزئي';
    else if (invoiceType == 'RETURN')
      return ('مبلغ مسترجع');
    else if (invoiceType == 'GUARANTEE')
      return ('مبلغ ضمان');
    else
      return (' ');
  }

  Color getInvoiceTypeColor() {
    if (invoiceType == 'FULL_PAYMENT')
      return Colors.green;
    else if (invoiceType == 'PARTIAL_PAYMENT')
      return Colors.orange;
    else if (invoiceType == 'RETURN')
      return Colors.red;
    else if (invoiceType == 'GUARANTEE')
      return Colors.blue;
    else
      return Colors.orange;
  }

  Invoice(this.amount, this.details, this.id, this.invoiceDate,
      this.invoiceType, this.reservationId, this.reservation);

  factory Invoice.fromJson(Map<String, dynamic> json) =>
      _$InvoiceFromJson(json);
  Map<String, dynamic> toJson() => _$InvoiceToJson(this);
}
