import 'package:farhaty_app/models/hall.dart';
import 'package:json_annotation/json_annotation.dart';

part 'extra.g.dart';

@JsonSerializable(nullable: true)
class Extra {
  final bool active;

  final Hall hall;

  final int hallId;

  final String hallName;

  final int id;

  final String name;

  final double price;

  bool checked = false;

  Extra(this.active, this.hall, this.hallId, this.hallName, this.id, this.name,
      this.price);

  factory Extra.fromJson(Map<String, dynamic> json) => _$ExtraFromJson(json);
  Map<String, dynamic> toJson() => _$ExtraToJson(this);
}
