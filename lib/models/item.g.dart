// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Item _$ItemFromJson(Map<String, dynamic> json) {
  return Item(
    json['id'] as int,
    json['name'] as String,
    json['currency'] as String,
    json['description'] as String,
    (json['price'] as num)?.toDouble(),
    json['active'] as bool,
    json['storeId'] as int,
    json['storeName'] as String,
  );
}

Map<String, dynamic> _$ItemToJson(Item instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'currency': instance.currency,
      'description': instance.description,
      'price': instance.price,
      'active': instance.active,
      'storeId': instance.storeId,
      'storeName': instance.storeName
    };
