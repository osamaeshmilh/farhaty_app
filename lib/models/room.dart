import 'package:farhaty_app/models/hall.dart';
import 'package:farhaty_app/models/hall_image.dart';
import 'package:json_annotation/json_annotation.dart';

part 'room.g.dart';

@JsonSerializable(nullable: true)
class Room {
  final bool active;

  final Hall hall;

  final int hallId;

  final String hallName;

  final int id;

  final int maxAttendees;

  final String name;

  final double price;

  bool canReserve;

  final DateTime requestedDate;

  final List reservedDates;

  final List<HallImage> images;

  Room(
      this.active,
      this.hall,
      this.hallId,
      this.hallName,
      this.id,
      this.maxAttendees,
      this.name,
      this.price,
      this.canReserve,
      this.requestedDate,
      this.reservedDates,
      {this.images});

  factory Room.fromJson(Map<String, dynamic> json) => _$RoomFromJson(json);

  Map<String, dynamic> toJson() => _$RoomToJson(this);
}
