// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'store.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Store _$StoreFromJson(Map<String, dynamic> json) {
  return Store(
    json['address'] as String,
    json['category'] == null
        ? null
        : Category.fromJson(json['category'] as Map<String, dynamic>),
    json['categoryId'] as int,
    json['categoryName'] as String,
    json['description'] as String,
    json['email'] as String,
    json['facebook'] as String,
    json['id'] as int,
    json['instagram'] as String,
    (json['lat'] as num)?.toDouble(),
    (json['lng'] as num)?.toDouble(),
    json['logo'] as String,
    json['name'] as String,
    json['phone'] as String,
    json['snapchat'] as String,
    json['twitter'] as String,
    json['userId'] as int,
    json['vip'] as bool,
    json['viper'] as String,
    json['whatsup'] as String,
    (json['items'] as List)
        ?.map(
            (e) => e == null ? null : Item.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$StoreToJson(Store instance) => <String, dynamic>{
      'address': instance.address,
      'category': instance.category,
      'categoryId': instance.categoryId,
      'categoryName': instance.categoryName,
      'description': instance.description,
      'email': instance.email,
      'facebook': instance.facebook,
      'id': instance.id,
      'instagram': instance.instagram,
      'lat': instance.lat,
      'lng': instance.lng,
      'logo': instance.logo,
      'name': instance.name,
      'phone': instance.phone,
      'snapchat': instance.snapchat,
      'twitter': instance.twitter,
      'userId': instance.userId,
      'vip': instance.vip,
      'viper': instance.viper,
  'whatsup': instance.whatsup,
  'items': instance.items,

    };
