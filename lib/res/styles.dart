import 'package:flutter/material.dart';

import 'app_colors.dart';

//text styles
TextStyle primary([double size = 14.0, bool bold = false]) =>
    TextStyle(                                 
        fontFamily: 'Cairo',
        color: AppColors.primary, fontSize: size, fontWeight: bold ? FontWeight.bold : FontWeight.normal);

TextStyle accent([double size = 14.0]) => TextStyle(color: AppColors.accent, fontSize: size);

TextStyle grey([double size = 14.0]) => TextStyle(color: Colors.grey, fontSize: size);

TextStyle subGrey([double size = 14.0]) => TextStyle(color: Colors.grey.shade300, fontSize: size);

TextStyle white([double size = 14.0, bool bold = false]) => TextStyle(color: Colors.white, fontSize: size, fontWeight: bold ? FontWeight.bold : FontWeight.normal);

TextStyle defaultStyle([double size = 14.0, bool bold = false]) =>
    TextStyle(fontSize: size, fontWeight: bold ? FontWeight.bold : FontWeight.normal);

// spacing
const vS4 = SizedBox(height: 4);
const vS8 = SizedBox(height: 8);
const vS12 = SizedBox(height: 12);
const vS16 = SizedBox(height: 16);
const vS24 = SizedBox(height: 24);
const vS32 = SizedBox(height: 32);

const hS8 = SizedBox(width: 8);
const hS16 = SizedBox(width: 16);
const hS24 = SizedBox(width: 24);
const hS32 = SizedBox(width: 32);

// padding
const hp8 = EdgeInsets.symmetric(horizontal: 8);
const hp16 = EdgeInsets.symmetric(horizontal: 16);
const hp24 = EdgeInsets.symmetric(horizontal: 24);

defaultDecoration(String label) => InputDecoration(border: OutlineInputBorder(), labelText: label);
