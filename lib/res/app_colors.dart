import 'package:flutter/material.dart';

class AppColors {
  static Color primary = Color(0xFFcc9999);
  static Color background = Colors.grey.shade50;
  static Color accent = Color(0xFFcc9999);
  static Color light = Color(0xFFcc9999).withOpacity(0.5);

  static Color lightPurple = Color(0xFFcc9999);
}
